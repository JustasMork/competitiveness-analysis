package com.catModel;

import java.util.ArrayList;
import java.util.List;

public class ScrapeResults {

    public int id;

    public List<ScrapeResult> scrapeResults;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<ScrapeResult> getScrapeResults() {
        return scrapeResults;
    }

    public void setScrapeResults(List<ScrapeResult> scrapeResults) {
        this.scrapeResults = scrapeResults;
    }

    public void addResult(ScrapeResult result) {
        if (scrapeResults == null) {
            scrapeResults = new ArrayList<>();
        }

        scrapeResults.add(result);
    }
}
