package com.catModel;

public enum CabinClass {

    Economy(0),
    PremEconomy(1),
    Business(2),
    First(3);

    private int classCode;

    CabinClass(int classCode) {
        this.classCode = classCode;
    }

    public int getClassCode() {
        return classCode;
    }

    public static CabinClass fromCode(int code) {
        for (CabinClass classCode : CabinClass.values()) {
            if (code == classCode.getClassCode()) {
                return classCode;
            }
        }

        return null;
    }
}
