package com.catModel;

import java.math.BigDecimal;
import java.util.Currency;

public class Price {

    private BigDecimal amount;

    private String currCode;

    private String currSymbol;

    private Currency currency;

    public Price() {}

    public Price(BigDecimal amount, Currency currency) {
        this.amount = amount;
        this.currency = currency;
    }

    public Price(BigDecimal amount, String currSymbol) {
        this.amount = amount;
        this.currCode = currSymbol;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currCode;
    }

    public void setCurrSymbol(String symbol) {
        this.currSymbol = symbol;
    }

    public void setCurrency(String currency) {
        this.currCode = currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }
}
