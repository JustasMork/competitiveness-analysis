package com.catModel;

public enum FlightType {

    OW(0),
    RT(1),
    MC(2),
    OJ(3);

    private int typeCode;

    FlightType(int type) {
        this.typeCode = type;
    }

    public int getCode() {
        return typeCode;
    }

    public static FlightType fromCode(int code) {
        for (FlightType value : FlightType.values()) {
            if (value.typeCode == code) {
                return value;
            }
        }

        return null;
    }

}
