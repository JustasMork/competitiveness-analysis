package com.catModel;



import com.fasterxml.jackson.annotation.JsonIgnore;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;
import java.util.Map;

public class FlightScrapeRequest implements ScrapeRequest {

    private List<ODpair> itinerary;

    private Map<PTC, Integer> travellers;

    private CabinClass cabinClass;

    private String countryCode;

    public List<ODpair> getItinerary() {
        return itinerary;
    }

    public void setItinerary(List<ODpair> itinerary) {
        this.itinerary = itinerary;
    }

    @JsonIgnore
    public String getItineraryString() {
        return (new JSONArray(itinerary, true)).toString();
    }

    @JsonIgnore
    public ODpair getFirstOD() {
        return itinerary.get(0);
    }

    public Map<PTC, Integer> getTravellers() {
        return travellers;
    }

    @JsonIgnore
    public String getTravellersString() {
        return (new JSONObject(travellers)).toString();
    }

    public void setTravellers(Map<PTC, Integer> travellers) {
        this.travellers = travellers;
    }

    public CabinClass getCabinClass() {
        return cabinClass;
    }

    public void setCabinClass(CabinClass cabinClass) {
        this.cabinClass = cabinClass;
    }

    public void setCabinClass(String cabinClass) {
        this.cabinClass = CabinClass.valueOf(cabinClass);
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    @JsonIgnore
    public boolean isOneWay() {
        return itinerary.size() == 1;
    }

    @JsonIgnore
    public boolean isRoundTrip() {
        return itinerary.size() == 2
                && itinerary.get(0).getOrigin().equals(itinerary.get(1).getDestination())
                && itinerary.get(0).getDestination().equals(itinerary.get(1).getOrigin());
    }
}
