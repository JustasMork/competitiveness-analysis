package com.catModel;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.List;

public class Trip {

    private int id;

    private FlightType flightType;

    private List<Leg> legs;

    private List<PriceInfo> priceInfos;

    @JsonIgnore
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public FlightType getFlightType() {
        return flightType;
    }

    public void setFlightType(FlightType flightType) {
        this.flightType = flightType;
    }

    public List<Leg> getLegs() {
        return legs;
    }

    public void setLegs(List<Leg> legs) {
        this.legs = legs;
    }

    public void addLeg(Leg leg) {
        if (legs == null) {
            legs = new ArrayList<>();
        }

        legs.add(leg);
    }

    public List<PriceInfo> getPriceInfos() {
        if (priceInfos == null) {
            priceInfos = new ArrayList<>();
        }

        return priceInfos;
    }

    public void setPriceInfos(List<PriceInfo> priceInfos) {
        this.priceInfos = priceInfos;
    }
}
