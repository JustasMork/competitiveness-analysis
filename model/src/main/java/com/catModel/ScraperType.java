package com.catModel;

public enum ScraperType {

    SKYSCANNER(1, "skyscanner", "Sky Scanner"),
    KIWI(2, "kiwi", "Kiwi.com");


    private int id;

    private String propTag;

    private String displayName;

    ScraperType(int id, String propTag, String displayName) {
        this.id = id;
        this.propTag = propTag;
        this.displayName = displayName;
    }

    public static ScraperType fromPropTag(String propTag) {
        for (ScraperType type : ScraperType.values()) {
            if (propTag.equals(type.propTag)) {
                return type;
            }
        }

        return null;
    }

    public int getId() {
        return id;
    }

    public String getPropTag() {
        return propTag;
    }

    public String getDisplayName() {
        return displayName;
    }
}
