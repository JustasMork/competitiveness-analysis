package com.catModel;

import org.json.JSONObject;

import java.io.Serializable;


public class Airport implements Serializable {

    private static final long serialVersionUID = 1L;

    private String iata;

    private String icao;

    private String name;

    private String country;

    private String countryCode;

    private String region;

    private String city;

    public String getIata() {
        return iata;
    }

    public void setIata(String iata) {
        this.iata = iata;
    }

    public String getIcao() {
        return icao;
    }

    public void setIcao(String icao) {
        this.icao = icao;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;

        if (!(obj instanceof Airport))
            return false;

        Airport airport = (Airport) obj;

        return iata.equals(airport.iata)
                && icao.equals(airport.icao)
                && name.equals(airport.name)
                && country.equals(airport.country)
                && (countryCode == null || countryCode.equals(airport.countryCode))
                && region.equals(airport.region)
                && city.equals(airport.city);
    }

    @Override
    public int hashCode() {
        return iata.hashCode()
                + icao.hashCode()
                + name.hashCode()
                + country.hashCode()
                + countryCode.hashCode()
                + region.hashCode()
                + city.hashCode();
    }

    @Override
    public String toString() {
        return (new JSONObject(this)).toString();
    }
}
