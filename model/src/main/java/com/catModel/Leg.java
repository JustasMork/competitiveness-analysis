package com.catModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Leg {

    private List<Segment> segments;

    public Leg() {
        segments = new ArrayList<>();
    }

    public void addSegment(Segment segment) {
        segments.add(segment);
    }

    public void setSegments(List<Segment> segments) {
        this.segments = segments;
    }

    public List<Segment> getSegments() {
        return segments;
    }

    public Date getDeparture() {
        // TODO: implement departure parsing from first segment.
        return null;
    }

    public Date getArrival() {
        // TODO: implement arrival parsing from last segment.
        return null;
    }

    public Airport getOrigin() {
        // TODO: implement departure airport parsing from first segment.
        return null;
    }

    public Airport getDestination() {
        // TODO: implement destination airport parsing from last segment.
        return null;
    }
}
