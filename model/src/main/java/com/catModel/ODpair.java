package com.catModel;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ODpair {

    private Airport origin;

    private Airport destination;

    private Date departure;

    public Airport getOrigin() {
        return origin;
    }

    public void setOrigin(Airport origin) {
        this.origin = origin;
    }

    public Airport getDestination() {
        return destination;
    }

    public void setDestination(Airport destination) {
        this.destination = destination;
    }

    @JsonIgnore
    public Date getDepartureDate() {
        return departure;
    }

    public String getDeparture() {
        return (new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ")).format(departure);
    }

    public void setDeparture(String departure) {
        try {
            this.departure = (new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ")).parse(departure);
        } catch (ParseException e) {
            try {
                this.departure = (new SimpleDateFormat("yyyy-MM-dd")).parse(departure);
            } catch (ParseException ex) {
                ex.printStackTrace();
            }
        }
    }

    public void setDeparture(Date departure) {
        this.departure = departure;
    }
}
