package com.catModel;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FlightSearchResult implements ScrapeResult {

    private int id;

    private ScraperType scraperType;

    private Date scrapingTime;

    private Integer resultLoadingTime;

    private List<Trip> trips;

    public FlightSearchResult() {}

    @JsonIgnore
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getScrapingTime() {
        return scrapingTime;
    }

    public void setScrapingTime(Date scrapingTime) {
        this.scrapingTime = scrapingTime;
    }

    public Integer getResultLoadingTime() {
        return resultLoadingTime;
    }

    public void setResultLoadingTime(Integer resultLoadingTime) {
        this.resultLoadingTime = resultLoadingTime;
    }

    public List<Trip> getTrips() {
        if (trips == null) {
            trips = new ArrayList<>();
        }
        return trips;
    }

    public void setTrips(List<Trip> trips) {
        this.trips = trips;
    }

    public void addTrip(Trip trip) {
        if (trips == null) {
            trips = new ArrayList<>();
        }

        trips.add(trip);
    }

    public ScraperType getScraperType() {
        return scraperType;
    }

    @Override
    public void setScraperType(ScraperType scraperType) {
        this.scraperType = scraperType;
    }
}
