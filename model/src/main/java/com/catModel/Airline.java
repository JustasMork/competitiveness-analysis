package com.catModel;

import java.io.Serializable;

public class Airline implements Serializable {

    private String code;

    private String iata;

    private String name;

    private String country;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getIata() {
        return iata;
    }

    public void setIata(String iata) {
        this.iata = iata;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public int hashCode() {
        return  code.hashCode()
                +iata.hashCode()
                + name.hashCode()
                + country.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;

        if (!(obj instanceof Airline))
            return false;

        Airline airline = (Airline) obj;

        return  code.equals(airline.code)
                && iata.equals(airline.iata)
                && (name == null || name.equals(airline.name))
                && (country == null || country.equals(airline.country));
    }

    @Override
    public String toString() {
        return String.format("{ %s, %s, %s, %s }", getCode(), getIata(), getName(), getCountry());
    }
}
