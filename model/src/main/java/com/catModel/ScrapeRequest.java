package com.catModel;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME)
@JsonSubTypes(
        @JsonSubTypes.Type(value = FlightScrapeRequest.class, name = "flightScrapeRequest")
)
public interface ScrapeRequest {

}
