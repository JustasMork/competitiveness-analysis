package com.catModel;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.json.JSONArray;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ScrapeRequests {

    private int id;

    private String browser;

    private List<ScraperType> scraperTypes;

    private List<ScrapeRequest> scrapeRequests;

    private int started;

    private Date dateCreated;

    private Date dateCompleted;

    public ScrapeRequests() {}

    public ScrapeRequests(String browser, List<ScraperType> scrapers) {
        this.browser = browser;
        this.scraperTypes = scrapers;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBrowser() {
        return browser;
    }

    public void setBrowser(String browser) {
        this.browser = browser;
    }

    public void setScraperTypes(List<ScraperType> scraperTypes) {
        this.scraperTypes = scraperTypes;
    }

    public List<ScraperType> getScraperTypes() {
        return scraperTypes;
    }

    @JsonIgnore
    public String getScraperTypesString() {
        return (new JSONArray(scraperTypes)).toString();
    }

    public List<ScrapeRequest> getRequests() {
        return scrapeRequests;
    }

    public void addRequest(ScrapeRequest request) {
        if (scrapeRequests == null) {
            scrapeRequests = new ArrayList<>();
        }

        scrapeRequests.add(request);
    }

    public void setRequests(List<ScrapeRequest> requests) {
        this.scrapeRequests = requests;
    }

    public int getStarted() {
        return started;
    }

    public void setStarted(int started) {
        this.started = started;
    }

    public String getDateCreated() {
        if (dateCreated == null) {
            return "";
        }
        return new SimpleDateFormat("yyyy-MM-dd HH:ss").format(dateCreated);
    }

    public void setDateCreated(String dateCreated) throws ParseException {
        this.dateCreated = (new SimpleDateFormat("yyyy-MM-dd HH:ss")).parse(dateCreated);
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getDateCompleted() {
        if (dateCompleted == null) {
            return "";
        }
        return new SimpleDateFormat("yyyy-MM-dd HH:ss").format(dateCompleted);
    }

    public void setDateCompleted(String dateCompleted) throws ParseException {
        this.dateCompleted = (new SimpleDateFormat("yyyy-MM-dd HH:ss")).parse(dateCompleted);
    }

    public void setDateCompleted(Date dateCompleted) {
        this.dateCompleted = dateCompleted;
    }
}
