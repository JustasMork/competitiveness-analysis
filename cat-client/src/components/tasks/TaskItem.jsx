import React, {Component} from "react";
import TableRow from "@material-ui/core/TableRow/index";
import TableCell from "@material-ui/core/TableCell/index";
import {getLocalesString, getTaskItemDetailsString} from "../../helpers/FormatHelper";
import Chip from "@material-ui/core/Chip/index";
import RequestDetailsItem from "../RequestDetailsItem";

class TaskItem extends Component {



    renderStatus = (status) => {
        switch (status) {
            case 0:
                return <Chip label="Waiting" size={"small"} className={"waiting-status"} />;
            case 1:
                return <Chip label="Pending" size={"small"} className={"pending-status"} />;
            case 2:
                return <Chip label="Completed" size={"small"} className={"completed-status"} />;
            default:
                return <Chip label="Failed" size={"small"} className={"failed-status"} />;
        }
    };

    render() {
        if (this.props.task) {
            return (
                <TableRow>
                    <TableCell>{this.props.task.id}</TableCell>
                    <TableCell>{this.props.task.scraperTypes.join(", ")}</TableCell>
                    <TableCell>{getLocalesString(this.props.task.requests)}</TableCell>
                    <TableCell>{this.props.task.browser}</TableCell>
                    <TableCell><RequestDetailsItem requests={this.props.task.requests}/></TableCell>
                    <TableCell>{this.renderStatus(this.props.task.started)}</TableCell>
                    <TableCell className={"no-wrap"}>{this.props.task.dateCreated}</TableCell>
                </TableRow>
            )
        }
    }
}

export default TaskItem;