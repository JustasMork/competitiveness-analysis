import React, {Component} from "react";
import Grid from "@material-ui/core/Grid";
import TravellerSelection from "./TravellerSelection";
import ItinerarySelection from "./ItinerarySelection";
import Chip from "@material-ui/core/Chip";
import Button from "@material-ui/core/Button";
import {FormControl, InputLabel, Select} from "@material-ui/core";
import MenuItem from "@material-ui/core/MenuItem";


class ScrapeRequestForm extends Component {

    constructor(props) {
        super(props);
        this.state = {
            "@type": "flightScrapeRequest",
            itinerary: [],
            travellers: null,
            cabinClass: this.props.cabinClasses[0] || null,
            countryCode: "us"
        }
    }

    addItinerary = (itinerary) => {
        this.state.itinerary.push(itinerary)
        this.setState(this.state)
    };

    handleTravellerChange = (travellers) => {
        this.state.travellers = travellers;
    };

    renderItineraryItem = (item, index) => {
        return (
            <Chip variant={"outlined"}
                      label={item.origin.iata + " - " + item.destination.iata + " " + item.departure}
                      onDelete={() => {
                          this.state.itinerary.splice(index, 1);
                          this.setState(this.state);
                      }}
        />)
        ;
    };

    renderItineraryItems = () => {
        if (this.state.itinerary.length > 0) {
           return this.state.itinerary.map((item, index) => {
               return this.renderItineraryItem(item, index);
           })
        }
    };

    isStateValid = () => {
        return this.state.travellers
            && this.state.itinerary
            && this.state.itinerary.length > 0;
    };

    renderAddButton = () => {

        if (this.isStateValid()) {
            return (
                <Grid item xs={12} sm={8} >
                    <Button
                            variant={"outlined"}
                            onClick={this.submitScrapingRequest}
                            className="pull-right"
                    >Add scraping request</Button>
                </Grid>
            )
        }
    };

    submitScrapingRequest = () => {
        this.props.handleAdd(this.state);
        this.setState({
            "@type": "flightScrapeRequest",
            itinerary: [],
            travellers: this.state.travellers,
        });
    };


    render() {
        return (
                <Grid container spacing={1} >

                    <ItinerarySelection itinerary={this.state.itinerary} handleAdd={this.addItinerary}/>
                    <Grid item xs={12} sm={12} spacing={1} className="items-itinerary" justify={"space-between"}>
                        {this.renderItineraryItems()}
                    </Grid>

                    <TravellerSelection handleChange={this.handleTravellerChange}/>
                    <Grid item sm={4} xs={12}>
                        <FormControl fullWidth={true}>
                            <InputLabel>Cabin class</InputLabel>
                            <Select value={this.state.cabinClass}
                                    onChange={(e) => {this.state.cabinClass = e.target.value; this.setState(this.state);}}
                            >
                                {this.props.cabinClasses.map((item) => {
                                    return (<MenuItem value={item}>{item}</MenuItem>);
                                })}
                            </Select>
                        </FormControl>
                    </Grid>

                    {this.renderAddButton()}
                </Grid>
        )
    }
}

export default ScrapeRequestForm;