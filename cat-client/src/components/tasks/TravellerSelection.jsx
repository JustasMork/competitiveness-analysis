import React, {Component} from "react";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Slider from "@material-ui/core/Slider";

class TravellerSelection extends Component {

    constructor(props) {
        super(props);
        this.state = {
            travellers: [
                {name: "Adults", type: "ADT", amount: 1},
                {name: "Children", type: "CHD", amount: 0},
                {name: "Infants", type: "INF", amount: 0}
            ]
        };
        this.props.handleChange(this.getFormattedTypes())
    }

    renderTravellerSliders = () => {
        return this.state.travellers.map((traveller, index) => {
            return this.renderSlider(traveller, index);
        })
    };

    getIntRange = (from, to) => {
        let range = [];
        for (let i = from; i <= to; i++) {
            range.push({value: i, label: i});
        }
        return range;
    };

    getFormattedTypes = () => {
      let travellers = {};
      this.state.travellers
          .filter((item) => item.amount > 0)
          .forEach((item) => {travellers[item.type] = item.amount});
      return travellers;
    };

    handleSliderChange = (index, value) => {
        this.state.travellers[index].amount = value;
        this.props.handleChange(this.getFormattedTypes());
        this.setState(this.state);
    };

    renderSlider = (traveller, index) => {
        return(
            <Grid item sm={4} xs={12}>
                <Typography>{traveller.name}</Typography>
                <Slider step={1}
                        min={0}
                        max={9}
                        value={traveller.amount}
                        marks={this.getIntRange(0, 9)}
                        onChange={(e, val) => {this.handleSliderChange(index, val)}}/>
            </Grid>
        )
    };

    render() {
        return(
            <Grid container spacing={3}>
                {this.renderTravellerSliders()}
            </Grid>
        )
    }

}

export default TravellerSelection;