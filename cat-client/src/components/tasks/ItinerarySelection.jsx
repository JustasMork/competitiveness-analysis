import React, {Component} from "react";
import Grid from "@material-ui/core/Grid/index";
import AirportAutocomplete from "../AirportAutocomplete";
import {DatePicker, MuiPickersUtilsProvider} from "@material-ui/pickers/index";
import MomentUtils from "@date-io/moment/build/index";
import IconButton from "@material-ui/core/IconButton/index";
import {Add} from "@material-ui/icons/index";
import moment from "moment/moment";

class ItinerarySelection extends Component {

    constructor(props) {
        super(props);
        this.state = {
            minDate: moment().add(1, 'days').toDate(),
            origin: null,
            destination: null,
            departure: null
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.itinerary.length === 0 && prevProps.itinerary.length > 0) {
            this.state.minDate = moment().add(1, 'days').toDate();
            this.setState(this.state);
        }
    }

    handleDateChange = (date) => {
        this.state.departure = date;
        this.setState(this.state);
    };

    getItineraryObject = () => {
        return {
            origin: {iata:this.state.origin},
            destination: {iata: this.state.destination},
            departure: moment(this.state.departure).format("YYYY-MM-DD")
        }
    };

    handleAddClick = () => {
        if (this.state.origin && this.state.destination && this.state.departure) {
            this.props.handleAdd(this.getItineraryObject());
            this.setState({
                minDate: moment(this.state.departure).add(1, 'days').toDate(),
                origin: this.state.destination,
                destination: null,
                departure: null,
            });
        }
    };

    render() {
        return (
            <Grid container sm={12} xs={12} spacing={1} alignItems={"center"}>
                <Grid item sm={4} xs={12}>
                    <AirportAutocomplete title={"Origin"}
                                         value={this.state.origin}
                                         update={(val) => {
                                             this.state.origin = val
                                         }}/>
                </Grid>
                <Grid item sm={4} xs={12}>
                    <AirportAutocomplete title={"Destination"}
                                         value={this.state.destination}
                                         update={(val) => {
                                             this.state.destination = val
                                         }}/>
                </Grid>
                <Grid item sm={3} xs={10}>
                    <MuiPickersUtilsProvider utils={MomentUtils}>
                        <DatePicker
                            fullWidth={true}
                            disableToolbar
                            variant="inline"
                            format="YYYY-MM-DD"
                            margin="normal"
                            label="Departure"
                            value={this.state.departure}
                            onChange={this.handleDateChange}
                            minDate={this.state.minDate}
                        />
                    </MuiPickersUtilsProvider>
                </Grid>
                <Grid item sm={1} xs={2} alignItems={"center"}>
                    <IconButton color={"primary"}
                                size={"small"}
                                edge={"end"}
                                className="bgr-primary pull-right"
                                onClick={this.handleAddClick}>
                        <Add/>
                    </IconButton>
                </Grid>
            </Grid>
        );
    }
}

export default ItinerarySelection;