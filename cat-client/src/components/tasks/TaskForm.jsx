import React, {Component} from "react";
import Dialog from "@material-ui/core/Dialog/index";
import Button from "@material-ui/core/Button/index";
import {connect} from "react-redux";
import {allTasks, toggleTasksModal} from "../../redux/actions";
import DialogTitle from "@material-ui/core/DialogTitle/index";
import DialogContent from "@material-ui/core/DialogContent/index";
import DialogActions from "@material-ui/core/DialogActions/index";
import {browsers} from "../../data/Browser";
import MenuItem from "@material-ui/core/MenuItem/index";
import { Select, FormControl, InputLabel} from '@material-ui/core';
import ScrapeRequestForm from "./ScrapeRequestForm";
import ScrapeRequestItems from "./ScrapeRequestItems";
import {getAllTasks, postTask} from "../../redux/restActions";

class TaskForm extends Component {

   constructor(props) {
       super(props);

       this.state = {
           browser: this.props.modalData.browser,
           scraperTypes: this.props.scraperTypes,
           requests: []
       }
   }

    toggleModal = (isOpen) => {
        this.props.dispatch(toggleTasksModal(isOpen))
    };

    handleClickOpen = () => {
        this.toggleModal(true);
    };

    handleClickClose = () => {
        this.toggleModal(false);
        this.setState({
            browser: this.props.modalData.browser,
            scraperTypes: this.props.scraperTypes,
            requests: []
        });
    };

    renderBrowserSelections = () => {
        return browsers.map(item => {
            return (
                <MenuItem value={item.value}>{item.name}</MenuItem>
            );
        })
    };

    renderScraperTypesSelections = () => {
        return this.props.scraperTypes.map((item) => {
            return (
                <MenuItem value={item}>{item}</MenuItem>
            );
        })
    };

    scraperTypeChange = (e) => {
        this.state.scraperTypes = e.target.value;
        this.setState(this.state);
    };

    handleAddRequest = (scrapeRequest) => {
        this.state.requests.push(scrapeRequest);
        this.setState(this.state);
    };

    renderRequests = () => {
        if (this.state.requests.length > 0) {
            return (
                <ScrapeRequestItems items={this.state.requests}/>
            );
        }
    };

    handleBrowserChange = (e) => {
        this.state.browser = e.target.value;
        this.setState(this.state);
    };

    handleFormSubmit = () => {
        postTask(this.state)
            .then(() => {
                getAllTasks().then((data) => {
                    this.props.dispatch(allTasks(data))
                })
            });
        this.handleClickClose();
    };

    render() {
        return (
            <div>
                <Button variant="contained" className="pull-right" onClick={this.handleClickOpen}>{this.props.title}</Button>
                <Dialog fullWidth={true} maxWidth={"md"} open={this.props.isOpen} onClose={this.handleClickClose} >
                    <DialogTitle id="form-dialog-title">{this.props.title}</DialogTitle>
                    <DialogContent>
                        <ScrapeRequestForm cabinClasses={this.props.cabinClasses} handleAdd={this.handleAddRequest}/>

                        {this.renderRequests()}


                        <FormControl className={"margin-top-15"} fullWidth={true}>
                            <InputLabel>Scrapers</InputLabel>
                            <Select multiple
                                    value={this.state.scraperTypes}
                                    onChange={this.scraperTypeChange}
                            >
                                {this.renderScraperTypesSelections()}
                            </Select>
                        </FormControl>


                        <FormControl className={"margin-top-15"} fullWidth={true}>
                            <InputLabel id="browser-selection-label">Browser</InputLabel>
                            <Select labelId="browser-selection-label"
                                value={this.state.browser}
                                onChange={this.handleBrowserChange}>
                                {this.renderBrowserSelections()}
                            </Select>
                        </FormControl>


                    </DialogContent>
                    <DialogActions>
                        <Button variant={"outlined"} color={"primary"} onClick={this.handleFormSubmit}>Submit</Button>
                        <Button onClick={this.handleClickClose} variant={"outlined"}>
                            Cancel
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        isOpen: state.tasks.isOpen,
        modalData: state.tasks.modalData,
        scraperTypes: state.tasks.scraperTypes,
        cabinClasses: state.tasks.cabinClasses
    }
};

export default connect(mapStateToProps)(TaskForm)