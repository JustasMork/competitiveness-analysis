import React, {Component} from "react";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import TableContainer from "@material-ui/core/TableContainer";
import {Paper} from "@material-ui/core";

class ScrapeRequestItems extends Component{

    getItineraryString = (itinerary) => {
      return itinerary.map((item) => {
          return "[" + item.origin.iata + " - " + item.destination.iata + " " + item.departure + "]"
      }).join(", ");
    };

    getTravellersString = (travellers) => {
        return Object.keys(travellers).map((item) => {
            return item + ":" + travellers[item];
        }).join(", ");
    };

    renderItems = () => {
        return this.props.items.map((item, index) => {
            console.log(item);
            return (
                <TableRow>
                    <TableCell>{index + 1}</TableCell>
                    <TableCell>{item.cabinClass}</TableCell>
                    <TableCell>{this.getItineraryString(item.itinerary)}</TableCell>
                    <TableCell>{this.getTravellersString(item.travellers)}</TableCell>
                </TableRow>
            );
        })
    };

    render() {
        return (
            <TableContainer className={"margin-top-15"} component={Paper}>
                <Table size={"small"}>
                    <TableHead>
                        <TableRow>
                            <TableCell>#</TableCell>
                            <TableCell>Cabin class</TableCell>
                            <TableCell>Itinerary</TableCell>
                            <TableCell>Travellers</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {this.renderItems()}
                    </TableBody>
                </Table>
            </TableContainer>
        );
    }

}

export default ScrapeRequestItems;