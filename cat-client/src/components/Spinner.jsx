import React, {Component} from "react";
import CircularProgress from "@material-ui/core/CircularProgress";

class Spinner extends Component{
    render() {
        return(
            <div className={"spinner-container"}>
                <CircularProgress size={this.props.size} />
            </div>
        );
    }
}

export default Spinner;