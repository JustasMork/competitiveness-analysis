import React, {Component} from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import {Link} from "react-router-dom";
import Button from "@material-ui/core/Button";

class Header extends Component {

    renderMenuItems = (paths) => {
        return paths.map((item) => {
            if (item.name) {
                return <Link to={item.path}><Button className={"menu-item"}>{item.name}</Button></Link>
            }
        })
    };

    render() {
        return (

            <AppBar position="static">
                <Toolbar>
                    <Grid container spacing={3}>
                        <Grid item xs={6} sm={6} >
                            <Typography variant="h6">
                                CA dashboard
                            </Typography>
                        </Grid>
                        <Grid item alignContent='flex-end' xs={6} sm={6}>
                            <div className="pull-right">
                                {this.renderMenuItems(this.props.paths)}
                            </div>
                        </Grid>
                    </Grid>
                </Toolbar>
            </AppBar>
        );
    }
}

export default Header;