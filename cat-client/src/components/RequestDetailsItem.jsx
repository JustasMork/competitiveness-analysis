import React, {Component} from "react";
import {getTaskItemDetailsString} from "../helpers/FormatHelper";
import InfoIcon from '@material-ui/icons/Info';
import Tooltip from "@material-ui/core/Tooltip";
import moment from "moment";
import {withStyles} from "@material-ui/core";
import FlightTakeoffIcon from '@material-ui/icons/FlightTakeoff';
import FlightLandIcon from '@material-ui/icons/FlightLand';
import EventNoteIcon from '@material-ui/icons/EventNote';

const HtmlTooltip = withStyles((theme) => ({
    tooltip: {
        backgroundColor: '#CCCCCC',
        color: '#212121',
        fontSize: theme.typography.pxToRem(12),
        border: '1px solid #dadde9',
    },
}))(Tooltip);

class RequestDetailsItem extends Component{

    renderTooltip = () => {
        return (
            <div className={"tooltip-details"}>
                {this.props.requests.map(request => this.renderSingleRequest(request))}
            </div>

        );
    };

    renderSingleRequest = (request) => {
        return (
            <div className={"request-item"}>
                {request.itinerary.map(itin => this.renderItineraryDetails(itin))}
            </div>
        );
    };

    renderItineraryDetails = (itinerary) => {
        return (
            <div className={"itinerary-details"}>
                <div className={"item origin"}><FlightTakeoffIcon/><span>{itinerary.origin.iata}</span></div>
                <span className={"separator"}>-</span>
                <div className={"item destination"}><span>{itinerary.destination.iata}</span><FlightLandIcon/></div>
                <div className={"item arrival"}><span>{moment(itinerary.departure).format("YYYY-MM-DD")}</span><EventNoteIcon/></div>
            </div>
        );
    };


    render() {
        return (
          <div className={"request-details-container"}>
            <div className={"truncated-details-container"}>
                <span className={"truncated-details"}>{getTaskItemDetailsString(this.props.requests)}</span>
            </div>
                <HtmlTooltip title={this.renderTooltip()}>
                    <InfoIcon/>
                </HtmlTooltip>


          </div>
        );
    }
}

export default RequestDetailsItem;