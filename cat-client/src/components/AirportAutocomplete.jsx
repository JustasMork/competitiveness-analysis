import React, {Component} from "react";
import Autocomplete from "@material-ui/lab/Autocomplete";
import TextField from "@material-ui/core/TextField";
import {getAirportOptions} from "../redux/restActions";


class AirportAutocomplete extends Component {

    constructor(props) {
        super(props);
        this.state = {
            value: props.value ? props.value : null,
            airports: []
        };
    }

    handleInputChange = (e) => {
        if (e && e.target && e.target.value && e.target.value.length >= 3) {
            getAirportOptions(e.target.value)
                .then((response) => {
                    this.state.airports = response;
                    this.setState(this.state);
                })
        }
    };

    handleValueChange = (e, value) => {
        this.state.value = value ? value.iata : null;
        if (this.props.update) {
            this.props.update(this.state.value)
        }
        this.setState(this.state);
    };

    renderInput = (params) => {
        return (
            <TextField label={this.props.title} margin="normal" value={this.state.value} {...params} />
        );
    };

    formatOptionLabel = (option) => {
        if (option)
            return  option.iata + " " + option.name;
        return "";
    };

    render() {
        return (
            <Autocomplete
                renderInput={this.renderInput}
                onChange={this.handleValueChange}
                getOptionLabel={this.formatOptionLabel}
                noOptionsText={"No options"}
                options={this.state.airports}
                onInputChange={this.handleInputChange}

            />
        )
    }

}


export default AirportAutocomplete;
