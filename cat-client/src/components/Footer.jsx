import React, {Component} from "react";

class Footer extends Component{
    render() {
        return(
            <footer className={"footer"}>
                <div className={"centered-content"}>
                    <span>Made with ♥ in Kaunas!</span>
                </div>
            </footer>
        );
    }
}

export default Footer;