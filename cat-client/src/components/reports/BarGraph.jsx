import React, {Component} from "react";
import {
    Crosshair,
    DiscreteColorLegend,
    FlexibleWidthXYPlot, HorizontalGridLines,
    VerticalBarSeries, VerticalGridLines,
    XAxis,
    YAxis
} from "react-vis";
import Grid from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardHeader from "@material-ui/core/CardHeader";


class BarGraph extends Component {

    constructor(props) {
        super(props);
        this.state = {
            crosshair: []
        }
    }

    renderEntries = () => {
        return this.props.entries.map((item, index) => {
            if (index == 0) {
                return <VerticalBarSeries data={item} onNearestX={this.onNearestX}/>
            }
            return <VerticalBarSeries data={item}/>
        })
    };

    onMouseLeave = () => {
        this.state.crosshair = [];
        this.setState(this.state);
    };

    onNearestX = (value, {index}) => {
        this.state.crosshair = this.props.entries.map(entry => entry[index]);
        console.log(this.state.crosshair);
        this.setState(this.state);
    };

    renderCrosshair = () => {
        if (this.state.crosshair.length > 0) {
            return (
                <div className={"graph-info-box"}>
                    <h3>{this.state.crosshair[0].x}</h3>
                    {this.state.crosshair.map((val, index) => {
                        return <p>{this.props.labelItems[index].title}: {this.state.crosshair[index].y}</p>
                    })}
                </div>
            );
        }
    };

    render() {
        return (
            <Grid item md={4} sm={6} xs={12} className={"graph-item-container"}>
                <Card className={"graph-item"}>
                    <CardHeader title={this.props.title} titleTypographyProps={{variant: "h7"}}/>
                    <CardContent>
                        <DiscreteColorLegend className={"legends-container"}
                            items={this.props.labelItems || []}
                        />
                        <Grid item sm={12} className="margin-top-15">
                            <FlexibleWidthXYPlot
                                height={350}
                                xType="ordinal"
                                onMouseLeave={this.onMouseLeave}
                            >
                                <XAxis/>
                                <YAxis/>
                                <VerticalGridLines />
                                <HorizontalGridLines />
                                {this.renderEntries()}
                                <Crosshair values={this.state.crosshair}>
                                    {this.renderCrosshair()}
                                </Crosshair>
                            </FlexibleWidthXYPlot>
                        </Grid>
                    </CardContent>
                </Card>
            </Grid>
        );
    }
}

export default BarGraph;