import React, {Component} from "react";
import Grid from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import {
    DiscreteColorLegend,
    FlexibleWidthXYPlot,
    LineMarkSeries,
    VerticalGridLines,
    HorizontalGridLines,
    YAxis} from "react-vis/es";
import {Crosshair} from "react-vis";


class LineGraph extends Component {

    constructor(props) {
        super(props);
        this.state = {
            crosshair: []
        }
    }

    renderEntries = () => {
        return this.props.entries.map((item, index) => {
            if (index === 0) {
                return (<LineMarkSeries style={{
                    strokeLinejoin: 'round',
                    strokeWidth: 2
                }} data={item}
                onNearestX={this.onNearestX}
                />);
            } else {
                return <LineMarkSeries style={{
                    strokeLinejoin: 'round',
                    strokeWidth: 2
                }} data={item}/>
            }
        })
    };

    onMouseLeave = () => {
        this.state.crosshair = [];
        this.setState(this.state);
    };

    onNearestX = (value, {index}) => {
        this.state.crosshair = this.props.entries.map(entry => entry[index]);
        console.log(this.state.crosshair);
        this.setState(this.state);
    };

    renderCrosshair = () => {
        if (this.state.crosshair.length > 0) {
            return (
                <div className={"graph-info-box"}>
                    <h3>{this.state.crosshair[0].x}</h3>
                    {this.state.crosshair.map((val, index) => {
                        return <p>{this.props.labelItems[index].title}: {this.state.crosshair[index].y} {this.state.crosshair[index].unit || ""}</p>
                    })}
                </div>
            );
        }
    };

    render() {
        return (
            <Grid item sm={4} xs={6} alignItems={"stretch"} className={"graph-item-container"}>
                <Card className={"graph-item"}>
                    <CardHeader title={this.props.title} titleTypographyProps={{variant: "h7"}}/>
                    <CardContent>
                        <Grid item sm={12}>
                            <DiscreteColorLegend className={"legends-container"}
                                                 items={this.props.labelItems || []}
                            />
                            <FlexibleWidthXYPlot
                                height={350}
                                xType="ordinal"
                                onMouseLeave={this.onMouseLeave}
                            >
                                <VerticalGridLines />
                                <HorizontalGridLines />
                                <YAxis/>
                                {this.renderEntries()}
                                <Crosshair values={this.state.crosshair}>
                                    {this.renderCrosshair()}
                                </Crosshair>
                            </FlexibleWidthXYPlot>
                        </Grid>
                    </CardContent>
                </Card>
            </Grid>
        );
    }
}

export default LineGraph;