import React, {Component} from "react";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import {getLocalesString} from "../../helpers/FormatHelper";
import Button from "@material-ui/core/Button";
import {Link} from "react-router-dom";
import RequestDetailsItem from "../RequestDetailsItem";

class ReportItem extends Component{
    render() {
        if (this.props.report) {
            return (
                <TableRow>
                    <TableCell>{this.props.report.id}</TableCell>
                    <TableCell>{this.props.report.scraperTypes.join(", ")}</TableCell>
                    <TableCell>{getLocalesString(this.props.report.requests)}</TableCell>
                    <TableCell>{this.props.report.browser}</TableCell>
                    <TableCell>
                        <RequestDetailsItem requests={this.props.report.requests}/>
                    </TableCell>
                    <TableCell className={"no-wrap"}>{this.props.report.dateCompleted}</TableCell>
                    <TableCell>
                        <Link className={"route-link"} to={"/reports/" + this.props.report.id}><Button variant={"outlined"} size={"small"}>Open</Button></Link>
                    </TableCell>
                </TableRow>
            )
        }
    }
}

export default ReportItem;