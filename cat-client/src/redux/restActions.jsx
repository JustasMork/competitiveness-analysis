import axios from 'axios';

const BASE_URL = "http://localhost:8080";

const AXIOS_CONFIG = {mode: 'no-cors'};

let getAllTasks = ()  => {
    return axios.get(BASE_URL + "/scrape/tasks/all", AXIOS_CONFIG)
        .then((response) => {
            return response.data;
        })
        .catch((error) => {
            console.log(error);
        })
};

let postTask = (data) => {
  return axios.post(BASE_URL + "/scrape/tasks", data, AXIOS_CONFIG)
      .then((response) => {
          console.log(response);
      })
      .catch((error) => {
          console.log(error);
      })
};

let getScraperTypes = () => {
    return axios.get(BASE_URL + "/static/scraperTypes", AXIOS_CONFIG)
        .then((response) => {
            return response.data;
        })
        .catch((error) => {
            console.log(error);
        })
};

let getCabinClasses = () => {
    return axios.get(BASE_URL + "/static/cabinClass", AXIOS_CONFIG)
        .then((response) => {
            return response.data;
        })
        .catch((error) => {
            console.log(error);
        })
};

let getAirportOptions = (query) => {
    let params = {params: {query: query}};
    return axios.get(BASE_URL + "/static/airports", params)
        .then((response) => {
            return response.data
        })
        .catch((error) => {
            console.log(error);
        })
};

let getTransferDetails = (reportId) => {
  return axios.get(BASE_URL + "/reports/"+reportId, AXIOS_CONFIG)
      .then((response) => {
          return response.data;
      })
      .catch((error) => {
          console.log(error);
      })
};

export {getAllTasks, postTask, getScraperTypes, getCabinClasses, getAirportOptions, getTransferDetails};
