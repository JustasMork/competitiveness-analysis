import {combineReducers} from "redux";
import TasksReducer from "./TasksReducer";
import ReportsReducer from "./ReportsReducer";

export default combineReducers({
    tasks: TasksReducer,
    reports: ReportsReducer,
})