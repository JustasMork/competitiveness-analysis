import {ALL_TASKS, SET_TRANSFER_DETAILS} from "../actions";
import cloneDeep from "lodash/cloneDeep";

const initialState = {
  reports: [],
    reportDetails: {
      transferDetails: []
    }
};

export default (state = initialState, action) => {
    let stateClone = cloneDeep(state);
    switch (action.type) {
        case ALL_TASKS:
            stateClone.reports = action.data;
            return stateClone;
        case SET_TRANSFER_DETAILS:
            stateClone.reportDetails.transferDetails = action.data;
            return stateClone;
    }

    return state;
}