import {
    ADD_TASK,
    ALL_TASKS, AVAILABLE_CABIN_CLASSES,
    AVAILABLE_SCRAPER_TYPES,
    GET_AIRPORT_OPTIONS,
    SET_AUTOCOMPLETE_VALUE, SET_SCRAPER_TYPES,
    TOGGLE_TASKS_MODAL
} from "../actions";
import cloneDeep from "lodash/cloneDeep";
import {FIREFOX} from "../../data/Browser";

const initialState = {
    tasks: [],
    isOpen: false,
    modalData: {
        browser: FIREFOX.value,
        scraperTypes: [],
        requests: []
    },
    scraperTypes: [],
    cabinClasses: [],
    airports: [],
    itineraryData: {
        origin: "",
        destination: "",
        departure: ""
    }
};

export default (state = initialState, action) => {
    let stateClone = cloneDeep(state);
    switch (action.type) {
        case ALL_TASKS:
            stateClone.tasks = action.data;
            return stateClone;
        case TOGGLE_TASKS_MODAL:
            stateClone.isOpen = action.isOpen;
            return stateClone;
        case AVAILABLE_SCRAPER_TYPES:
            stateClone.scraperTypes = action.data;
            return stateClone;
        case AVAILABLE_CABIN_CLASSES:
            stateClone.cabinClasses = action.data;
            return stateClone;
        case GET_AIRPORT_OPTIONS:
            stateClone.airports = action.data;
            return stateClone;
        case SET_AUTOCOMPLETE_VALUE:
            stateClone.itineraryData[action.prop] = action.value;
            return stateClone;
        case SET_SCRAPER_TYPES:
            stateClone.modalData.scraperTypes = action.data;
            return stateClone;
    }

    return state;
}