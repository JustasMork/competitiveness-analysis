
export const ALL_TASKS = "ALL_TASKS";
export const ADD_TASK = "ADD_TASK";

export const TOGGLE_TASKS_MODAL = "OPEN_TASKS_MODAL";
export const AVAILABLE_SCRAPER_TYPES = "AVAILABLE_SCRAPER_TYPES";
export const AVAILABLE_CABIN_CLASSES = "AVAILABLE_CABIN_CLASSES";
export const GET_AIRPORT_OPTIONS = "GET_AIRPORT_OPTIONS";
export const SET_AUTOCOMPLETE_VALUE = "SET_AUTOCOMPLETE_VALUE";
export const SET_SCRAPER_TYPES = "SET_SCRAPER_TYPES";

export const SET_TRANSFER_DETAILS = "SET_TRANSFER_DETAILS";

export function allTasks (data) {
    return {
        type : ALL_TASKS,
        data : data
    }
}

export function toggleTasksModal(isOpen) {
    return {
        type: TOGGLE_TASKS_MODAL,
        isOpen : isOpen
    }
}

export function availableScraperTypes(data) {
    return {
        type: AVAILABLE_SCRAPER_TYPES,
        data: data
    }
}

export function availableCabinClasses(data) {
    return {
        type: AVAILABLE_CABIN_CLASSES,
        data: data
    }
}

export function setReportTransferDetails(data) {
    return {
        type: SET_TRANSFER_DETAILS,
        data: data
    }
}

export function airportsAutocomplete(data) {
    return {
        type: GET_AIRPORT_OPTIONS,
        data: data
    }
}

export function setScraperTypes(data) {
    return {
        type: SET_SCRAPER_TYPES,
        data: data
    }
}

export function setAutocompleteValue(prop, value) {
    return {
        type: SET_AUTOCOMPLETE_VALUE,
        prop: prop,
        value: value
    }
}