import moment from "moment";

let getTravellersString = (travellers) => {
    return Object.keys(travellers).map((item) => {
        return item + ":" + travellers[item];
    }).join(", ");
};

let getItineraryString = (item) => {

    return item.origin.iata + " - " + item.destination.iata + " " + moment(item.departure).format("YYYY-MM-DD");
};

let getScrapingRequestString = (request) => {
  return "["+ request.itinerary.map(item => {
      return "[ "+ getItineraryString(item) + " ]";
  }).join(", ") + " " + getTravellersString(request.travellers) + "]";
};

let getTaskItemDetailsString = (requests) => {
    return requests.map((request) => {
        return getScrapingRequestString(request);
    }).join(", ");
};

let getLocalesString = (requests) => {
    if (requests)
        return requests.map((request) => {
            return request.countryCode;
        }).join(", ");
    return "";
};

export {getTravellersString, getItineraryString, getScrapingRequestString, getTaskItemDetailsString, getLocalesString};