import React, {Component} from "react";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableBody from "@material-ui/core/TableBody";
import TableContainer from "@material-ui/core/TableContainer";
import TableCell from "@material-ui/core/TableCell";
import {connect} from "react-redux";
import {getAllTasks} from "../redux/restActions";
import {allTasks} from "../redux/actions";
import ReportItem from "../components/reports/ReportItem";
import Spinner from "../components/Spinner";
import {Paper} from "@material-ui/core";

class Reports extends Component {

    tableColumns = [
        {name: "ID"},
        {name: "Scrapers"},
        {name: "Locales"},
        {name: "Browser"},
        {name: "Task details"},
        {name: "Created"},
        {name: "Actions"}
    ];

    componentDidMount() {
        getAllTasks().then((data) => {
            this.props.dispatch(allTasks(data));
        })
    }

    renderTableHeader = () => {
        return this.tableColumns.map((item) => {
            return (
                <TableCell>{item.name}</TableCell>
            );
        })
    };

    renderTableItems = (data) => {
        if (data) {
            return data.map((item, index) => {
                if (item.started == 2)
                    return <ReportItem report={item} key={"taskItem" + index}/>
            })
        }
    };

    renderTable = () => {
        if (this.props.reports) {
            return (
                <TableContainer className="margin-top-15" component={Paper}>
                    <Table>
                        <TableHead>
                            <TableRow>{this.renderTableHeader()}</TableRow>
                        </TableHead>
                        <TableBody>
                            {this.renderTableItems(this.props.reports)}
                        </TableBody>
                    </Table>
                </TableContainer>
            );
        } else {

        }
    };

    renderContent = () => {
        if (this.props.reports) {
            return (
                <Grid item sm={10}>
                    <Typography variant="h5">Analysis reports</Typography>
                    {this.renderTable()}
                </Grid>
            )
        } else {
            return <Spinner size={100}/>;
        }
    }

    render(){
        return this.renderContent();
    }
}

const mapStateToProps = (state) => {
    return {reports: state.reports.reports};
};

export default connect(mapStateToProps)(Reports);