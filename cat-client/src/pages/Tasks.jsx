import React, {Component} from "react";
import TaskItem from "../components/tasks/TaskItem";
import {connect} from "react-redux";
import {getAllTasks, getCabinClasses, getScraperTypes} from "../redux/restActions";
import {allTasks, availableCabinClasses, availableScraperTypes} from "../redux/actions";
import Grid from "@material-ui/core/Grid";
import Table from "@material-ui/core/Table";
import Typography from "@material-ui/core/Typography";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import TaskForm from "../components/tasks/TaskForm";
import Spinner from "../components/Spinner";
import {Paper} from "@material-ui/core";


class Tasks extends Component{

    tableColumns = [
        {name: "ID"},
        {name: "Scrapers"},
        {name: "Locales"},
        {name: "Browser"},
        {name: "Task details"},
        {name: "Status"},
        {name: "Date created"},
    ];

    componentDidMount() {
        getAllTasks().then((data) => {
            this.props.dispatch(allTasks(data))
        });
        getScraperTypes().then((data) => {
            this.props.dispatch(availableScraperTypes(data))
        });
        getCabinClasses().then((data) => {
            this.props.dispatch(availableCabinClasses(data));
        });
    };

    renderTableHeader = () => {
        return this.tableColumns.map((item) => {
            return (
                <TableCell>{item.name}</TableCell>
            );
        })
    };

    renderTableItems = (data) => {
        if (data) {
            return data.map((item, index) => {
                return <TaskItem task={item} key={"taskItem" + index}/>
            })
        }
    };

    renderTable = () => {
        if (this.props.tasks.length > 0) {
            return (
                <TableContainer className="margin-top-15" size={"small"} component={Paper}>
                    <Table size={"small"}>
                        <TableHead>
                            <TableRow>{this.renderTableHeader()}</TableRow>
                        </TableHead>
                        <TableBody>
                            {this.renderTableItems(this.props.tasks)}
                        </TableBody>
                    </Table>
                </TableContainer>
            );
        }

    };

    renderContent = () => {
        if (this.props.tasks && this.props.tasks.length > 0) {
            return (
                <Grid item sm={10} className={"margin-top-15"}>
                    <Grid container xs={12} sm={12} justify={"center"}>
                        <Grid item xs={8}>
                            <Typography variant="h5">Scraping tasks</Typography>
                        </Grid>
                        <Grid item sm={4} xs={4}>
                            <TaskForm title={"New scraping request"}/>
                        </Grid>
                    </Grid>
                    {this.renderTable()}
                </Grid>
            )
        }
        return <Spinner size={100}/>
    }

    render(){
        return this.renderContent();
    };
}

const mapStateToProps = (state) => {
    return {tasks: state.tasks.tasks};
};

export default connect(mapStateToProps)(Tasks)