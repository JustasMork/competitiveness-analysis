import {createMuiTheme} from "@material-ui/core";

export const customTheme = createMuiTheme({
    palette: {
        primary: {
            light: "#FF690F",
            main: "#FF690F",
            dark: "#212121",
        },
        secondary: {
            light: "#CCCCCC",
            main: "#CC540C",
            dark: "#1A1A1A"
        }
    }
});