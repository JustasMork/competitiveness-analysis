import React, {Component} from "react";
import BarGraph from "../components/reports/BarGraph";
import Grid from "@material-ui/core/Grid";
import {getTransferDetails} from "../redux/restActions";
import {setReportTransferDetails} from "../redux/actions";
import {connect} from "react-redux";
import LineGraph from "../components/reports/LineGraph";
import Spinner from "../components/Spinner";

class ReportDetails extends Component{

    componentDidMount() {
        getTransferDetails(this.props.match.params.id)
            .then((data) => {
                this.props.dispatch(setReportTransferDetails(data))
            })
    }

    renderGraphs = () => {
        if (this.props.reportDetails.transferDetails.length > 0) {
            return this.props.reportDetails.transferDetails.filter(item => item).map((table) => {
                switch (table.type) {
                    case "BAR":
                        return (
                            <BarGraph title={table.tableName} entries={table.entries} labelItems={table.labels}/>
                        );
                    case "LINE":
                        return (
                          <LineGraph title={table.tableName} entries={table.entries} labelItems={table.labels}/>
                        );
                }
            })
        } else {
            return <Spinner size={100}/>
        }
    };


    render() {
        return (
            <Grid item sm={10} className="margin-top-15">
                <Grid container alignItems={"stretch"}  spacing={1} >
                    {this.renderGraphs()}
                </Grid>
            </Grid>
        )
    }
}

const mapStateToProps = (state) => {
    return {reportDetails: state.reports.reportDetails};
};

export default connect(mapStateToProps)(ReportDetails);