import React from 'react';
import './App.css';
import {Route, Switch} from "react-router-dom";
import { BrowserRouter as Router } from 'react-router-dom';
import Tasks from "./pages/Tasks";
import Reports from "./pages/Reports";
import Header from "./components/Header";
import './styles/styles.scss';
import Grid from "@material-ui/core/Grid";
import ReportDetails from "./pages/ReportDetails";
import Footer from "./components/Footer";

const paths = [
    {path: "/tasks", component: Tasks, name: "Tasks"},
    {path: "/reports/:id", component: ReportDetails},
    {path: "/reports", component: Reports, name: "Reports"}

];

const RenderRoutes = (paths) => {
    return paths.map((item) => {
        return (
            <Route path={item.path} component={item.component}/>
        )
    })
}

const App = () => {

    return (
        <div className={"flex-wrapper"}>
            <Router>
                <Header paths={paths}/>
                <Grid container justify="center" className="margin-top-15">
                    <Switch>
                        {RenderRoutes(paths)}
                    </Switch>
                </Grid>
                <Footer/>
            </Router>
        </div>
    )
}

export default App;
