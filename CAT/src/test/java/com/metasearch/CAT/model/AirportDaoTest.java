package com.metasearch.CAT.model;

import com.catModel.Airport;
import com.metasearch.CAT.TestBase;
import com.metasearch.CAT.model.DAO.AirportDao;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@Disabled("Should be executed manually")
class AirportDaoTest extends TestBase {

    @Autowired
    AirportDao airportDao;

    @Test
    void testAirportInsert() {
        Airport airport = new Airport();
        airport.setName("Test airport");
        airport.setRegion("Test region");
        airport.setCountry("Lithuania");
        airport.setCountryCode("lt");
        airport.setIata("VNO");
        airport.setIcao("VNOA");
        airport.setCity("Vilnius");

        airportDao.insertAirport(airport);
    }

    @Test
    void testGetAirport() {
        Airport airport = airportDao.getAirport("VNO");
        assertNotNull(airport);
    }

    @Test
    void testUpdateAirport() {
        Airport airport = airportDao.getAirport("VNO");

        airport.setCountry("TEST COUNTRY");
        airportDao.updateAirport(airport);
    }
}
