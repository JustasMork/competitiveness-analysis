package com.metasearch.CAT.model.clients;

import com.catModel.FlightSearchResult;
import com.catModel.ScrapeResults;
import com.catModel.ScraperType;
import com.catModel.Trip;
import com.metasearch.CAT.TestBase;
import com.metasearch.CAT.general.DaoManager;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@Disabled("Should be executed manually")
class ScrapeResultsDaoClientTest extends TestBase {

    @Test
    void testScrapeResultsInsert() {
        ScrapeResults results = new ScrapeResults();
        results.setId(8);

        FlightSearchResult result = new FlightSearchResult();
        result.setScraperType(ScraperType.SKYSCANNER);
        result.setScrapingTime(new Date());
        result.setResultLoadingTime(200);
        results.addResult(result);

        Trip trip = new Trip();
        result.addTrip(trip);

        DaoManager.getInstance().getScrapeResultsDao().insert(results);
    }

    @Test
    void testGetResults() {
        List<FlightSearchResult> results = DaoManager.getInstance().getScrapeResultsDao().getFlightResults(15);

        assertNotNull(results);
    }

}