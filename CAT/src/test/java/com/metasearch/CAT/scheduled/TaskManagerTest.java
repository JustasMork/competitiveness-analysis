package com.metasearch.CAT.scheduled;

import com.metasearch.CAT.TestBase;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

@Disabled("Should be executed manually")
class TaskManagerTest extends TestBase {

    @Autowired
    TaskManager taskManager;

    @Test
    void testHourlyTasksExecution() throws InterruptedException {
        taskManager.executeHourly();
    }

    @Test
    void testDailyTasksExecution() throws InterruptedException {
        taskManager.executeDaily();
    }

    @Test
    void testWeeklyTasksExecution() throws InterruptedException {
        taskManager.executeWeekly();
    }

    @Test
    void testMonthlyTasksExecution() throws InterruptedException {
        taskManager.executeMonthly();
    }
}