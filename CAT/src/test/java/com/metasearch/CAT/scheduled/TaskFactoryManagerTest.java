package com.metasearch.CAT.scheduled;

import com.metasearch.CAT.TestsEnclosedBase;
import com.metasearch.CAT.scheduled.tasks.airportImport.AirportImportTask;
import com.metasearch.CAT.scheduled.tasks.Task;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

class TaskFactoryManagerTest extends TestsEnclosedBase {

    @Autowired
    TaskFactory factory;

    @Test
    void testAirportImportTaskCreation() {
        String taskName = "airportImport";
        String className = (AirportImportTask.class).getName();

        Task task = factory.createTask(taskName, className);

        assertNotNull(task);
        assertTrue(task instanceof AirportImportTask);
    }

}