package com.metasearch.CAT.scheduled;

import com.metasearch.CAT.TestsEnclosedBase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;


class TaskInitializationConfigurationTest extends TestsEnclosedBase {

    @Autowired
    TaskInitializationConfig taskInitializationConfiguration;

    @Test
    void testGetEnabledFactories() {
        Map<String, String> enabledFactories = taskInitializationConfiguration.getEnabledFactories();

        assertNotNull(enabledFactories.get("TASKNAME1"));
        assertNotNull(enabledFactories.get("TASKNAME2"));
        assertNotNull(enabledFactories.get("TASKNAME3"));

        assertEquals("CLASSPATH1", enabledFactories.get("TASKNAME1"));
        assertEquals("CLASSPATH2", enabledFactories.get("TASKNAME2"));
        assertEquals("CLASSPATH3", enabledFactories.get("TASKNAME3"));
    }

    @Test
    void testGetEnabledFactoriesByExecutionIntervalHourly() {
        Map<String, String> factories = taskInitializationConfiguration.getEnabledFactories(ExecutionInterval.HOURLY);

        assertNotNull(factories);
        assertEquals(1, factories.size());
        assertTrue(factories.containsKey("hourlyTask"));
        assertEquals("hourlyTaskClassPath", factories.get("hourlyTask"));
    }

    @Test
    void testGetEnabledFactoriesByExecutionIntervalDaily() {
        Map<String, String> factories = taskInitializationConfiguration.getEnabledFactories(ExecutionInterval.DAILY);

        assertNotNull(factories);
        assertEquals(1, factories.size());
        assertTrue(factories.containsKey("dailyTask"));
        assertEquals("dailyTaskClassPath", factories.get("dailyTask"));
    }

    @Test
    void testGetEnabledFactoriesByExecutionIntervalWeekly() {
        Map<String, String> factories = taskInitializationConfiguration.getEnabledFactories(ExecutionInterval.WEEKLY);

        assertNotNull(factories);
        assertEquals(1, factories.size());
        assertTrue(factories.containsKey("weeklyTask"));
        assertEquals("weeklyTaskClassPath", factories.get("weeklyTask"));
    }

    @Test
    void testGetEnabledFactoriesByExecutionIntervalMonthly() {
        Map<String, String> factories = taskInitializationConfiguration.getEnabledFactories(ExecutionInterval.MONTHLY);

        assertNotNull(factories);
        assertEquals(1, factories.size());
        assertTrue(factories.containsKey("monthlyTask"));
        assertEquals("monthlyTaskClassPath", factories.get("monthlyTask"));
    }
}
