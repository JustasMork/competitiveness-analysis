package com.metasearch.CAT.scheduled.tasks;

import com.metasearch.CAT.TestsEnclosedBase;
import com.metasearch.CAT.scheduled.TaskContext;
import com.metasearch.CAT.scheduled.tasks.airportImport.AirportImportConfig;
import com.metasearch.CAT.scheduled.tasks.airportImport.AirportImportTask;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.mock;

@Disabled("Should be executed manually")
class AirportImportTaskTest extends TestsEnclosedBase {

    @Test
    void testAirportImport() {
        AirportImportConfig config = new AirportImportConfig("airportImport");
        AirportImportTask airportImport = new AirportImportTask(config);

        airportImport.runTask(mock(TaskContext.class));
    }

}