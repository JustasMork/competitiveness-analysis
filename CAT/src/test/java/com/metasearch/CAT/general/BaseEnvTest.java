package com.metasearch.CAT.general;

import com.metasearch.CAT.TestsEnclosedBase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class BaseEnvTest extends TestsEnclosedBase {

    private static final String PROP_PREFIX = "env.tests.";

    @Autowired
    Env env;

    @Test
    void testGetProp() {
        assertEquals("testProperty", env.getProp(PROP_PREFIX + "getPropTest"));
    }

    @Test
    void testGetPropUndefined() {
        assertNull(env.getProp(PROP_PREFIX + "testGetPropUndefined"));
    }

    @Test
    void testGetPropDefault() {
        assertEquals("expectedProp", env.getProp(PROP_PREFIX + "testGetPropUndefined", "expectedProp"));
    }

    @Test
    void testGetBoolTestTrue() {
        assertTrue(env.getBoolean(PROP_PREFIX + "getBoolTestTrue"));
    }

    @Test
    void testGetBoolTestFalse() {
        assertFalse(env.getBoolean(PROP_PREFIX + "getBoolTestFalse"));
    }

    @Test
    void testGetBoolRandom() {
        assertFalse(env.getBoolean(PROP_PREFIX + "getBoolTestUnknown"));
    }

    @Test
    void testGetBoolRandomDefault() {
        assertFalse(env.getBoolean(PROP_PREFIX + "getBoolUndefined", false));
        assertTrue(env.getBoolean(PROP_PREFIX + "getBoolUndefined", true));
    }

    @Test
    void testGetList() {
        List<String> list = env.getList(PROP_PREFIX + "getListTest");
        List<String> expectedList = List.of("first", "second", "third");

        assertLists(expectedList, list);
    }

    @Test
    void testGetListDefault() {
        List<String> expectedList = List.of("first", "second", "third");
        List<String> list = env.getList(PROP_PREFIX + "getListTestUndefined", expectedList);

        assertNull(env.getList(PROP_PREFIX + "getListTestUndefined"));
        assertLists(expectedList, list);

    }

    private void assertLists(List<String> expectedList, List<String> list) {
        assertNotNull(list);
        assertEquals(expectedList.size(), list.size());

        for (int i = 0; i < list.size(); i++) {
            assertEquals(expectedList.get(i), list.get(i));
        }
    }

}