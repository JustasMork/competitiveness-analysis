package com.metasearch.CAT.general;

import com.catModel.*;
import com.metasearch.CAT.TestBase;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertTrue;

@Disabled("Should be executed manually")
class DaoManagerTest extends TestBase {

    @Test
    void testScrapingRequestInsert() {
        SimpleDateFormat dtFormat = new SimpleDateFormat("YYYY-MM-dd");

        Airport origin = new Airport();
        origin.setIata("VNO");
        origin.setName("Vilnius Airport");
        origin.setCity("Vilnius");
        origin.setCountry("Lithuania");
        origin.setCountryCode("lt");

        Airport destination = new Airport();
        destination.setIata("SEN");
        destination.setName("London Southend Airport");
        destination.setCountry("England");
        destination.setCountryCode("gb");

        ODpair odPair = new ODpair();
        odPair.setOrigin(origin);
        odPair.setDestination(destination);
        try {
            odPair.setDeparture(dtFormat.parse("2020-08-15"));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Map<PTC, Integer> travellers = new HashMap<>();
        travellers.put(PTC.ADT, 2);
        travellers.put(PTC.CHD, 1);

        FlightScrapeRequest request = new FlightScrapeRequest();
        request.setItinerary(List.of(odPair));
        request.setCabinClass(CabinClass.Economy);
        request.setTravellers(travellers);
        request.setCountryCode("lt");

        ScrapeRequests requests = new ScrapeRequests("chrome", List.of(ScraperType.SKYSCANNER));
        requests.addRequest(request);

        DaoManager.getInstance().getScrapeRequestDao().insert(requests);
        assertTrue(true);
    }

    @Test
    void testGetScrapingTask() {
        DaoManager.getInstance().getScrapeRequestDao().getScrapingTask();
    }



}