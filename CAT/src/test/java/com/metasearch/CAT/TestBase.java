package com.metasearch.CAT;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class TestBase extends CatApplicationTests {

    public String getResourcePath() {
        return Paths.get("src", "test", "resources").toFile().getAbsolutePath();
    }

    public String getFileContent(String path) throws IOException {
        return Files.readString(Paths.get(path));
    }
}
