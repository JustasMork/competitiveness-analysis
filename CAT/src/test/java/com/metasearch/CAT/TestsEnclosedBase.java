package com.metasearch.CAT;

import org.springframework.test.context.TestPropertySource;

@TestPropertySource(locations = "classpath:application.test.properties")
public class TestsEnclosedBase extends CatApplicationTests {
}
