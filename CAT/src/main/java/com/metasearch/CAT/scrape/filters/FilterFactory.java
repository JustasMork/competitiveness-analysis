package com.metasearch.CAT.scrape.filters;

import com.metasearch.CAT.general.Config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Constructor;
import java.util.LinkedList;
import java.util.List;

public class FilterFactory {

    private Logger log = LoggerFactory.getLogger(FilterFactory.class);

    private static String PROP_PREFIX = "scrape.filter.factory.";

    private static String RESULTS_FILTER_PROP = PROP_PREFIX + "resultsFilter.";

    private static String TASKS_FILTER_PROP = PROP_PREFIX + "tasksFilter.";

    public List<ResultsFilter> createResultFilters() {
        List<String[]> filterClassPaths = getFilterClassPaths(RESULTS_FILTER_PROP);
        List<ResultsFilter> filters = new LinkedList<>();
        for (String[] classPath : filterClassPaths) {
            try {
                ResultsFilter filter = createFilter(classPath[0], classPath[1], ResultsFilter.class);
                filters.add(filter);
            } catch (Exception e) {
                log.error("Failed to create " + classPath[0]);
            }
        }

        return filters;
    }

    public List<TasksFilter> createTasksFilter() {
        List<String[]> filterClassPaths = getFilterClassPaths(TASKS_FILTER_PROP);
        List<TasksFilter> filters = new LinkedList<>();
        for (String[] classPath : filterClassPaths) {
            try {
                TasksFilter filter = createFilter(classPath[0], classPath[1], TasksFilter.class);
                filters.add(filter);
            } catch (Exception e) {
                log.error("Failed to create " + classPath[0]);
            }
        }

        return filters;
    }

    private <T> T createFilter(String classPath, String name, Class<T> type) throws Exception {
        Class clazz = Class.forName(classPath);
        Constructor[] constructors = clazz.getConstructors();
        return type.cast(constructors[0].newInstance(name));
    }

    private List<String[]> getFilterClassPaths(String propPrefix) {
        int i = 1;
        String currentClassPath = null;
        List<String[]> classPaths = new LinkedList<>();
        while (currentClassPath != null || i == 1) {
            currentClassPath = Config.getInstance().getEnv().getProp(propPrefix + i);
            if (currentClassPath != null) {
                classPaths.add(currentClassPath.split(","));
            }
            i++;
        }
        return classPaths;
    }

}
