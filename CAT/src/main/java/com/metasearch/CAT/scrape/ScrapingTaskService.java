package com.metasearch.CAT.scrape;

import com.catModel.ScrapeRequests;

import java.util.List;

public interface ScrapingTaskService {

    ScrapeRequests getScrapingRequests();

    List<ScrapeRequests> getAllScrapingRequests();

    void addScrapingRequests(ScrapeRequests requests);

    // TODO: add method to restore unfinished tasks. (Use this in scheduled package)

}
