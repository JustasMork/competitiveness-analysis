package com.metasearch.CAT.scrape.filters;

import com.catModel.ScrapeRequests;

public interface TasksFilter {

    boolean isEnabled();

    void filter(ScrapeRequests requests);
}
