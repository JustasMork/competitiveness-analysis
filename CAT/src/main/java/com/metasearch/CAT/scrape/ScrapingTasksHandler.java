package com.metasearch.CAT.scrape;

import com.catModel.ScrapeRequests;
import com.metasearch.CAT.general.DaoManager;
import com.metasearch.CAT.scrape.filters.FilterFactory;
import com.metasearch.CAT.scrape.filters.TasksFilter;
import com.metasearch.CAT.scrape.utils.RequestsValidator;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
public class ScrapingTasksHandler implements ScrapingTaskService {

    @Override
    public ScrapeRequests getScrapingRequests() {
        ScrapeRequests scrapingTask = DaoManager.getInstance().getScrapeRequestDao().getScrapingTask();
        if (scrapingTask != null) {
            FilterFactory factory = new FilterFactory();
            List<TasksFilter> tasksFilter = factory.createTasksFilter();
            tasksFilter.stream()
                    .filter(TasksFilter::isEnabled)
                    .forEach(filter -> filter.filter(scrapingTask));

            return scrapingTask;
        }
        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Scraping tasks not found");
    }

    @Override
    public List<ScrapeRequests> getAllScrapingRequests() {
        List<ScrapeRequests> allScrapingTasks = DaoManager.getInstance().getScrapeRequestDao().getAllScrapingTasks();
        return allScrapingTasks != null ? allScrapingTasks : List.of();
    }

    @Override
    public void addScrapingRequests(ScrapeRequests requests) {
        RequestsValidator validator = new RequestsValidator(requests);
        if (validator.isValid()) {
            DaoManager.getInstance().getScrapeRequestDao().insert(requests);
        } else {
            throw new ResponseStatusException(validator.getStatus(), validator.getFailureMessage());
        }
    }
}
