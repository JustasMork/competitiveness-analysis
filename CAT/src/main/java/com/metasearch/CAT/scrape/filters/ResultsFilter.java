package com.metasearch.CAT.scrape.filters;

import com.catModel.ScrapeResult;
import com.catModel.ScraperType;

public interface ResultsFilter {

    boolean isEnabled();

    default void filterBeforeStoring(ScrapeResult result) {
        // We set this so that each filter would have to override all methods.
    }

    default void filterBeforeRetrieving(ScrapeResult result) {
        // We set this so that each filter would have to override all methods.
    }

}
