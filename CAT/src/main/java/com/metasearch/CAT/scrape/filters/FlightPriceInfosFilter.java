package com.metasearch.CAT.scrape.filters;

import com.catModel.FlightSearchResult;
import com.catModel.PriceInfo;
import com.catModel.ScrapeResult;
import com.catModel.Trip;

public class FlightPriceInfosFilter implements FlightResultsFilter {

    private String filterName;

    public FlightPriceInfosFilter(String name) {
        this.filterName = name;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public void filterBeforeStoring(ScrapeResult result) {
        if (!canBeFiltered(result)) {
            return;
        }

        filterResult((FlightSearchResult) result);
    }

    private boolean canBeFiltered(ScrapeResult result) {
        return result instanceof FlightSearchResult;
    }

    private void filterResult(FlightSearchResult result) {
        for (Trip trip : result.getTrips()) {
            for (PriceInfo priceInfo : trip.getPriceInfos()) {
                   filterPriceInfo(priceInfo);
            }
        }
    }

    private void filterPriceInfo(PriceInfo priceInfo) {
        priceInfo.setProvider(cleanUpProvider(priceInfo.getProvider()));
    }

    private String cleanUpProvider(String provider) {
        return provider.trim();
    }

}
