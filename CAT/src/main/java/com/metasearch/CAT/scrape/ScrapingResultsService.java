package com.metasearch.CAT.scrape;

import com.catModel.ScrapeResults;

public interface ScrapingResultsService {

    void saveResults(ScrapeResults results);

    ScrapeResults getScrapeResults(int requestId);

}
