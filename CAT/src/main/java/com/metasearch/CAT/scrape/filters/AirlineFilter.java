package com.metasearch.CAT.scrape.filters;

import com.catModel.*;
import com.metasearch.CAT.general.DaoManager;

import java.util.List;

public class AirlineFilter implements ResultsFilter{

    private String filterName;

    public AirlineFilter(String name) {
        filterName = name;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public void filterBeforeRetrieving(ScrapeResult result) {
        if (shouldFilter(result)) {
            filterTrips((FlightSearchResult) result);
        }
    }

    @Override
    public void filterBeforeStoring(ScrapeResult result) {
        if (shouldFilter(result)) {
            filterTrips((FlightSearchResult) result);
        }
    }

    private boolean shouldFilter(ScrapeResult result) {
        return result instanceof FlightSearchResult;
    }

    private void filterTrips(FlightSearchResult result) {
        result.getTrips().parallelStream()
                .forEach(trip -> filterLegs(trip.getLegs()));
    }

    private void filterLegs(List<Leg> legs) {
        legs.forEach(leg -> filterSegments(leg.getSegments()));
    }

    private void filterSegments(List<Segment> segments) {
        segments.forEach(this::filterSegment);
    }

    private void filterSegment(Segment segment) {
        cleanupFlightNr(segment);
        if (shouldParseAirline(segment)) {
            String airlineCode = segment.getFlightNr().substring(0, 2);
            Airline airline = DaoManager.getInstance().getAirlineDao().getAirline(airlineCode);
            if (airline != null) {
                segment.setAirline(airline);
            }
        }
    }

    private void cleanupFlightNr(Segment segment) {
        String flightNr = segment.getFlightNr();
        if (flightNr != null) {
            if (flightNr.contains("|")) {
                flightNr = flightNr.substring(0, flightNr.indexOf("|"));
            }
            segment.setFlightNr(flightNr);
        }
    }

    private boolean shouldParseAirline(Segment segment) {
        return segment.getAirline() == null
                || segment.getAirline().getCode() == null;
    }
}
