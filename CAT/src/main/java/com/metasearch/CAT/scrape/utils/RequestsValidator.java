package com.metasearch.CAT.scrape.utils;

import com.catModel.FlightScrapeRequest;
import com.catModel.ODpair;
import com.catModel.ScrapeRequest;
import com.catModel.ScrapeRequests;
import com.google.common.collect.ImmutableList;
import org.springframework.http.HttpStatus;

import java.util.Objects;
import java.util.function.Predicate;

public class RequestsValidator {

    private static ImmutableList<String> browsers = ImmutableList.<String>builder()
            .add("firefox")
            .add("chrome")
            .build();

    private ScrapeRequests requests;

    private String failureMessage;

    private HttpStatus status;

    public RequestsValidator(ScrapeRequests requests) {
        this.requests = requests;
    }

    private Predicate<ScrapeRequests> checkBrowser() {
        return r -> setMetaInfo("Incorrect browser type")
                && r.getBrowser() != null
                && browsers.contains(r.getBrowser());
    }

    private Predicate<ScrapeRequests> checkScraperTypes() {
        return r -> setMetaInfo("Incorrect scraper types")
                && r.getScraperTypes() != null
                && r.getScraperTypes().size() > 0;
    }

    private Predicate<ScrapeRequests> checkScrapeRequests() {
        return r -> r.getRequests().stream()
                .allMatch(item -> checkScrapeRequest().test(item));
    }

    private Predicate<ScrapeRequest> checkScrapeRequest() {
        return checkIfValidRequestType()
                .and(checkCountryCode())
                .and(checkItinerary())
                .and(validateTravellers());
    }

    private Predicate<ScrapeRequest> checkIfValidRequestType() {
        return r -> setMetaInfo("Invalid scraping request type")
                && (r instanceof FlightScrapeRequest);
    }

    private Predicate<ScrapeRequest> checkCountryCode() {
        return r -> setMetaInfo("Invalid country code")
                && ((FlightScrapeRequest) r).getCountryCode() != null
                && ((FlightScrapeRequest) r).getCountryCode().length() == 2;
    }

    private Predicate<ScrapeRequest> checkItinerary() {
        return item -> ((FlightScrapeRequest) item).getItinerary().stream()
                .allMatch(validateOdPair());
    }

    private Predicate<ODpair> validateOdPair() {
        return item -> setMetaInfo("Invalid OD pair data")
                && Objects.nonNull(item)
                && Objects.nonNull(item.getDestination())
                && Objects.nonNull(item.getOrigin())
                && Objects.nonNull(item.getDeparture())
                && item.getDestination().getIata().length() == 3
                && item.getOrigin().getIata().length() == 3;
    }

    private Predicate<ScrapeRequest> validateTravellers() {
        return item -> setMetaInfo("Invalid traveller data")
                && Objects.nonNull(((FlightScrapeRequest) item).getTravellers())
                && ((FlightScrapeRequest) item).getTravellers().size() > 0;
    }

    private boolean setMetaInfo(String msg) {
        return setMetaInfo(msg, HttpStatus.BAD_REQUEST);
    }

    private boolean setMetaInfo(String msg, HttpStatus status) {
        this.failureMessage = msg;
        this.status = status;
        return true;
    }

    public boolean isValid() {
        return checkBrowser()
                .and(checkScraperTypes())
                .and(checkScrapeRequests())
                .test(requests);
    }

    public String getFailureMessage() {
        return failureMessage;
    }

    public HttpStatus getStatus() {
        return status;
    }
}
