package com.metasearch.CAT.scrape;

import com.catModel.FlightSearchResult;
import com.catModel.ScrapeResult;
import com.catModel.ScrapeResults;
import com.metasearch.CAT.general.DaoManager;
import com.metasearch.CAT.scrape.filters.FilterFactory;
import com.metasearch.CAT.scrape.filters.ResultsFilter;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ScrapingResultsHandler implements ScrapingResultsService {

    FilterFactory filterFactory = new FilterFactory();

    @Override
    public void saveResults(ScrapeResults results) {
        List<ResultsFilter> filters = filterFactory.createResultFilters();

        results.getScrapeResults().forEach(result -> filters.stream()
                .filter(ResultsFilter::isEnabled)
                .forEach(filter -> filter.filterBeforeStoring(result)));

        DaoManager.getInstance().getScrapeResultsDao().insert(results);
        DaoManager.getInstance().getScrapeRequestDao().setRequestCompleted(results.getId());
    }

    @Override
    public ScrapeResults getScrapeResults(int requestId) {
        List<ResultsFilter> resultFilters = filterFactory.createResultFilters();
        List<FlightSearchResult> flightResults = DaoManager.getInstance().getScrapeResultsDao().getFlightResults(requestId);
        List<ScrapeResult> results =  flightResults.stream().peek(result -> resultFilters.stream()
                .filter(ResultsFilter::isEnabled)
                .forEach(filter -> filter.filterBeforeRetrieving(result)))
            .map(result -> (ScrapeResult) result)
            .collect(Collectors.toList());

        ScrapeResults scrapeResults = new ScrapeResults();
        scrapeResults.setId(requestId);
        scrapeResults.setScrapeResults(results);

        return scrapeResults;
    }

}
