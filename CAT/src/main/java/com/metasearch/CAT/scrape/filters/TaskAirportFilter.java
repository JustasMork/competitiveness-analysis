package com.metasearch.CAT.scrape.filters;

import com.catModel.*;
import com.metasearch.CAT.general.DaoManager;

public class TaskAirportFilter implements TasksFilter {

    private String filterName;

    public TaskAirportFilter(String name) {
        this.filterName = name;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public void filter(ScrapeRequests requests) {
        for (ScrapeRequest request : requests.getRequests()) {
            if (request instanceof FlightScrapeRequest) {
                filterFlightScrapeRequest((FlightScrapeRequest) request);
            }
        }
    }

    private void filterFlightScrapeRequest(FlightScrapeRequest request) {
        for (ODpair oDpair : request.getItinerary()) {
            Airport origin = DaoManager.getInstance().getAirportDao().getAirport(oDpair.getOrigin().getIata());
            Airport destination = DaoManager.getInstance().getAirportDao().getAirport(oDpair.getDestination().getIata());
            oDpair.setOrigin(origin);
            oDpair.setDestination(destination);
        }
    }


}
