package com.metasearch.CAT;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class CatApplication  {

	public static void main(String[] args) {
		SpringApplication.run(CatApplication.class, args);
	}

}
