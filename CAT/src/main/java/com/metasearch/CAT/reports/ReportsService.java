package com.metasearch.CAT.reports;

import com.catModel.ScrapeResults;
import com.metasearch.CAT.model.api.Table;

import java.util.List;

public interface ReportsService {

    List<Table> getTransferAnalysis(ScrapeResults results);

    List<Table> getJourneyAnalysis(ScrapeResults results);

    List<Table> getPriceAnalysis(ScrapeResults results);

    List<Table> getAirlinesAnalysis(ScrapeResults results);

    List<Table> getResultLoadingSpeedAnalysis(ScrapeResults results);

    List<Table> getBookingOptionsAnalysis(ScrapeResults results);

}
