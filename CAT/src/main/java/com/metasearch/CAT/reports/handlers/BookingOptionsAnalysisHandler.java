package com.metasearch.CAT.reports.handlers;

import com.catModel.FlightSearchResult;
import com.catModel.PriceInfo;
import com.catModel.Trip;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class BookingOptionsAnalysisHandler extends AnalysisHandlerBase {


    List<Set<String>>  uniqueBookingOptions;

    public BookingOptionsAnalysisHandler(List<FlightSearchResult> results) {
        super(results);
    }

    @Override
    public void analyze() {
        analyzeUniqueBookingOptions();
    }

    public List<Set<String>> getUniqueBookingOptions() {
        return uniqueBookingOptions;
    }

    private void analyzeUniqueBookingOptions() {
        List<Set<String>> bookingOptions = new ArrayList<>();
        try {
            for (FlightSearchResult result : results) {
                bookingOptions.add(parseBookingOptions(result));
            }
        } catch (Exception e) {
            System.err.println("Failed to parse unique booking options.");
            return;
        }
        uniqueBookingOptions = bookingOptions;
    }

    private Set<String> parseBookingOptions(FlightSearchResult result) {
        Set<String> bookingOptions = new HashSet<>();

        for (Trip trip : result.getTrips()) {
            Set<String> tripBookingOptions = trip.getPriceInfos().stream()
                    .map(PriceInfo::getProvider)
                    .collect(Collectors.toSet());
            bookingOptions.addAll(tripBookingOptions);
        }

        return bookingOptions;
    }
}
