package com.metasearch.CAT.reports.handlers;

import com.catModel.FlightSearchResult;
import com.catModel.Leg;
import com.catModel.Trip;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DurationAnalysisHandler extends AnalysisHandlerBase {

    /**
     * Total journey duration per search result page
     */
    private List<Long> totalDuration;

    /**
     * Avg journey duration per trip
     */
    private List<Float> avgDuration;

    public DurationAnalysisHandler(List<FlightSearchResult> results) {
        super(results);
    }

    @Override
    public void analyze() {
        analyzeTotalDuration();
    }

    public List<Long> getTotalDuration() {
        return totalDuration;
    }

    public List<Float> getAvgDuration() {
        return avgDuration;
    }

    private void analyzeTotalDuration() {
        List<Long> total = new ArrayList<>();
        List<Float> avg = new ArrayList<>();
        try {
            for (FlightSearchResult result : results) {
                Long resultDuration = calculateJourneyDuration(result);
                total.add(resultDuration);
                avg.add(resultDuration / (float) result.getTrips().size());
            }
        } catch (Exception e) {
            System.err.println("Missing data to parse journey duration.");
            return;
        }
        this.totalDuration = total;
        this.avgDuration = avg;
    }

    private Long calculateJourneyDuration(FlightSearchResult result) {
        Long duration = 0L;
        for (Trip trip : result.getTrips()) {
            for (Leg leg : trip.getLegs()) {
                Date from = leg.getSegments().get(0).getDeparture();
                Date to = leg.getSegments().get(leg.getSegments().size() - 1).getArrival();
                duration += getHourDifference(from, to);
            }
        }
        return duration;
    }

    private Long getHourDifference(Date from, Date to) {
        return to.getTime() - from.getTime();
    }


}
