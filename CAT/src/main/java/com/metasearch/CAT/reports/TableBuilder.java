package com.metasearch.CAT.reports;

import com.catModel.FlightSearchResult;
import com.catModel.ScraperType;
import com.metasearch.CAT.model.Tuple;
import com.metasearch.CAT.model.api.*;
import com.metasearch.CAT.reports.handlers.*;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

public class TableBuilder {

    public BarGraph getAvgTransfersTable(List<ScraperType> scraperTypes, List<TransfersAnalysisHandler> handlers, List<Label> labels) {
        try {
            BarGraph table = new BarGraph();
            table.setTableName("Average transfers per one trip");
            List<List<StringFloatEntry>> entries = new ArrayList<>();
            for (int entryColumn = 0; entryColumn < handlers.get(0).getAvgTransfers().size(); entryColumn++) {
                List<StringFloatEntry> entryList = new ArrayList<>();
                for (int scraperId = 0; scraperId < handlers.size(); scraperId++) {
                    StringFloatEntry entry = new StringFloatEntry();
                    entry.setX(scraperTypes.get(scraperId).name());
                    entry.setY(handlers.get(scraperId).getAvgTransfers().get(entryColumn));
                    entryList.add(entry);
                }
                entries.add(entryList);
            }

            table.setEntries(entries);
            table.setLabels(labels);
            return table;
        } catch (Exception e) {
            handleUnexpected(e, "AvgTransfers");
        }
        return null;
    }

    public BarGraph getAvgWaitTimeTable(List<ScraperType> scraperTypes, List<TransfersAnalysisHandler> handlers, List<Label> labels) {
        try {
            BarGraph table = new BarGraph();
            table.setTableName("Average transfer wait time per one trip (h)");
            List<List<StringFloatEntry>> entries = new ArrayList<>();
            for (int entryColumn = 0; entryColumn < handlers.get(0).getAvgTransfers().size(); entryColumn++) {
                List<StringFloatEntry> entryList = new ArrayList<>();
                for (int scraperId = 0; scraperId < handlers.size(); scraperId++) {
                    StringFloatEntry entry = new StringFloatEntry();
                    entry.setX(scraperTypes.get(scraperId).name());
                    entry.setY(handlers.get(scraperId).getAvgWaitTime().get(entryColumn) / 3600000);
                    entryList.add(entry);
                }
                entries.add(entryList);
            }

            table.setEntries(entries);
            table.setLabels(labels);
            return table;
        } catch (Exception e) {
            handleUnexpected(e, "AvgWaitTime");
        }
        return null;
    }

    public BarGraph getAvgDurationTime(List<ScraperType> scraperTypes, List<DurationAnalysisHandler> handlers, List<Label> labels) {
        try {
            BarGraph table = new BarGraph();
            table.setTableName("Average journey duration time per one trip (h)");
            List<List<StringFloatEntry>> entries = new ArrayList<>();
            for (int entryColumn = 0; entryColumn < handlers.get(0).getAvgDuration().size(); entryColumn++) {
                List<StringFloatEntry> entryList = new ArrayList<>();
                for (int scraperId = 0; scraperId < handlers.size(); scraperId++) {
                    StringFloatEntry entry = new StringFloatEntry();
                    entry.setX(scraperTypes.get(scraperId).name());
                    entry.setY(handlers.get(scraperId).getAvgDuration().get(entryColumn) / 3600000);
                    entryList.add(entry);
                }
                entries.add(entryList);
            }

            table.setEntries(entries);
            table.setLabels(labels);
            return table;
        } catch (Exception e) {
            handleUnexpected(e, "AvgDuration");
        }
        return null;
    }

    public BarGraph getMinPriceGraph(List<ScraperType> scraperTypes, List<OfferAnalysisHandler> handlers, List<Label> labels) {
        try {
            BarGraph table = new BarGraph();
            table.setTableName("Minimal price per search (EUR)");
            List<List<StringFloatEntry>> entries = new ArrayList<>();
            for (int entryColumn = 0; entryColumn < handlers.get(0).getMinPrices().size(); entryColumn++) {
                List<StringFloatEntry> entryList = new ArrayList<>();
                for (int scraperId = 0; scraperId < handlers.size(); scraperId++) {
                    StringFloatEntry entry = new StringFloatEntry();
                    entry.setX(scraperTypes.get(scraperId).name());
                    entry.setY(handlers.get(scraperId).getMinPrices().get(entryColumn));
                    entryList.add(entry);
                }
                entries.add(entryList);
            }

            table.setEntries(entries);
            table.setLabels(labels);
            return table;
        } catch (Exception e) {
            handleUnexpected(e, "MinPrice");
        }
        return null;
    }

    public BarGraph getUniqueAirlinesGraph(List<ScraperType> scraperTypes, List<AirlinesAnalysisHandler> handlers, List<Label> labels) {
        try {
            BarGraph table = new BarGraph();
            table.setTableName("Total unique airlines per search");
            List<List<StringFloatEntry>> entries = new ArrayList<>();
            for (int entryColumn = 0; entryColumn < handlers.get(0).getUniqueAirlines().size(); entryColumn++) {
                List<StringFloatEntry> entryList = new ArrayList<>();
                for (int scraperId = 0; scraperId < handlers.size(); scraperId++) {
                    StringFloatEntry entry = new StringFloatEntry();
                    entry.setX(scraperTypes.get(scraperId).name());
                    entry.setY((float) handlers.get(scraperId).getUniqueAirlines().get(entryColumn).size());
                    entryList.add(entry);
                }
                entries.add(entryList);
            }

            table.setEntries(entries);
            table.setLabels(labels);
            return table;
        } catch (Exception e) {
            handleUnexpected(e, "UniqueAirlines");
        }
        return null;
    }

    public List<Table> getMinOfferPricesGraphs(List<ScraperType> scraperTypes, List<OfferAnalysisHandler> handlers, List<Label> labels) {
       List<Table> graphs = new ArrayList<>();
       for (int i = 0; i < labels.size(); i++) {
           graphs.add(getMinOfferPriceGraph(scraperTypes, handlers, labels.get(i), i));
       }

       return graphs;
    }

    private LineGraph getMinOfferPriceGraph(List<ScraperType> scraperTypes, List<OfferAnalysisHandler> handlers, Label label, int index) {
        try {
            LineGraph lineGraph = new LineGraph();
            lineGraph.setTableName("Same offer price comparison " + label.getTitle());
            lineGraph.setLabels(scraperTypes.stream().map(item -> new Label(item.name())).collect(Collectors.toList()));
            List<String> offersIntersection = getOffersIntersection(handlers, index);

            List<List<StringFloatEntry>> entries = new ArrayList<>();

            for (int i = 0; i < scraperTypes.size(); i++) {
                List<StringFloatEntry> entry = new ArrayList<>();
                for (String offer : offersIntersection) {
                    StringFloatEntry entryItem = new StringFloatEntry();
                    entryItem.setX(offer);
                    entryItem.setY(getPriceByKeyFromOffer(offer, handlers.get(i).getMinOfferPrice().get(index)));
                    entryItem.setUnit("EUR");
                    entry.add(entryItem);
                }
                entries.add(entry);
            }
            lineGraph.setEntries(entries);

            return lineGraph;
        } catch (Exception e) {
            handleUnexpected(e, "minOfferPrice");
        }
        return null;
    }

    public List<Table> getBookingOptionsGraphs(List<ScraperType> scraperTypes, List<OfferAnalysisHandler> handlers, List<Label> labels) {
        List<Table> graphs = new ArrayList<>();
        for (int i = 0; i < labels.size(); i++) {
            graphs.add(getBookingOptionsGraph(scraperTypes, handlers, labels.get(i), i));
        }

        return graphs;
    }

    private LineGraph getBookingOptionsGraph(List<ScraperType> scraperTypes, List<OfferAnalysisHandler> handlers, Label label, int index) {
        try {
            LineGraph lineGraph = new LineGraph();
            lineGraph.setTableName("Same offer booking options " + label.getTitle());
            lineGraph.setLabels(scraperTypes.stream().map(item -> new Label(item.name())).collect(Collectors.toList()));
            List<String> offersIntersection = getOffersIntersection(handlers, index);

            List<List<StringFloatEntry>> entries = new ArrayList<>();

            for (int i = 0; i < scraperTypes.size(); i++) {
                List<StringFloatEntry> entry = new ArrayList<>();
                for (String offer : offersIntersection) {
                    StringFloatEntry entryItem = new StringFloatEntry();
                    entryItem.setX(offer);
                    entryItem.setY((float) getBookingOptionsByKeyFromOffer(offer, handlers.get(i).getOfferBookingOptions().get(index)));
                    entry.add(entryItem);
                }
                entries.add(entry);
            }
            lineGraph.setEntries(entries);

            return lineGraph;
        } catch (Exception e) {
            handleUnexpected(e, "OfferBookingOptions");
        }
        return null;
    }

    private List<String> getOffersIntersection(List<OfferAnalysisHandler> handlers, int index) {
        Set<String> intersection = null;
        for (OfferAnalysisHandler handler : handlers) {
            Set<Tuple<String, BigDecimal>> offers = handler.getMinOfferPrice().get(index);
            if (intersection == null) {
                intersection = new HashSet<>(getSetOfOfferKeys(offers));
            } else {
                Set<String> offerKeys = getSetOfOfferKeys(offers);
                intersection.retainAll(offerKeys);
            }
        }
        return new ArrayList<>(intersection);
    }

    private Set<String> getSetOfOfferKeys(Set<Tuple<String, BigDecimal>> offers) {
        Set<String> offerKeys = new HashSet<>();
        ArrayList<Tuple<String, BigDecimal>> offersList = new ArrayList<>(offers);
        for (Tuple<String, BigDecimal> offer : offersList) {
            offerKeys.add(offer.getFirst());
        }

        return offerKeys;
    }

    private BigDecimal getPriceByKeyFromOffer(String key, Set<Tuple<String, BigDecimal>> offers) {
        List<Tuple<String, BigDecimal>> offersList = new ArrayList<>(offers);
        for (Tuple<String, BigDecimal> offer : offersList) {
            if (offer.getFirst().equals(key)) {
                return offer.getSecond();
            }
        }
        return null;
    }

    private Integer getBookingOptionsByKeyFromOffer(String key, Set<Tuple<String, Integer>> bookingOptions) {
        List<Tuple<String, Integer>> offersList = new ArrayList<>(bookingOptions);
        for (Tuple<String, Integer> option : bookingOptions) {
            if (option.getFirst().equals(key)) {
                return option.getSecond();
            }
        }
        return null;
    }

    public BarGraph getResultLoadingTimeGraph(List<ScraperType> scraperTypes, Map<ScraperType, List<FlightSearchResult>> results, List<Label> labels) {
        try {
            BarGraph barGraph = new BarGraph();
            barGraph.setTableName("Search result loading time (s)");
            barGraph.setLabels(labels);
            List<List<StringFloatEntry>> entries = new ArrayList<>();
            for (int i = 0; i < results.get(scraperTypes.get(0)).size(); i++) {
                List<StringFloatEntry> entryList = new ArrayList<>();
                for (ScraperType scraperType : scraperTypes) {
                    StringFloatEntry entry = new StringFloatEntry();
                    entry.setX(scraperType.name());
                    entry.setY((results.get(scraperType).get(i).getResultLoadingTime() / (float) 1000));
                    entryList.add(entry);
                }
                entries.add(entryList);
            }
            barGraph.setEntries(entries);
            return barGraph;
        } catch (Exception e) {
            handleUnexpected(e, "ResultLoadingTime");
        }
        return null;
    }

    public BarGraph getUniqueBookingOptionsGraph(List<ScraperType> scraperTypes, List<BookingOptionsAnalysisHandler> handlers, List<Label> labels) {
        try {
            BarGraph table = new BarGraph();
            table.setTableName("Total unique booking options per search");
            List<List<StringFloatEntry>> entries = new ArrayList<>();
            for (int entryColumn = 0; entryColumn < handlers.get(0).getUniqueBookingOptions().size(); entryColumn++) {
                List<StringFloatEntry> entryList = new ArrayList<>();
                for (int scraperId = 0; scraperId < handlers.size(); scraperId++) {
                    StringFloatEntry entry = new StringFloatEntry();
                    entry.setX(scraperTypes.get(scraperId).name());
                    entry.setY((float) handlers.get(scraperId).getUniqueBookingOptions().get(entryColumn).size());
                    entryList.add(entry);
                }
                entries.add(entryList);
            }

            table.setEntries(entries);
            table.setLabels(labels);
            return table;
        } catch (Exception e) {
            handleUnexpected(e, "UniqueAirlines");
        }
        return null;
    }


    private void handleUnexpected(Exception e, String table) {
        System.err.println("Failed to create report table "+ table +": " + e.getMessage());
    }
}
