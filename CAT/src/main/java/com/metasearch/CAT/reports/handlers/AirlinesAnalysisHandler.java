package com.metasearch.CAT.reports.handlers;

import com.catModel.FlightSearchResult;
import com.catModel.Leg;
import com.catModel.Segment;
import com.catModel.Trip;

import java.util.*;

public class AirlinesAnalysisHandler extends AnalysisHandlerBase{

    private List<Set<String>> uniqueAirlines;

    public AirlinesAnalysisHandler(List<FlightSearchResult> results) {
        super(results);
    }

    @Override
    public void analyze() {
        parseUniqueAirlines();
    }

    public List<Set<String>> getUniqueAirlines() {
        return uniqueAirlines;
    }

    private void parseUniqueAirlines() {
        List<Set<String>> airlines = new ArrayList<>();
        try {
            for (FlightSearchResult result : results) {
                airlines.add(parseAirlines(result));
            }
        } catch (Exception e) {
            System.err.println("Failed to unique airlines.");
            return;
        }
        uniqueAirlines = airlines;
    }

    private Set<String> parseAirlines(FlightSearchResult result) {
        Set<String> airlinesSet = new HashSet<>();

        for (Trip trip : result.getTrips()) {
            for (Leg leg : trip.getLegs()) {
                for (Segment segment : leg.getSegments()) {
                    airlinesSet.add(segment.getAirline().getCode());
                }
            }
        }

        return airlinesSet;
    }

}
