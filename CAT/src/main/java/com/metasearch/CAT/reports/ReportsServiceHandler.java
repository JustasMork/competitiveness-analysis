package com.metasearch.CAT.reports;

import com.catModel.*;
import com.metasearch.CAT.model.api.Label;
import com.metasearch.CAT.model.api.Table;
import com.metasearch.CAT.reports.handlers.*;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ReportsServiceHandler implements ReportsService {

    private TableBuilder tableBuilder = new TableBuilder();

    @Override
    public List<Table> getTransferAnalysis(ScrapeResults results) {
        Map<ScraperType, List<FlightSearchResult>> sortedResults = getSortedResults(results);
        List<Table> tables = new ArrayList<>();

        List<TransfersAnalysisHandler> handlers = new ArrayList<>();
        List<ScraperType> scraperTypes = new ArrayList<>(sortedResults.keySet());

        for (ScraperType scraperType : scraperTypes) {
            TransfersAnalysisHandler handler = new TransfersAnalysisHandler(sortedResults.get(scraperType));
            handler.analyze();
            handlers.add(handler);
        }

        List<Label> labels = getTableLabels(sortedResults.get(scraperTypes.get(0)));

        tables.add(tableBuilder.getAvgTransfersTable(scraperTypes, handlers, labels));
        tables.add(tableBuilder.getAvgWaitTimeTable(scraperTypes, handlers, labels));
        return tables;
    }

    @Override
    public List<Table> getJourneyAnalysis(ScrapeResults results) {
        Map<ScraperType, List<FlightSearchResult>> sortedResults = getSortedResults(results);
        List<Table> tables = new ArrayList<>();

        List<DurationAnalysisHandler> handlers = new ArrayList<>();
        List<ScraperType> scraperTypes = new ArrayList<>(sortedResults.keySet());

        for (ScraperType scraperType : scraperTypes) {
            DurationAnalysisHandler handler = new DurationAnalysisHandler(sortedResults.get(scraperType));
            handler.analyze();
            handlers.add(handler);
        }
        List<Label> labels = getTableLabels(sortedResults.get(scraperTypes.get(0)));
        tables.add(tableBuilder.getAvgDurationTime(scraperTypes, handlers, labels));
        return tables;
    }

    @Override
    public List<Table> getPriceAnalysis(ScrapeResults results) {
        Map<ScraperType, List<FlightSearchResult>> sortedResults = getSortedResults(results);
        List<Table> tables = new ArrayList<>();

        List<OfferAnalysisHandler> handlers = new ArrayList<>();
        List<ScraperType> scraperTypes = new ArrayList<>(sortedResults.keySet());

        for (ScraperType scraperType : scraperTypes) {
            OfferAnalysisHandler handler = new OfferAnalysisHandler(sortedResults.get(scraperType));
            handler.analyze();
            handlers.add(handler);
        }
        List<Label> labels = getTableLabels(sortedResults.get(scraperTypes.get(0)));
        tables.addAll(tableBuilder.getMinOfferPricesGraphs(scraperTypes, handlers, labels));
        tables.add(tableBuilder.getMinPriceGraph(scraperTypes, handlers, labels));
        tables.addAll(tableBuilder.getBookingOptionsGraphs(scraperTypes, handlers, labels));
        return tables;
    }

    @Override
    public List<Table> getAirlinesAnalysis(ScrapeResults results) {
        Map<ScraperType, List<FlightSearchResult>> sortedResults = getSortedResults(results);
        List<Table> tables = new ArrayList<>();

        List<AirlinesAnalysisHandler> handlers = new ArrayList<>();
        List<ScraperType> scraperTypes = new ArrayList<>(sortedResults.keySet());

        for (ScraperType scraperType : scraperTypes) {
            AirlinesAnalysisHandler handler = new AirlinesAnalysisHandler(sortedResults.get(scraperType));
            handler.analyze();
            handlers.add(handler);
        }
        List<Label> labels = getTableLabels(sortedResults.get(scraperTypes.get(0)));
        tables.add(tableBuilder.getUniqueAirlinesGraph(scraperTypes, handlers, labels));
        return tables;
    }

    @Override
    public List<Table> getResultLoadingSpeedAnalysis(ScrapeResults results) {
        Map<ScraperType, List<FlightSearchResult>> sortedResults = getSortedResults(results);
        List<Table> tables = new ArrayList<>();
        List<ScraperType> scraperTypes = new ArrayList<>(sortedResults.keySet());
        List<Label> labels = getTableLabels(sortedResults.get(scraperTypes.get(0)));

        tables.add(tableBuilder.getResultLoadingTimeGraph(scraperTypes, sortedResults, labels));

        return tables;
    }

    @Override
    public List<Table> getBookingOptionsAnalysis(ScrapeResults results) {
        Map<ScraperType, List<FlightSearchResult>> sortedResults = getSortedResults(results);
        List<Table> tables = new ArrayList<>();

        List<BookingOptionsAnalysisHandler> handlers = new ArrayList<>();
        List<ScraperType> scraperTypes = new ArrayList<>(sortedResults.keySet());

        for (ScraperType scraperType : scraperTypes) {
            BookingOptionsAnalysisHandler handler = new BookingOptionsAnalysisHandler(sortedResults.get(scraperType));
            handler.analyze();
            handlers.add(handler);
        }
        List<Label> labels = getTableLabels(sortedResults.get(scraperTypes.get(0)));
        tables.add(tableBuilder.getUniqueBookingOptionsGraph(scraperTypes, handlers, labels));
        return tables;
    }

    private List<Label> getTableLabels(List<FlightSearchResult> results) {

        List<Label> labels = new ArrayList<>();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        for (FlightSearchResult result : results) {
            StringBuilder builder = new StringBuilder();
            Trip trip = result.getTrips().get(0);
            for (Leg leg : trip.getLegs()) {
                int segmentsCount = leg.getSegments().size();
                builder.append("[").append(leg.getSegments().get(0).getOrigin().getIata()).append("-");
                builder.append(leg.getSegments().get(segmentsCount-1).getDestination().getIata()).append(" ");
                builder.append(format.format(leg.getSegments().get(0).getDeparture())).append("]");
                builder.append(", ");
            }
            String labelName = builder.toString();

            labels.add(new Label(labelName.substring(0, labelName.length() - 2)));
        }
        return labels;
    }

    private Map<ScraperType, List<FlightSearchResult>> getSortedResults(ScrapeResults results) {
        Map<ScraperType, List<FlightSearchResult>> sortedResults = new HashMap<>();

        for (ScrapeResult scrapeResult : results.getScrapeResults()) {
            FlightSearchResult result = (FlightSearchResult) scrapeResult;

            List<FlightSearchResult> resultsList = sortedResults.computeIfAbsent(result.getScraperType(),
                    key -> new ArrayList<>());
            resultsList.add(result);
        }
        return sortedResults;
    }
}
