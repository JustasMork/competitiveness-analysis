package com.metasearch.CAT.reports.handlers;

import com.catModel.FlightSearchResult;

import java.util.List;

public abstract class AnalysisHandlerBase {

    protected List<FlightSearchResult> results;

    public AnalysisHandlerBase(List<FlightSearchResult> results) {
        this.results = results;
    }

    public abstract void analyze();
}
