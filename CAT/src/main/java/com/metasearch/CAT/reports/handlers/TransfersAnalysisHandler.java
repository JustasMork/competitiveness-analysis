package com.metasearch.CAT.reports.handlers;

import com.catModel.FlightSearchResult;
import com.catModel.Leg;
import com.catModel.Trip;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TransfersAnalysisHandler extends AnalysisHandlerBase {


    /**
     * Total transfers per search page
     */
    private List<Integer> totalTransfers;

    /**
     * Avg transfers per trip
     */
    private List<Float> avgTransfers;

    /**
     * Total transfer wait time per search page
     */
    private List<Long> totalWaitTime;

    /**
     * Avg transfer wait time per trip
     */
    private List<Float> avgWaitTime;

    public TransfersAnalysisHandler(List<FlightSearchResult> results) {
        super(results);
    }

    @Override
    public void analyze() {
        analyzeTransferAmount();
        analyseWaitTime();
    }

    public List<Integer> getTotalTransfers() {
        return totalTransfers;
    }

    public List<Float> getAvgTransfers() {
        return avgTransfers;
    }

    public List<Long> getTotalWaitTime() {
        return totalWaitTime;
    }

    public List<Float> getAvgWaitTime() {
        return avgWaitTime;
    }

    private void analyzeTransferAmount() {
        this.totalTransfers = new ArrayList<>();
        this.avgTransfers = new ArrayList<>();
        for (FlightSearchResult result : results) {
            Integer totalTransfers = calculateTotalTransfers(result);
            this.totalTransfers.add(totalTransfers);
            this.avgTransfers.add((totalTransfers/(float)result.getTrips().size()));
        }
    }

    private Integer calculateTotalTransfers(FlightSearchResult result) {
        Integer totalTransfers = 0;
        for (Trip trip : result.getTrips()) {
            for (Leg leg : trip.getLegs()) {
                totalTransfers += leg.getSegments().size() - 1;
            }
        }
        return totalTransfers;
    }

    private void analyseWaitTime() {
        List<Long> total = new ArrayList<>();
        List<Float> avg = new ArrayList<>();
        try {
            for (FlightSearchResult result : results) {
                Long totalWait = calculateTotalWaitDuration(result);
                total.add(totalWait);
                avg.add(totalWait/(float) result.getTrips().size());
            }
        } catch (Exception e) {
            System.err.println("Missing data to parse transfer wait time.");
            return;
        }
        this.totalWaitTime = total;
        this.avgWaitTime = avg;
    }

    private Long calculateTotalWaitDuration(FlightSearchResult result) {
        Long waitTime = 0L;
        for (Trip trip : result.getTrips()) {
            for (Leg leg : trip.getLegs()) {
                if (leg.getSegments().size() > 1) {
                    waitTime += getTransferWaitTime(leg);
                }
            }
        }
        return waitTime;
    }

    private Long getTransferWaitTime(Leg leg) {
        Long waitTimeInMins = 0L;

        for (int i = 0; i < leg.getSegments().size() - 1; i++) {
            Date from = leg.getSegments().get(i).getArrival();
            Date to = leg.getSegments().get(i + 1).getDeparture();
            Long diff = to.getTime() - from.getTime();
            waitTimeInMins += diff;
        }

        return waitTimeInMins;
    }

}
