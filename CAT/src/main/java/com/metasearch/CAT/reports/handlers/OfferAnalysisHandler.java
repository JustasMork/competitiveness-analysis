package com.metasearch.CAT.reports.handlers;

import com.catModel.*;
import com.metasearch.CAT.general.Cache;
import com.metasearch.CAT.general.Config;
import com.metasearch.CAT.model.Tuple;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

public class OfferAnalysisHandler extends AnalysisHandlerBase {


    private List<BigDecimal> minPrices;

    private List<Set<Tuple<String, BigDecimal>>> minOfferPrice;

    private List<Set<Tuple<String, Integer>>> offerBookingOptions;

    public OfferAnalysisHandler(List<FlightSearchResult> results) {
        super(results);
    }

    @Override
    public void analyze() {
        parseMinPrices();
        parseMinPricesPerOffer();
        parseOfferBookingOptions();
    }

    public List<BigDecimal> getMinPrices() {
        return minPrices;
    }

    public List<Set<Tuple<String, BigDecimal>>> getMinOfferPrice() {
        return minOfferPrice;
    }

    public List<Set<Tuple<String, Integer>>> getOfferBookingOptions() {
        return offerBookingOptions;
    }

    private void parseMinPrices() {
        List<BigDecimal> prices = new ArrayList<>();

        try {
            for (FlightSearchResult result : results) {
                prices.add(findMinPrice(result));
            }
        } catch (Exception e) {
            System.err.println("Price analysis failed.");
            e.printStackTrace();
            return;
        }

        minPrices = prices;
    }

    private BigDecimal findMinPrice(FlightSearchResult result) throws JSONException {
        BigDecimal minPrice = null;
        String currCode = null;
        for (Trip trip : result.getTrips()) {
            for (PriceInfo priceInfo : trip.getPriceInfos()) {
                Price price = priceInfo.getPrice();
                if (minPrice == null) {
                    minPrice = price.getAmount();
                    currCode = price.getCurrency();
                } else if (minPrice.compareTo(price.getAmount()) > 0) {
                    minPrice = price.getAmount();
                    currCode = price.getCurrency();
                }
            }
        }

        if (!currCode.equals("EUR")) {
            minPrice = convertPriceToEur(minPrice, currCode);
        }

        return minPrice;
    }

    private BigDecimal convertPriceToEur(BigDecimal amount, String currCode) throws JSONException {
        BigDecimal toEurRate = Cache.getInstance().getDecimal(currCode + "_EUR");
        if (toEurRate == null) {
            toEurRate = getCurrRate(currCode);
            Cache.getInstance().add(currCode+"_EUR", toEurRate);
        }
        return amount.multiply(toEurRate);
    }

    private BigDecimal getCurrRate(String currCode) throws JSONException {
        RestTemplate restTemplate = new RestTemplate();

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl("https://free.currconv.com/api/v7/convert")
                .queryParam("q", currCode+"_EUR")
                .queryParam("compact", "ultra")
                .queryParam("apiKey", Config.getInstance().getEnv().getProp("reports.currency.rates.apiKey"));

        ResponseEntity<String> responseEntity = restTemplate.getForEntity(builder.toUriString(), String.class);

        JSONObject jsonObject = new JSONObject(responseEntity.getBody());
        return new BigDecimal(jsonObject.getDouble(currCode+"_EUR"));
    }

    private void parseMinPricesPerOffer() {
        List<Set<Tuple<String, BigDecimal>>> offersPrices = new ArrayList<>();
        try {
            for (FlightSearchResult result : results) {
                offersPrices.add(parseSearchOfferPrices(result));
            }
        } catch (Exception e) {
            System.err.println("Price per offer analysis failed.");
            e.printStackTrace();
            return;
        }
        this.minOfferPrice = offersPrices;
    }

    private Set<Tuple<String, BigDecimal>> parseSearchOfferPrices(FlightSearchResult result) throws JSONException {
        Set<Tuple<String, BigDecimal>> offers = new HashSet<>();
        for (Trip trip : result.getTrips()) {
            String offerKey = getOfferKey(trip);
            BigDecimal offerMinPrice = getMinOfferPrice(trip.getPriceInfos());
            offers.add(new Tuple<>(offerKey, offerMinPrice));
        }
        return offers;
    }

    private String getOfferKey(Trip trip) {
        StringBuilder builder = new StringBuilder();
        for (Leg leg : trip.getLegs()) {
            for (Segment segment : leg.getSegments()) {
                builder.append(segment.getFlightNr()).append("-");
            }
        }

        String key = builder.toString();
        return key.substring(0, key.length() - 1);
    }

    private BigDecimal getMinOfferPrice(List<PriceInfo> priceInfos) throws JSONException {
        BigDecimal minPrice = null;
        String currCode = null;
        for (PriceInfo priceInfo : priceInfos) {
            if (minPrice == null || minPrice.compareTo(priceInfo.getPrice().getAmount()) > 0) {
                minPrice = priceInfo.getPrice().getAmount();
                currCode = priceInfo.getPrice().getCurrency();
            }
        }

        return convertPriceToEur(minPrice, currCode);
    }

    private void parseOfferBookingOptions() {
        List<Set<Tuple<String, Integer>>> offerBookingOptions = new ArrayList<>();
        try {
            for (FlightSearchResult result : results) {
               offerBookingOptions.add(parseSearchOffersBookingOptions(result));
            }
        } catch (Exception e) {
            System.err.println("Booking options per offer parsing failed.");
            e.printStackTrace();
            return;
        }
        this.offerBookingOptions = offerBookingOptions;
    }

    private Set<Tuple<String, Integer>> parseSearchOffersBookingOptions(FlightSearchResult result) throws JSONException {
        Set<Tuple<String, Integer>> bookingOptions = new HashSet<>();
        for (Trip trip : result.getTrips()) {
            String offerKey = getOfferKey(trip);
            Set<String> uniqueBookingOptions = getUniqueBookingOptions(trip);
            bookingOptions.add(new Tuple<>(offerKey, uniqueBookingOptions.size()));
        }
        return bookingOptions;
    }

    private Set<String> getUniqueBookingOptions(Trip trip) {
        return trip.getPriceInfos().stream().map(PriceInfo::getProvider).collect(Collectors.toSet());
    }

}
