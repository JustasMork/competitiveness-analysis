package com.metasearch.CAT.controller;

import com.catModel.ScrapeResults;
import com.metasearch.CAT.general.DaoManager;
import com.metasearch.CAT.model.api.Table;
import com.metasearch.CAT.reports.ReportsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/reports")
public class ReportsController {

    @Autowired
    ReportsService reportsService;

    @RequestMapping(path = "/{id}",  method = RequestMethod.GET, produces = "application/json")
    public List<Table> getReport(@PathVariable("id") int id) {
        ScrapeResults scrapeResults = DaoManager.getInstance().getScrapeResultsDao().getScrapeResults(id);

        List<Table> tables = reportsService.getPriceAnalysis(scrapeResults);
        tables.addAll(reportsService.getBookingOptionsAnalysis(scrapeResults));
        tables.addAll(reportsService.getJourneyAnalysis(scrapeResults));
        tables.addAll(reportsService.getTransferAnalysis(scrapeResults));
        tables.addAll(reportsService.getAirlinesAnalysis(scrapeResults));
        tables.addAll(reportsService.getResultLoadingSpeedAnalysis(scrapeResults));
        return tables;

    }

}
