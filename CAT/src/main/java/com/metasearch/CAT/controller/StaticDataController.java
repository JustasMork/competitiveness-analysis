package com.metasearch.CAT.controller;

import com.catModel.Airport;
import com.catModel.CabinClass;
import com.catModel.ScraperType;
import com.metasearch.CAT.general.DaoManager;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/static")
public class StaticDataController {

    @RequestMapping(value = "scraperTypes", method = RequestMethod.GET, produces = "application/json")
    public ScraperType[] getScraperTypes() {
        return ScraperType.values();
    }

    @RequestMapping(value = "cabinClass", method = RequestMethod.GET, produces = "application/json")
    public CabinClass[] getCabinClasses() {
        return CabinClass.values();
    }

    @RequestMapping(value = "airports", method = RequestMethod.GET, produces = "application/json")
    public List<Airport> getAirports(@RequestParam("query") String query) {
        return DaoManager.getInstance().getAirportDao().findAirports("%"+query+"%");
    }
}
