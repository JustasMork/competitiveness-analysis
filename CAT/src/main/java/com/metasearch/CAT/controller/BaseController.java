package com.metasearch.CAT.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BaseController {

    @RequestMapping("/")
    public String handleBase(@RequestParam(name = "name", defaultValue = "Justas") String name) {
        return "Hello " + name +"!";
    }
}
