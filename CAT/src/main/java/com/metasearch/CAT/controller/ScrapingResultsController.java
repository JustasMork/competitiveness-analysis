package com.metasearch.CAT.controller;

import com.catModel.ScrapeResults;
import com.metasearch.CAT.scrape.ScrapingResultsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/scrape/results")
public class ScrapingResultsController {

    @Autowired
    ScrapingResultsService service;

    @RequestMapping(method = RequestMethod.POST, consumes = "application/json")
    public void saveResults(@RequestBody ScrapeResults results) {
        service.saveResults(results);
    }

}
