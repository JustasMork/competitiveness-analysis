package com.metasearch.CAT.controller;

import com.catModel.ScrapeRequests;
import com.catModel.ScraperType;
import com.metasearch.CAT.general.DaoManager;
import com.metasearch.CAT.scrape.ScrapingTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/scrape/tasks")
public class ScrapingTasksController {


    @Autowired
    ScrapingTaskService taskService;

    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public ScrapeRequests getScrapingRequests() {
        return taskService.getScrapingRequests();
    }

    @RequestMapping(method = RequestMethod.POST, consumes = "application/json")
    public void publishScrapingRequest(@RequestBody ScrapeRequests requests) {
        taskService.addScrapingRequests(requests);
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET, produces = "application/json")
    public List<ScrapeRequests> getAllScrapingRequests() {
        return taskService.getAllScrapingRequests();
    }

}
