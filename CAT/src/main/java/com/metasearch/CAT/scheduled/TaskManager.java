package com.metasearch.CAT.scheduled;

import com.metasearch.CAT.scheduled.tasks.Task;
import com.metasearch.CAT.scheduled.tasks.TaskConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@Component
public class TaskManager {

    Logger log = LoggerFactory.getLogger(TaskManager.class);

    @Value("${scheduled.manager.tasksExecution.timeout}")
    private String taskExecutionTimeout;

    private final TaskFactory taskFactory;

    private final TaskInitializationConfig factoryConfiguration;

    public TaskManager(TaskFactory taskFactory, TaskInitializationConfig factoryConfiguration) {
        this.taskFactory = taskFactory;
        this.factoryConfiguration = factoryConfiguration;
    }

    /**
     * Execute this method every hour 01:01
     */
    @Scheduled(cron = "1 1 * * * *")
    public void executeHourly() throws InterruptedException {
        log.info("Executing hourly cron job");

        TaskContext context = new TaskContext();
        List<Task<TaskConfig>> tasks = createTasks(ExecutionInterval.HOURLY);
        executeTasks(tasks, context);

        log.info("Hourly cron job finished execution");
    }

    /**
     * Execute this method every day at 03:01:01
     */
    @Scheduled(cron = "1 1 3 * * *")
    public void executeDaily() throws InterruptedException {
        log.info("Executing daily cron job");

        TaskContext context = new TaskContext();
        List<Task<TaskConfig>> tasks = createTasks(ExecutionInterval.DAILY);
        executeTasks(tasks, context);

        log.info("Daily cron job finished execution");
    }

    /**
     * Execute this method every week on Monday 02:01:01
     */
    @Scheduled(cron = "1 1 2 ? * MON")
    public void executeWeekly() throws InterruptedException {
        log.info("Executing weekly cron job");

        TaskContext context = new TaskContext();
        List<Task<TaskConfig>> tasks = createTasks(ExecutionInterval.WEEKLY);
        executeTasks(tasks, context);

        log.info("Weekly cron job finished execution");
    }

    /**
     * Execute this method every month on 1st at 01:01:01
     */
    @Scheduled(cron = "1 1 1 1 * ?")
    public void executeMonthly() throws InterruptedException {
        log.info("Executing monthly cron job");

        TaskContext context = new TaskContext();
        List<Task<TaskConfig>> tasks = createTasks(ExecutionInterval.MONTHLY);
        executeTasks(tasks, context);

        log.info("Monthly cron job finished execution");
    }

    private List<Task<TaskConfig>> createTasks(ExecutionInterval executionInterval) {
        List<Task<TaskConfig>> tasks = new ArrayList<>();
        Map<String, String> enabledFactories = factoryConfiguration.getEnabledFactories(executionInterval);

        for (Map.Entry<String, String> entry : enabledFactories.entrySet()) {
            Task<TaskConfig> task = taskFactory.createTask(entry.getKey(), entry.getValue());

            if (task != null) {
                tasks.add(task);
            }
        }

        return tasks;
    }

    private void executeTasks(List<Task<TaskConfig>> tasks, TaskContext context) throws InterruptedException {

        if (tasks.size() < 1) {
            return;
        }

        executeBefore(tasks, context);

        ExecutorService executor = Executors.newFixedThreadPool(tasks.size());
        for (Task<TaskConfig> task : tasks) {
            log.info("Executing " + task.getTaskName());
            executor.execute(() -> task.runTask(context));
        }
        executor.shutdown();
        executor.awaitTermination(Long.valueOf(taskExecutionTimeout), TimeUnit.MINUTES);

        executeAfter(tasks, context);
    }

    private void executeBefore(List<Task<TaskConfig>> tasks, TaskContext context) {
        for (Task<TaskConfig> task : tasks) {
            task.beforeRun(context);
        }
    }

    private void executeAfter(List<Task<TaskConfig>> tasks, TaskContext context) {
        for (Task<TaskConfig> task : tasks) {
            task.afterRun(context);
        }
    }
}
