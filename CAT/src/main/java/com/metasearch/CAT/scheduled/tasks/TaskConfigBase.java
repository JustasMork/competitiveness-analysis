package com.metasearch.CAT.scheduled.tasks;


import com.metasearch.CAT.general.Config;
import com.metasearch.CAT.general.Env;

public abstract class TaskConfigBase implements TaskConfig {

    private static final String PROP_PREFIX = "scheduled.task.";

    private static final String PROP_IS_ENABLED = ".enabled";

    private Env env;

    private String taskName;

    public TaskConfigBase(String taskName) {
        this.taskName = taskName;
        env = Config.getInstance().getEnv();
    }

    @Override
    public boolean isEnabled() {
        return env.getBoolean(PROP_PREFIX + taskName + PROP_IS_ENABLED);
    }

    @Override
    public final String getTaskName() {
        return taskName;
    }

    @Override
    public final String getProp(String suffix) {
        return env.getProp(PROP_PREFIX + taskName + "." + suffix);
    }

}
