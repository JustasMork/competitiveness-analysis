package com.metasearch.CAT.scheduled.tasks;

import com.metasearch.CAT.general.Config;

public class TaskUtils {

    public static String cleanUpString(String string) {
        return string.replace("\"", "")
                .replace("'", "")
                .replace("\\N", "")
                .replace("\\n", "")
                .replace("\\R", "")
                .replace("\\r", "");
    }

    public static String getFilePath(String resource) {
        return Config.getInstance().getResourcesPath() + "/" + resource;
    }

}
