package com.metasearch.CAT.scheduled.tasks.airlineImport;


import com.catModel.Airline;
import com.metasearch.CAT.model.DAO.AirlineDao;
import com.metasearch.CAT.general.DaoManager;
import com.metasearch.CAT.scheduled.TaskContext;
import com.metasearch.CAT.scheduled.tasks.Task;
import com.metasearch.CAT.scheduled.tasks.TaskUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;

import static com.metasearch.CAT.scheduled.tasks.TaskUtils.cleanUpString;

public class AirlineImportTask extends Task<AirlineImportConfig> {

    Logger log = LoggerFactory.getLogger(AirlineImportTask.class);

    private static int INDEX_CODE = 3;

    private static int INDEX_IATA = 4;

    private static int INDEX_NAME = 1;

    private static int INDEX_COUNTRY = 6;

    AirlineDao airlineDao;

    public AirlineImportTask(AirlineImportConfig config) {
        super(config);
        airlineDao = DaoManager.getInstance().getAirlineDao();
    }

    @Override
    public void runTask(TaskContext context) {
        FileInputStream stream = getResourceStream();

        if (stream != null) {
            readLines(stream);
        } else {
            log.error("Airline import task failed. Unable to read resource stream");
        }
    }

    private void readLines(FileInputStream stream) {
        int notImportedCounter = 0;
        try (Scanner scanner = new Scanner(stream)) {
            while (scanner.hasNextLine()) {
                String[] lineSplit = scanner.nextLine().split(",");
                Airline airline = parseAirline(lineSplit);
                if (isAirlineValid(airline)) {
                    saveAirline(airline);
                } else {
                    log.info("Not valid: " + airline);
                    notImportedCounter++;
                }
            }
        }

        if (notImportedCounter != 0) {
            log.warn("Failed to import " + notImportedCounter + " airports");
        }
    }

    private Airline parseAirline(String[] lineSplit) {
        Airline airline = new Airline();
        airline.setCode(cleanUpString(lineSplit[INDEX_CODE]));
        airline.setIata(cleanUpString(lineSplit[INDEX_IATA]));
        airline.setName(cleanUpString(lineSplit[INDEX_NAME]));
        airline.setCountry(cleanUpString(lineSplit[INDEX_COUNTRY]));

        return airline;
    }

    private boolean isAirlineValid(Airline airline) {
        return (airline != null)
                && airline.getCode() != null
                && airline.getIata() != null
                && airline.getCode().length() == 2
                && airline.getIata().length() == 3
                && airline.getIata().matches("[A-Z0-9]+");
    }

    private void saveAirline(Airline airline) {
        Airline oldAirline = airlineDao.getAirline(airline.getCode());
        if (oldAirline == null) {
            airlineDao.insertAirline(airline);
        } else {
            if (!airline.equals(oldAirline)) {
                airlineDao.updateAirline(airline);
            }
        }
    }

    private FileInputStream getResourceStream() {
        try {
            return new FileInputStream(getResourceFile());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }

    private File getResourceFile() {
        return new File(TaskUtils.getFilePath(getConfig().getResource()));
    }
}
