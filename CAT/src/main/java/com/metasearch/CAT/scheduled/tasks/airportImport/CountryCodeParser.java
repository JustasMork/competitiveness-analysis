package com.metasearch.CAT.scheduled.tasks.airportImport;

import com.metasearch.CAT.scheduled.tasks.TaskUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class CountryCodeParser {

    private static int INDEX_NAME = 0;

    private static int INDEX_CODE = 1;

    private static String SEPARATOR = ",";

    private Logger log = LoggerFactory.getLogger(CountryCodeParser.class);

    private AirportImportConfig config;

    private Map<String, String> countryToCodeMap;

    public CountryCodeParser(AirportImportConfig config) {
        this.config = config;
    }

    public String getCode(String countryName) {
        if (countryName == null) {
            return null;
        }

        if (countryToCodeMap == null) {
            initializeMap();
        }

        String countryCode = countryToCodeMap.get(countryName.toLowerCase());

        if (countryCode == null) {
            log.info("Country code not found for: " + countryName.toLowerCase());
        }

        return countryCode;
    }

    private void initializeMap() {
        InputStream stream = getStream();
        if (stream != null) {
            countryToCodeMap = new HashMap<>();
            readCodes(stream);
        }
    }

    private void readCodes(InputStream stream) {
        try (Scanner scanner = new Scanner(stream)) {
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                String[] lineSplit = line.split(SEPARATOR);
                writeCodeToMap(lineSplit);
            }
        }
    }

    private void writeCodeToMap(String[] lineSplit) {
        String countryName = TaskUtils.cleanUpString(lineSplit[INDEX_NAME]);
        String countryCode = TaskUtils.cleanUpString(lineSplit[INDEX_CODE]);
        countryToCodeMap.put(countryName.toLowerCase(), countryCode.toLowerCase());
    }

    private InputStream getStream() {
        try {
            return new FileInputStream(getResourceFile());
        } catch (FileNotFoundException e) {
            log.error("Country code resource file not found: " + e.getMessage());
        }
        return null;
    }

    private File getResourceFile() {
        return new File(TaskUtils.getFilePath(config.getCountryCodesResource()));
    }

}
