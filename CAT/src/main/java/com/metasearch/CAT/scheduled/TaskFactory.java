package com.metasearch.CAT.scheduled;

import com.metasearch.CAT.scheduled.tasks.Task;
import com.metasearch.CAT.scheduled.tasks.TaskConfig;

public interface TaskFactory {

    Task<TaskConfig> createTask(String taskName, String classpath);

}
