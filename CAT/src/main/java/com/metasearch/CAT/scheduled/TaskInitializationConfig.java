package com.metasearch.CAT.scheduled;

import java.util.Map;

public interface TaskInitializationConfig {

    Map<String, String> getEnabledFactories();

    Map<String, String> getEnabledFactories(ExecutionInterval interval);

}
