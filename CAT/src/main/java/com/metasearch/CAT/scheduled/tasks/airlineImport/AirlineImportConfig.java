package com.metasearch.CAT.scheduled.tasks.airlineImport;

import com.metasearch.CAT.scheduled.tasks.TaskConfigBase;

public class AirlineImportConfig extends TaskConfigBase {

    public AirlineImportConfig(String taskName) {
        super(taskName);
    }

    public String getResource() {
        return super.getProp("resource");
    }
}
