package com.metasearch.CAT.scheduled.tasks.airportImport;

import com.metasearch.CAT.scheduled.tasks.TaskConfigBase;

public class AirportImportConfig extends TaskConfigBase {

    public AirportImportConfig(String taskName) {
        super(taskName);
    }

    public String getResource() {
        return super.getProp("resource");
    }

    public String getCountryCodesResource() {
        return super.getProp("countryCodes.resource");
    }
}
