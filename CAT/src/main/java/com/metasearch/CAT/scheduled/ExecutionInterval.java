package com.metasearch.CAT.scheduled;

public enum  ExecutionInterval {
    HOURLY,
    DAILY,
    WEEKLY,
    MONTHLY
}
