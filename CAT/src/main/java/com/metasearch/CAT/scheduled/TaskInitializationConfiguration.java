package com.metasearch.CAT.scheduled;

import com.metasearch.CAT.general.Env;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class TaskInitializationConfiguration implements TaskInitializationConfig {

    private static final String PROP_FACTORY_CONFIG = "scheduled.task.";

    private static final String PROP_FACTORY = PROP_FACTORY_CONFIG + "factory.";

    private static final String PROP_EXECUTION_INTERVAL = ".executionInterval";

    private final Env env;

    public TaskInitializationConfiguration(Env environment) {
        this.env = environment;
    }

    @Override
    public Map<String, String> getEnabledFactories() {
        Map<String, String> allFactories = getAllFactories();
        Map<String, String> filteredFactories = new HashMap<>();

        for (String taskName : allFactories.keySet()) {
            if (taskName != null && allFactories.get(taskName) != null) {
                filteredFactories.put(taskName, allFactories.get(taskName));
            }
        }

        return filteredFactories;
    }

    private Map<String, String> getAllFactories() {
        Map<String, String> factories = new HashMap<>();
        boolean didFind = true;
        for (int i = 1; didFind; i++) {
            String property = env.getProp(PROP_FACTORY + i);
            if (property != null) {
                String[] split = property.split(",");

                if (split.length == 2) {
                    factories.put(split[0], split[1]);
                }
            } else {
                didFind = false;
            }
        }

        return factories;
    }

    @Override
    public Map<String, String> getEnabledFactories(ExecutionInterval interval) {
        Map<String, String> filteredFactories = new HashMap<>();
        Map<String, String> enabledFactories = getEnabledFactories();
        for (String taskName : enabledFactories.keySet()) {
            ExecutionInterval taskInterval = getExecutionInterval(taskName);
            if (taskInterval != null && taskInterval.equals(interval)) {
                filteredFactories.put(taskName, enabledFactories.get(taskName));
            }
        }

        return filteredFactories;
    }

    private ExecutionInterval getExecutionInterval(String taskName) {
        String executionInterval = env.getProp(PROP_FACTORY_CONFIG + taskName + PROP_EXECUTION_INTERVAL);
        if (executionInterval != null) {
            return ExecutionInterval.valueOf(executionInterval);
        }

        return null;
    }

}
