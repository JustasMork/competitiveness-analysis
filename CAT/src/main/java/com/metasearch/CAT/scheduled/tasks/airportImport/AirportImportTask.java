package com.metasearch.CAT.scheduled.tasks.airportImport;


import com.catModel.Airport;
import com.metasearch.CAT.model.DAO.AirportDao;
import com.metasearch.CAT.general.DaoManager;
import com.metasearch.CAT.scheduled.TaskContext;
import com.metasearch.CAT.scheduled.tasks.TaskUtils;
import com.metasearch.CAT.scheduled.tasks.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;

import static com.metasearch.CAT.scheduled.tasks.TaskUtils.cleanUpString;

public class AirportImportTask extends Task<AirportImportConfig> {

    Logger log = LoggerFactory.getLogger(AirportImportTask.class);

    private static String SEPARATOR = ",";

    private static int IATA_INDEX = 4;

    private static int ICAO_INDEX = 5;

    private static int NAME_INDEX = 1;

    private static int COUNTRY_INDEX = 3;

    private static int REGION_INDEX = 11;

    private static int CITY_INDEX = 2;

    private AirportDao airportDao;

    private CountryCodeParser countryCodeParser;

    public AirportImportTask(AirportImportConfig config) {
        super(config);
        airportDao = DaoManager.getInstance().getAirportDao();
    }

    @Override
    public void runTask(TaskContext context) {
        countryCodeParser = new CountryCodeParser(getConfig());
        FileInputStream fileStream = getFileStream();
        if (fileStream == null) {
            return;
        }

        readLines(fileStream);
    }

    private FileInputStream getFileStream() {
        try {
            return new FileInputStream(getResourceFile());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    private File getResourceFile() {
        return new File(TaskUtils.getFilePath(getConfig().getResource()));
    }

    private void readLines(FileInputStream fileStream) {
        int notInsertedRecords = 0;
        try (Scanner scanner = new Scanner(fileStream)) {
            while(scanner.hasNextLine()) {
                Airport airport = parseLine(scanner.nextLine());
                if (isValid(airport)) {
                    insertAirport(airport);
                } else {
                    logNotInsertedAirport(airport);
                    notInsertedRecords++;
                }
            }
        }

        if (notInsertedRecords != 0) {
            log.warn("Failed to insert " + notInsertedRecords + " airports");
        }
    }

    private Airport parseLine(String line) {
        String[] split = line.split(SEPARATOR);
        return constructAirport(split);
    }

    private Airport constructAirport(String[] split) {
        Airport airport = new Airport();
        airport.setIata(cleanUpString(split[IATA_INDEX]));
        airport.setIcao(cleanUpString(split[ICAO_INDEX]));
        airport.setName(cleanUpString(split[NAME_INDEX]));
        airport.setCity(cleanUpString(split[CITY_INDEX]));
        airport.setCountry(cleanUpString(split[COUNTRY_INDEX]));
        airport.setRegion(cleanUpString(split[REGION_INDEX]));
        airport.setCountryCode(countryCodeParser.getCode(airport.getCountry()));

        return airport;
    }

    private boolean isValid(Airport airport) {
        return (airport.getIata() != null)
                && airport.getIata().matches("[A-Z]{3}")
                && (airport.getCountryCode() != null)
                && (airport.getCountryCode().length() == 2);
    }

    private void logNotInsertedAirport(Airport airport) {
        log.info("Airport not inserted: " + airport);
    }

    /**
     * protected for ease of testing
     */
    protected void insertAirport(Airport airport) {
        Airport dbRecord = airportDao.getAirport(airport.getIata());

        if (dbRecord == null) {
            try {
                airportDao.insertAirport(airport);
            } catch (DataIntegrityViolationException e) {
                e.printStackTrace();
            }
        } else {
            if (!airport.equals(dbRecord)) {
                airportDao.updateAirport(airport);
            }
        }

    }
}
