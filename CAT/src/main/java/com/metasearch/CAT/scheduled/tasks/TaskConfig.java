package com.metasearch.CAT.scheduled.tasks;

public interface TaskConfig {

    boolean isEnabled();

    String getTaskName();

    String getProp(String suffix);

}
