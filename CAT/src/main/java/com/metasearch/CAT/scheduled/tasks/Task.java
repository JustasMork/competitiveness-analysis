package com.metasearch.CAT.scheduled.tasks;

import com.metasearch.CAT.scheduled.TaskContext;

public abstract class Task <T extends TaskConfig> {

    private T config;

    public Task(T config) {
        this.config = config;
    }

    protected T getConfig() {
        return config;
    }

    public abstract void runTask(TaskContext context);

    public void afterRun(TaskContext context) {
        // Default
    }

    public void beforeRun(TaskContext context) {
        // Default
    }

    public String getTaskName() {
        return config.getTaskName();
    }


}
