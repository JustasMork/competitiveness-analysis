package com.metasearch.CAT.scheduled;

import com.metasearch.CAT.scheduled.tasks.Task;
import com.metasearch.CAT.scheduled.tasks.TaskConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.lang.reflect.Constructor;
import java.util.Arrays;

@Service
public class TaskFactoryManager implements TaskFactory {

    private Logger log = LoggerFactory.getLogger(TaskManager.class);

    @Override
    public Task<TaskConfig> createTask(String taskName, String classpath) {

        try {
            Constructor<Task<TaskConfig>> constructor = getTaskConstructor(classpath);
            TaskConfig config = getTaskConfig(taskName, constructor);

            if (config == null) {
                return null;
            }

            return constructor.newInstance(config);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private Constructor<Task<TaskConfig>> getTaskConstructor(String classpath) throws Exception {
        Class clazz = Class.forName(classpath);
        Constructor[] constructors = clazz.getConstructors();
        if (constructors.length != 1) {
            String msg = "All tasks must have only one constructor: " + clazz.getSimpleName();
            failCreation(msg);
        }

        return (Constructor<Task<TaskConfig>>) constructors[0];
    }

    private TaskConfig getTaskConfig(String taskName, Constructor<Task<TaskConfig>> constructor) throws Exception {
        Class<?>[] parameters = constructor.getParameterTypes();

        if (parameters.length != 1) {
            String msg = "Unknown amount of constructor params: " + Arrays.toString(parameters);
            failCreation(msg);
            return null;
        }

        return (TaskConfig) parameters[0].getConstructor(String.class).newInstance(taskName);
    }


    private void failCreation(String errorMessage) {
        log.error(errorMessage);
        throw new UnsupportedOperationException(errorMessage);
    }
}
