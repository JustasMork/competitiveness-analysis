package com.metasearch.CAT.general;

import com.metasearch.CAT.model.DAO.*;
import com.metasearch.CAT.model.clients.ScrapeRequestsDaoClient;
import com.metasearch.CAT.model.clients.ScrapeResultsDaoClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
class DaoManagerInitializer {

    @Autowired
    private AirportDao airportDao;

    @Autowired
    private AirlineDao airlineDao;

    @Autowired
    private ScrapeRequestsDao scrapeRequestsDao;

    @Autowired
    private FlightScrapeRequestDao flightScrapeRequestDao;

    @Autowired
    private FlightSearchResultDao flightSearchResultDao;

    @Autowired
    private PriceInfoDao priceInfoDao;

    @Autowired
    private SegmentDao segmentDao;

    @Autowired
    private TripDao tripDao;

    @PostConstruct
    private void initializeManager() {
        DaoManager instance = DaoManager.getInstance();
        instance.setAirportDao(airportDao);
        instance.setAirlineDao(airlineDao);
        instance.setScrapeRequestsDao(buildScrapeRequestsClient());
        instance.setScrapeResultsDao(buildScrapeResultsClient());
    }

    private ScrapeRequestsDaoClient buildScrapeRequestsClient() {
        return new ScrapeRequestsDaoClient(scrapeRequestsDao, flightScrapeRequestDao);
    }

    private ScrapeResultsDaoClient buildScrapeResultsClient() {
        return new ScrapeResultsDaoClient.Builder()
                .with(flightSearchResultDao)
                .with(priceInfoDao)
                .with(segmentDao)
                .with(tripDao)
                .build();
    }

}
