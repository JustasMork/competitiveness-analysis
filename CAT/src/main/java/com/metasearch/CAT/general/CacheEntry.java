package com.metasearch.CAT.general;

class CacheEntry {

    private long expireTime;

    private String data;

    public CacheEntry(long expireTime, String data) {
        this.expireTime = expireTime;
        this.data = data;
    }

    public boolean isExpired() {
        return expireTime < System.currentTimeMillis();
    }

    public String getData() {
        return data;
    }
}
