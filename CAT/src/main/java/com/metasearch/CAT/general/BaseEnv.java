package com.metasearch.CAT.general;

import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class BaseEnv implements Env {

    private final Environment environment;

    public BaseEnv(Environment environment) {
        super();
        this.environment = environment;
    }

    @Override
    public boolean getBoolean(String prop) {
        return getBoolean(prop, false);
    }

    @Override
    public boolean getBoolean(String prop, boolean defaultVal) {
        String property = environment.getProperty(prop);
        if (property != null) {
            return property.equalsIgnoreCase("true");
        }

        return defaultVal;
    }

    @Override
    public List<String> getList(String prop) {
        return getList(prop, null);
    }

    @Override
    public List<String> getList(String prop, List<String> defaultVal) {
        String property = environment.getProperty(prop);
        if (property != null) {
            return List.of(property.split(","));
        }
        return defaultVal;
    }

    @Override
    public String getProp(String prop) {
        return environment.getProperty(prop);
    }

    @Override
    public String getProp(String prop, String defaultVal) {
        return environment.getProperty(prop, defaultVal);
    }

    @Override
    public Long getLong(String prop) {
        return Long.parseLong(Objects.requireNonNull(environment.getProperty(prop)));
    }

    @Override
    public Long getLong(String prop, Long defaultVal) {
        String propVal = environment.getProperty(prop);
        return propVal != null ? Long.parseLong(propVal) : defaultVal;
    }
}
