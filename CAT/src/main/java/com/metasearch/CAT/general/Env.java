package com.metasearch.CAT.general;

import java.util.List;

public interface Env {

    boolean getBoolean(String prop);

    boolean getBoolean(String prop, boolean defaultVal);

    List<String> getList(String prop);

    List<String> getList(String prop, List<String> defaultVal);

    String getProp(String prop);

    String getProp(String prop, String defaultVal);

    Long getLong(String prop);

    Long getLong(String prop, Long defaultVal);

}
