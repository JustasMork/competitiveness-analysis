package com.metasearch.CAT.general;

import com.metasearch.CAT.model.DAO.AirlineDao;
import com.metasearch.CAT.model.DAO.AirportDao;
import com.metasearch.CAT.model.clients.ScrapeRequestsDaoClient;
import com.metasearch.CAT.model.clients.ScrapeResultsDaoClient;

public class DaoManager {

    private static DaoManager instance;

    private AirportDao airportDao;

    private AirlineDao airlineDao;

    private ScrapeRequestsDaoClient scrapeRequestDao;

    private ScrapeResultsDaoClient scrapeResultsDao;

    public static DaoManager getInstance() {
        if (instance == null) {
            instance = new DaoManager();
        }
        return instance;
    }

    void setAirportDao(AirportDao airportDao) {
        this.airportDao = airportDao;
    }

    void setAirlineDao(AirlineDao airlineDao) {
        this.airlineDao = airlineDao;
    }

    void setScrapeRequestsDao(ScrapeRequestsDaoClient client) {
        this.scrapeRequestDao = client;
    }

    void setScrapeResultsDao(ScrapeResultsDaoClient client) {
        this.scrapeResultsDao = client;
    }

    public AirportDao getAirportDao() {
        return airportDao;
    }

    public AirlineDao getAirlineDao() {
        return airlineDao;
    }

    public ScrapeRequestsDaoClient getScrapeRequestDao() {
        return scrapeRequestDao;
    }

    public ScrapeResultsDaoClient getScrapeResultsDao() {
        return scrapeResultsDao;
    }

}
