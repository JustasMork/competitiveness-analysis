package com.metasearch.CAT.general;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class Cache {

    private static Cache instance;

    private Map<String, CacheEntry> cache = new HashMap<>();

    public static Cache getInstance() {
        if (instance == null) {
            instance = new Cache();
        }

        return instance;
    }

    public void add(String key, String data) {
        long dayInMillis = 24 * 60 * 60 * 1000;
        CacheEntry entry = new CacheEntry(System.currentTimeMillis() + dayInMillis, data);
        cache.put(key, entry);
    }

    public void add(String key, BigDecimal data) {
        add(key, data.toString());
    }

    public String get(String key) {
        CacheEntry entry = cache.get(key);
        if (entry == null) {
            return null;
        }

        if (entry.isExpired()) {
            cache.remove(key);
            return null;
        }

        return entry.getData();
    }

    public BigDecimal getDecimal(String key) {
        String data = get(key);
        if (data != null) {
            return new BigDecimal(data);
        }
        return null;
    }


}
