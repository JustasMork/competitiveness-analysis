package com.metasearch.CAT.model.clients;

import com.catModel.*;
import com.metasearch.CAT.model.DAO.FlightSearchResultDao;
import com.metasearch.CAT.model.DAO.PriceInfoDao;
import com.metasearch.CAT.model.DAO.SegmentDao;
import com.metasearch.CAT.model.DAO.TripDao;

import java.util.List;

public class ScrapeResultsDaoClient {

    private FlightSearchResultDao searchResultDao;

    private TripDao tripDao;

    private SegmentDao segmentDao;

    private PriceInfoDao priceInfoDao;

    private ScrapeResultsDaoClient(FlightSearchResultDao searchResultDao,
                                   TripDao tripDao,
                                   SegmentDao segmentDao,
                                   PriceInfoDao priceInfoDao) {

        this.searchResultDao = searchResultDao;
        this.tripDao = tripDao;
        this.segmentDao = segmentDao;
        this.priceInfoDao = priceInfoDao;
    }

    public void insert(ScrapeResults results) {
        for (ScrapeResult scrapeResult : results.getScrapeResults()) {
            if (scrapeResult instanceof FlightSearchResult) {
                insertFlightResult(results.getId(), (FlightSearchResult) scrapeResult);
            }
        }
    }

    private void insertFlightResult(int requestId, FlightSearchResult result) {
        searchResultDao.insert(requestId, result);
        int resultId = result.getId();
        for (Trip trip : result.getTrips()) {
            tripDao.insert(resultId, trip);
            insertSegments(trip.getId(), trip);
            priceInfoDao.insert(trip.getId(), trip.getPriceInfos());
        }
    }

    private void insertSegments(int tripId, Trip trip) {
        int legCounter = 1;
        for (Leg leg : trip.getLegs()) {
            for (Segment segment : leg.getSegments()) {
                segmentDao.insert(tripId, legCounter, segment);
            }
            legCounter++;
        }
    }

    public ScrapeResults getScrapeResults(int requestId) {
        ScrapeResults results = new ScrapeResults();
        results.setId(requestId);
        List<FlightSearchResult> flightSearchResults = getFlightResults(requestId);
        flightSearchResults.forEach(results::addResult);
        return results;
    }

    public List<FlightSearchResult> getFlightResults(int requestId) {

        List<FlightSearchResult> results = searchResultDao.getResults(requestId);
        for (FlightSearchResult result : results) {
            List<Integer> tripIds = tripDao.getTripIds(result.getId());
            for (Integer tripId : tripIds) {
                result.addTrip(getTrip(tripId));
            }

        }
        return results;
    }

    private Trip getTrip(Integer tripId) {
        Trip trip = new Trip();

        int counter = 1;
        List<Segment> segments = segmentDao.getSegments(tripId, counter++);
        while (segments != null && segments.size() > 0) {
            Leg leg = new Leg();
            leg.setSegments(segments);
            trip.addLeg(leg);

            segments = segmentDao.getSegments(tripId, counter++);
        }
        trip.setPriceInfos(priceInfoDao.getPriceInfos(tripId));

        return trip;
    }

    /**
     * Inner scrape results DAO client builder class
     */
    public static class Builder {

        private FlightSearchResultDao searchResultDao;

        private TripDao tripDao;

        private SegmentDao segmentDao;

        private PriceInfoDao priceInfoDao;

        public Builder with(FlightSearchResultDao searchResultDao) {
            this.searchResultDao = searchResultDao;
            return this;
        }

        public Builder with(TripDao tripDao) {
            this.tripDao = tripDao;
            return this;
        }

        public Builder with(SegmentDao segmentDao) {
            this.segmentDao = segmentDao;
            return this;
        }

        public Builder with(PriceInfoDao priceInfoDao) {
            this.priceInfoDao = priceInfoDao;
            return this;
        }


        public ScrapeResultsDaoClient build() {
            return new ScrapeResultsDaoClient(searchResultDao, tripDao, segmentDao, priceInfoDao);
        }

    }
}
