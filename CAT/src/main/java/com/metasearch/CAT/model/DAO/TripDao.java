package com.metasearch.CAT.model.DAO;

import com.catModel.Trip;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface TripDao {

    void insert(@Param("resultId") int resultId, @Param("trip") Trip trip);

    List<Integer> getTripIds(@Param("resultId") int resultId);


}
