package com.metasearch.CAT.model.clients;

import com.catModel.FlightScrapeRequest;
import com.catModel.ScrapeRequests;
import com.metasearch.CAT.model.DAO.FlightScrapeRequestDao;
import com.metasearch.CAT.model.DAO.ScrapeRequestsDao;

import java.util.List;
import java.util.stream.Collectors;

public class ScrapeRequestsDaoClient {

    private ScrapeRequestsDao scrapeRequestsDao;

    private FlightScrapeRequestDao flightRequestDao;

    /**
     * This class should be initialized only in DaoManagerInitializer
     * @param scrapeRequestsDao
     */
    public ScrapeRequestsDaoClient(ScrapeRequestsDao scrapeRequestsDao, FlightScrapeRequestDao flightRequestDao) {
        this.scrapeRequestsDao = scrapeRequestsDao;
        this.flightRequestDao = flightRequestDao;
    }

    public void insert(ScrapeRequests requests) {
        scrapeRequestsDao.insert(requests);
        List<FlightScrapeRequest> flightRequests = requests.getRequests().stream()
                .filter(item -> item instanceof FlightScrapeRequest)
                .map(item -> (FlightScrapeRequest) item)
                .collect(Collectors.toList());
        flightRequestDao.insert(flightRequests, requests.getId());
    }

    public ScrapeRequests getScrapingTask() {
        ScrapeRequests scrapingTask = scrapeRequestsDao.getScrapingTask();
        if (scrapingTask != null) {
            scrapeRequestsDao.changeStatus(scrapingTask.getId(), 1);
            List<FlightScrapeRequest> requests = flightRequestDao.getRequests(scrapingTask.getId());
            requests.forEach(scrapingTask::addRequest);
        }

        return scrapingTask;
    }

    public List<ScrapeRequests> getAllScrapingTasks() {
        List<ScrapeRequests> scrapingTasks = scrapeRequestsDao.getAllScrapingTasks();
        for (ScrapeRequests scrapingTask : scrapingTasks) {
            List<FlightScrapeRequest> requests = flightRequestDao.getRequests(scrapingTask.getId());
            requests.forEach(scrapingTask::addRequest);
        }
        return scrapingTasks;
    }

    public ScrapeRequests getById(int id) {
        return scrapeRequestsDao.getTaskById(id);
    }

    public void setRequestCompleted(int taskId) {
        scrapeRequestsDao.changeStatus(taskId, 2);
    }

}
