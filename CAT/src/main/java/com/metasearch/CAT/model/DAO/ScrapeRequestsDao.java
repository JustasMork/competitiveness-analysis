package com.metasearch.CAT.model.DAO;

import com.catModel.ScrapeRequests;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ScrapeRequestsDao {

    void insert(@Param("scrapeRequests") ScrapeRequests scrapeRequests);

    ScrapeRequests getScrapingTask();

    ScrapeRequests getTaskById(int id);

    List<ScrapeRequests> getAllScrapingTasks();

    void changeStatus(@Param("requestsId") int id, @Param("started") int i);

}
