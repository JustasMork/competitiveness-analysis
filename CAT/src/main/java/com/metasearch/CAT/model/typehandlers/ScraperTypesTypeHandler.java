package com.metasearch.CAT.model.typehandlers;

import com.catModel.ScraperType;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

@MappedJdbcTypes(JdbcType.VARCHAR)
public class ScraperTypesTypeHandler extends BaseTypeHandler<List<ScraperType>> {
    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, List<ScraperType> parameter, JdbcType jdbcType) throws SQLException {
        // Ignore
    }

    @Override
    public List<ScraperType> getNullableResult(ResultSet rs, String columnName) throws SQLException {
        String json = rs.getString(columnName);
        if (json != null && !json.isBlank()) {
            ObjectMapper mapper = new ObjectMapper();
            try {
                ScraperType[] scraperTypes = mapper.readValue(json, ScraperType[].class);
                return Arrays.asList(scraperTypes);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }
        return List.of();
    }

    @Override
    public List<ScraperType> getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        // Ignore
        return null;
    }

    @Override
    public List<ScraperType> getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        // Ignore
        return null;
    }
}
