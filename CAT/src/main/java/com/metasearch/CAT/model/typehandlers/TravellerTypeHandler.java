package com.metasearch.CAT.model.typehandlers;

import com.catModel.PTC;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class TravellerTypeHandler extends BaseTypeHandler<Map<PTC, Integer>> {
    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, Map<PTC, Integer> parameter, JdbcType jdbcType) throws SQLException {
        // Ignore
    }

    @Override
    public Map<PTC, Integer> getNullableResult(ResultSet rs, String columnName) throws SQLException {
        String json = rs.getString(columnName);
        if (json != null && !json.isBlank()) {
            ObjectMapper mapper = new ObjectMapper();
            TypeReference<HashMap<PTC, Integer>> typeRef = new TypeReference<>() {};
            try {
                return mapper.readValue(json, typeRef);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public Map<PTC, Integer> getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        // Ignore
        return null;
    }

    @Override
    public Map<PTC, Integer> getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        // Ignore
        return null;
    }
}
