package com.metasearch.CAT.model.api;

public class Label {

    private String title;

    public Label(String name) {
        this.title = name;
    }

    public String getTitle() {
        return title;
    }
}
