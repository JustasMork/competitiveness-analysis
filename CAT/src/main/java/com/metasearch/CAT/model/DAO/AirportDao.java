package com.metasearch.CAT.model.DAO;


import com.catModel.Airport;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


@Mapper
public interface AirportDao {

    void insertAirport(@Param("airport") Airport airport);

    void updateAirport(@Param("airport") Airport airport);

    Airport getAirport(String iata);

    List<Airport> findAirports(@Param("query") String query);

}
