package com.metasearch.CAT.model.DAO;

import com.catModel.PriceInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface PriceInfoDao {

    void insert(@Param("tripId") int tripId, @Param("priceInfos") List<PriceInfo> priceInfo);

    List<PriceInfo> getPriceInfos(@Param("tripId") int tripId);
}
