package com.metasearch.CAT.model.DAO;

import com.catModel.FlightScrapeRequest;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface FlightScrapeRequestDao {

    void insert(@Param("requests") List<FlightScrapeRequest> requests, @Param("requestsId") int requestsId);

    List<FlightScrapeRequest> getRequests(@Param("requestsId") int requestsId);
}
