package com.metasearch.CAT.model.api;

public interface Table {

    String getType();

    String getTableName();
}
