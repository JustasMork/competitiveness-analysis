package com.metasearch.CAT.model.api;

import java.math.BigDecimal;

public class StringDecimalEntry {

    private String x;

    private BigDecimal y;

    public String getX() {
        return x;
    }

    public void setX(String x) {
        this.x = x;
    }

    public BigDecimal getY() {
        return y;
    }

    public void setY(BigDecimal y) {
        this.y = y;
    }
}
