package com.metasearch.CAT.model.DAO;

import com.catModel.FlightSearchResult;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface FlightSearchResultDao {

    void insert(@Param("requestId") int requestId, @Param("result") FlightSearchResult scrapeResult);

    List<FlightSearchResult> getResults(@Param("requestId") int requestId);

}
