package com.metasearch.CAT.model.DAO;

import com.catModel.Segment;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SegmentDao {

    void insert(@Param("tripId") int tripId, @Param("legId") int legId, @Param("segment") Segment segment);

    List<Segment> getSegments(@Param("tripId") int tripId, @Param("legId") int legId);

}
