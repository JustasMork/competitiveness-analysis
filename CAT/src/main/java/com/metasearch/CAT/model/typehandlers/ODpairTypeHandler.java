package com.metasearch.CAT.model.typehandlers;

import com.catModel.ODpair;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;


public class ODpairTypeHandler extends BaseTypeHandler<List<ODpair>> {

    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, List<ODpair> parameter, JdbcType jdbcType) throws SQLException {
        // Ignore
    }

    @Override
    public List<ODpair> getNullableResult(ResultSet rs, String columnName) throws SQLException {
        String json = rs.getString(columnName);
        if (json != null && !json.isBlank()) {
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            try {
                ODpair[] scraperTypes = mapper.readValue(json, ODpair[].class);
                return Arrays.asList(scraperTypes);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }
        return List.of();
    }

    @Override
    public List<ODpair> getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        // Ignore
        return null;
    }

    @Override
    public List<ODpair> getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        // Ignore
        return null;
    }
}
