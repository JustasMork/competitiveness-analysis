package com.metasearch.CAT.model;

public class Tuple<K,V> {

    private K first;

    private V second;

    public Tuple(K first, V second) {
        this.first = first;
        this.second = second;
    }

    public K getFirst() {
        return first;
    }

    public V getSecond() {
        return second;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;

        Tuple<K, V> other = (Tuple<K, V>) obj;
        return getFirst().equals(other.getFirst());
    }

    @Override
    public int hashCode() {
        return first.hashCode();
    }
}
