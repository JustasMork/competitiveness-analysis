package com.metasearch.CAT.model.api;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class StringFloatEntry {

    private String x;

    private Float y;

    private BigDecimal yBigDec;

    private String unit;

    public String getX() {
        return x;
    }

    public void setX(String x) {
        this.x = x;
    }

    public BigDecimal getY() {
        if (yBigDec != null) {
            yBigDec = yBigDec.setScale(3, RoundingMode.HALF_UP);
            return yBigDec;
        }

        BigDecimal xVal = new BigDecimal(y);
        xVal = xVal.setScale(3, RoundingMode.HALF_UP);
        return xVal;
    }

    public void setY(Float y) {
        this.y = y;
    }

    public void setY(BigDecimal y) {
        this.yBigDec = y;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
