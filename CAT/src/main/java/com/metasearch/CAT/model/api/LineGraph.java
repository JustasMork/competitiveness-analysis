package com.metasearch.CAT.model.api;

import java.util.List;

public class LineGraph implements Table {

    private String tableName;

    private List<List<StringFloatEntry>> entries;

    private List<Label> labels;

    @Override
    public String getType() {
        return "LINE";
    }

    @Override
    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public List<List<StringFloatEntry>> getEntries() {
        return entries;
    }

    public void setEntries(List<List<StringFloatEntry>> entries) {
        this.entries = entries;
    }

    public List<Label> getLabels() {
        return labels;
    }

    public void setLabels(List<Label> labels) {
        this.labels = labels;
    }
}
