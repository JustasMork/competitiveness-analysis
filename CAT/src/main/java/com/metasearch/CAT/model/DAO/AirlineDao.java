package com.metasearch.CAT.model.DAO;


import com.catModel.Airline;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface AirlineDao {

    void insertAirline(@Param("airline") Airline airline);

    void updateAirline(@Param("airline") Airline airline);

    Airline getAirline(String code);

}
