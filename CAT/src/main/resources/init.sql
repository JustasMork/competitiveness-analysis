CREATE DATABASE IF NOT EXISTS competitiveness
    CHARACTER SET utf8mb4
    COLLATE utf8mb4_unicode_ci;

USE competitiveness;

CREATE TABLE IF NOT EXISTS `airport`
(
    `iata`         varchar(3)   NOT NULL PRIMARY KEY,
    `icao`         varchar(4),
    `name`         varchar(120) NOT NULL,
    `country`      varchar(60)  NOT NULL,
    `country_code` varchar(2),
    `region`       varchar(60)  NOT NULL,
    `city`         varchar(120) NOT NULL
) DEFAULT CHARSET = utf8
  COLLATE utf8_general_ci
    COMMENT ='Table with airports';

CREATE TABLE IF NOT EXISTS `airline`
(
    `code`    varchar(2)   NOT NULL PRIMARY KEY,
    `iata`    varchar(3)   NOT NULL,
    `name`    varchar(120) NOT NULL,
    `country` varchar(60)  NOT NULL
) DEFAULT CHARSET = utf8
  COLLATE utf8_general_ci
    COMMENT ='Table with airlines';

CREATE TABLE IF NOT EXISTS `scrape_request`
(
    `id`            int(10)     NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `browser`       varchar(10) NOT NULL,
    `scraper_types` text        NOT NULL,
    `date_created`  datetime    NOT NULL DEFAULT NOW()
) DEFAULT CHARSET = utf8
  COLLATE utf8_general_ci
    COMMENT ='Table for storing scraping requests';

CREATE TABLE IF NOT EXISTS `flight_scrape_request`
(
    `request_id`  int(10)     NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `requests_id` int         NOT NULL,
    `cc`          varchar(2)  NOT NULL,
    `cabin`       varchar(25) NOT NULL,
    `itinerary`   text        NOT NULL,
    `travellers`  text        NOT NULL,
    FOREIGN KEY (requests_id) REFERENCES scrape_request (id)
) DEFAULT CHARSET = utf8
  COLLATE utf8_general_ci
    COMMENT ='Table for storing flight scraping requests';

CREATE TABLE IF NOT EXISTS `flight_search_result`
(
    `id`                  int(10)     NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `request_id`          int(10)     NOT NULL,
    `scraper_type`        varchar(25) NOT NULL,
    `scraping_time`       datetime DEFAULT NOW(),
    `result_loading_time` int(10),
    FOREIGN KEY (request_id) REFERENCES scrape_request (id)
) DEFAULT CHAR SET = utf8
  COLLATE utf8_general_ci
    COMMENT = 'Table for storing flight scraping results';

CREATE TABLE IF NOT EXISTS `trip`
(
    `id`               int(10) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `search_result_id` int(10) NOT NULL,
    FOREIGN KEY (search_result_id) REFERENCES flight_search_result (id)
) DEFAULT CHARSET = utf8
  COLLATE utf8_general_ci
    COMMENT = 'Table for storing relations between flight search result, segments and price infos.';

CREATE TABLE IF NOT EXISTS `segment`
(
    `id`               int(10)     NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `trip_id`          int(10)     NOT NULL,
    `leg_id`           int(10)     NOT NULL,
    `origin_iata`      varchar(3)  NOT NULL,
    `destination_iata` varchar(3)  NOT NULL,
    `departure_dt`     varchar(25) NOT NULL,
    `arrival_dt`       varchar(25) NOT NULL,
    `airline`          varchar(2),
    `flight_nr`        varchar(25),
    FOREIGN KEY (trip_id) REFERENCES trip (id)
) DEFAULT CHARSET = utf8
  COLLATE utf8_general_ci
    COMMENT = 'Table for storing segment information.';

CREATE TABLE IF NOT EXISTS `price_info`
(
    `id`            int(10)       NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `trip_id`       int(10)       NOT NULL,
    `provider`      varchar(50)   NOT NULL,
    `amount`        decimal(8, 2) NOT NULL,
    `currency_code` varchar(3)    NOT NULL,
    FOREIGN KEY (trip_id) REFERENCES trip (id)
) DEFAULT CHARSET = utf8
  COLLATE utf8_general_ci
    COMMENT = 'Table for storing price and provider information.';

