var performance = window.performance
    || window.mozPerformance
    || window.msPerformance
    || window.webkitPerformance
    || {};

var network = performance.getEntries() || {};

return JSON.stringify(network)