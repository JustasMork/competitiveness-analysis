package com.scraper.scraper.parsers;



import com.catModel.ScrapeRequest;
import com.catModel.ScrapeResult;
import com.catModel.ScraperType;
import org.openqa.selenium.WebDriver;

public abstract class Scraper<ScraperConfig extends ScraperConfigBase> {

    private ScraperConfig config;

    private WebDriver driver;

    public Scraper(ScraperConfig config, WebDriver driver){
        this.config = config;
        this.driver = driver;
    }

    public ScraperConfig getConfig() {
        return config;
    }

    public ScrapeResult scrape(ScrapeRequest request) {
        ScrapeResult result = null;
        try {
            beforeExecution(driver);
            driver.get(getRequest(request));
            beforeParsing(driver);
            result = parse(driver, request);
            result.setScraperType(getConfig().getScraperType());
            afterExecution(driver, result);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public ScraperType getType() {
        return getConfig().getScraperType();
    }

    public void quitDriver() {
        if (driver != null) {
            driver.quit();
        }
    }

    private String getRequest(ScrapeRequest request) throws Exception {
        String requestUrl = getRequestUrl(request) + config.getRequestSuffix();
        System.out.println("Loading: " + requestUrl);

        return requestUrl;
    }

    protected abstract String getRequestUrl(ScrapeRequest request) throws Exception;

    protected abstract ScrapeResult parse(WebDriver driver, ScrapeRequest request) throws Exception;

    protected void beforeExecution(WebDriver driver) {
        // Default behaviour
    }

    protected void beforeParsing(WebDriver driver) throws InterruptedException {
        // Default behaviour
    }

    protected void afterExecution(WebDriver driver, ScrapeResult result) {
        // Default behaviour
    }

}
