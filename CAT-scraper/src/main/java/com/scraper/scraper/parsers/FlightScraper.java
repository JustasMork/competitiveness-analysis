package com.scraper.scraper.parsers;

import com.catModel.FlightScrapeRequest;
import com.catModel.FlightSearchResult;
import com.catModel.ScrapeRequest;
import com.catModel.ScrapeResult;
import org.openqa.selenium.WebDriver;

public abstract class FlightScraper<ScraperConfig extends ScraperConfigBase> extends Scraper<ScraperConfig> {

    public FlightScraper(ScraperConfig scraperConfigBase, WebDriver driver) {
        super(scraperConfigBase, driver);
    }

    @Override
    public String getRequestUrl(ScrapeRequest request) throws Exception {
        return getRequestUrl((FlightScrapeRequest) request);
    }

    @Override
    protected ScrapeResult parse(WebDriver driver, ScrapeRequest request) throws Exception {
        return parse(driver, (FlightScrapeRequest) request);
    }

    protected abstract ScrapeResult parse(WebDriver driver, FlightScrapeRequest request) throws Exception;

    protected abstract String getRequestUrl(FlightScrapeRequest request) throws Exception;
}
