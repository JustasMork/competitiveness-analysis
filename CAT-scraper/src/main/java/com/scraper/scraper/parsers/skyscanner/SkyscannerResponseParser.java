package com.scraper.scraper.parsers.skyscanner;

import com.catModel.*;
import com.google.common.collect.ImmutableList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SkyscannerResponseParser {

    public static String OFFER_CONTAINER = "FlightsTicket_container";
    public static String MAIN_PRICE_CONTAINER = "Price_mainPriceContainer";
    public static String CURRENCY_CONTAINER = "CultureSelectorButton_CultureSelectorButton__currency-label--mobile";

    public static String MODAL_BACK_BTN = "BpkNavigationBarButtonLink_bpk-navigation-bar-button-link";
    public static String ITINERARY_EXPAND_BTN = "LegSummary_container";
    public static String LEG_INFO_CONTAINER = "Itinerary_leg";
    public static String LEG_DATE_CONTAINER = "LegHeader_legDate";
    public static String LEG_SEGMENT_CONTAINER = "LegSegmentDetails_container";
    public static String FLIGHT_NR_CONTAINER = "OperatedBy_operatedBy";
    public static String SEGMENT_TIMES_CONTAINER = "Times_segmentTimes";
    public static String SEGMENT_ROUTES_CONTAINER = "Routes_routes";
    public static String PRICING_ITEM_CONTAINER = "PricingItem_agentRow";
    public static String PRICING_NAME_CONTAINER = "AgentDetails_agentContainer";
    public static String PRICING_COST_CONTAINER = "PricingItem_price";
    public static String TIME_OFFSET_TOOLTIP = "TimeWithOffsetTooltip_offsetTooltip";

    private SkyscannerConfig config;

    private String currencyCode;

    public SkyscannerResponseParser(SkyscannerConfig config) {
        this.config = config;
    }

    public Integer parseResultLoadingTime(String networkJsonString) {
        try {
            JSONArray networkLog = new JSONArray(networkJsonString);
            List<JSONObject> resultPollingEntries = getResultPollingEntries(networkLog);
            Integer loadingDuration = 0;

            for (JSONObject entry : resultPollingEntries) {
                loadingDuration += getApiCallDuration(entry);
            }

            return loadingDuration;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }


    private List<JSONObject> getResultPollingEntries(JSONArray logArray) throws JSONException {
        List<JSONObject> filteredEntries = new LinkedList<>();

        for (int i = 0; i < logArray.length(); i++) {
            JSONObject entry = logArray.getJSONObject(i);
            if (isResultPolling(entry)) {
                filteredEntries.add(entry);
            }
        }

        return filteredEntries;
    }

    private boolean isResultPolling(JSONObject entry) {
        try {
            return entry.getString("initiatorType").equals("fetch")
                    && entry.getString("name").contains(config.getResultsPollingMatcher());
        } catch (Exception e) {
            return false;
        }
    }

    private Integer getApiCallDuration(JSONObject entry) throws JSONException {
        return (int) entry.getLong("duration");
    }

    public Map<String, String> parseResultPageClasses(String htmlContent) {
        Map<String, String> classesMap = new HashMap<>();
        classesMap.put(OFFER_CONTAINER, parseElementClasses(OFFER_CONTAINER, htmlContent));
        classesMap.put(MAIN_PRICE_CONTAINER, parseElementClasses(MAIN_PRICE_CONTAINER, htmlContent));

        return classesMap;
    }

    private String parseElementClasses(String matcher, String htmlContent) {
        String regex = "(" + matcher + ".*?)\"";
        Pattern pattern = Pattern.compile(regex);
        Matcher regexMatcher = pattern.matcher(htmlContent);

        if (regexMatcher.find()) {
            return regexMatcher.group(1);
        }

        return null;
    }

    public void parseCurrencyCode(WebElement headerElement) {
        String headerHTML = headerElement.getAttribute("innerHTML");
        String className = parseElementClasses(CURRENCY_CONTAINER, headerHTML);
        WebElement currencyElement = headerElement.findElement(byExactClass(className));

        String text = currencyElement.getAttribute("innerHTML");
        this.currencyCode = text.substring(0, 3);
    }

    public Map<String, String> parseOfferDetailsClasses(String htmlContent) {
        Map<String, String> classesMap = new HashMap<>();

        List<String> classes = getAllContentClasses(htmlContent);
        List<String> matcherList = getOfferDetailsClassesMatchers();

        for (String cssClass : classes) {
            if (classesMap.size() == matcherList.size()) {
                return classesMap;
            }

            for (String matcher : matcherList) {
                if (!classesMap.containsKey(matcher)) {
                    if (cssClass.contains(matcher)) {
                        classesMap.put(matcher, cssClass);
                    }
                }
            }
        }

        return classesMap;
    }

    private List<String> getOfferDetailsClassesMatchers() {
        return ImmutableList.<String>builder()
                .add(MODAL_BACK_BTN)
                .add(ITINERARY_EXPAND_BTN)
                .add(LEG_INFO_CONTAINER)
                .add(LEG_DATE_CONTAINER)
                .add(LEG_SEGMENT_CONTAINER)
                .add(FLIGHT_NR_CONTAINER)
                .add(SEGMENT_TIMES_CONTAINER)
                .add(SEGMENT_ROUTES_CONTAINER)
                .add(PRICING_ITEM_CONTAINER)
                .add(PRICING_NAME_CONTAINER)
                .add(PRICING_COST_CONTAINER)
                .add(TIME_OFFSET_TOOLTIP)
                .build();
    }

    private List<String> getAllContentClasses(String htmlContent) {

        List<String> allClasses = new LinkedList<>();

        String regex = "class=\"(.*?)\"";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(htmlContent);

        while (matcher.find()) {
            allClasses.add(matcher.group(1));
        }

        return allClasses;
    }

    public Trip parseTrip(WebElement modalElement, Map<String, String> offerDetailsClasses) throws Exception {
        Trip trip = new Trip();

        List<WebElement> legContainerElements = modalElement.findElements(byExactClass(offerDetailsClasses.get(LEG_INFO_CONTAINER)));

        for (WebElement legContainer : legContainerElements) {
            trip.addLeg(parseLeg(legContainer, offerDetailsClasses));
        }

        return trip;
    }

    private Leg parseLeg(WebElement legContainer, Map<String, String> offerDetailsClasses) throws Exception {
        Leg leg = new Leg();

        SimpleDateFormat parseDateFormat = new SimpleDateFormat("E, d MMM yyyy");

        WebElement dateElement = legContainer.findElement(byExactClass(offerDetailsClasses.get(LEG_DATE_CONTAINER)));
        Date date = parseDateFormat.parse(dateElement.getText());

        List<WebElement> segmentContainers = legContainer.findElements(byExactClass(offerDetailsClasses.get(LEG_SEGMENT_CONTAINER)));
        for (WebElement segmentContainer : segmentContainers) {
            leg.addSegment(parseSegment(segmentContainer, offerDetailsClasses, date));
        }

        List<WebElement> flightNrElements = legContainer.findElements(byExactClass(offerDetailsClasses.get(FLIGHT_NR_CONTAINER)));
        for (int i = 0; i < flightNrElements.size(); i++) {
            parseAirlineData(flightNrElements.get(i), leg.getSegments().get(i));
        }

        return leg;
    }

    private void parseAirlineData(WebElement flightNrEl, Segment segment) {
        try {
            String flightNr = flightNrEl.getText();
            segment.setFlightNr(flightNr);
            Airline airline = new Airline();
            airline.setCode(flightNr.substring(0, 2));
            segment.setAirline(airline);
        } catch (NoSuchElementException e) {
            e.printStackTrace();
        }
    }

    private Segment parseSegment(WebElement segmentContainer, Map<String, String> offerDetailsClasses, Date date) throws Exception {
        Segment segment = new Segment();

        try {
            WebElement routesElement = segmentContainer.findElement(byExactClass(offerDetailsClasses.get(SEGMENT_ROUTES_CONTAINER)));
            List<WebElement> airportElements = routesElement.findElements(By.tagName("span"));
            if (airportElements.size() != 2) {
                throw new Exception("Bad airport information. Expected size: 2, Actual: " + airportElements.size());
            }

            String originIata = airportElements.get(0).getText().substring(0, 3);
            String destinationIata = airportElements.get(1).getText().substring(0, 3);

            Airport originAirport = new Airport();
            originAirport.setIata(originIata);
            segment.setOrigin(originAirport);

            Airport destinationAirport = new Airport();
            destinationAirport.setIata(destinationIata);
            segment.setDestination(destinationAirport);
        } catch (NoSuchElementException e) {
            e.printStackTrace();
        }

        WebElement timesContainer = segmentContainer.findElement(byExactClass(offerDetailsClasses.get(SEGMENT_TIMES_CONTAINER)));
        List<WebElement> timeElements = timesContainer.findElements(By.tagName("div"));
        if (timeElements.size() != 2) {
            throw new Exception("Bad times information. Expected size: 2, Actual: " + timeElements.size());
        }

        date = checkForDateChange(timeElements.get(0), offerDetailsClasses, date);
        segment.setDeparture(parseDate(timeElements.get(0), date));
        date = checkForDateChange(timeElements.get(1), offerDetailsClasses, date);
        segment.setArrival(parseDate(timeElements.get(1), date));

        return segment;
    }

    private Date checkForDateChange(WebElement timeEl, Map<String, String> offerDetailsClasses, Date date) {
        String tooltipClass = offerDetailsClasses.get(TIME_OFFSET_TOOLTIP);
        if (tooltipClass == null) {
            return date;
        }

        if (timeEl.findElements(byExactClass(tooltipClass)).size() < 1) {
            return date;
        }

        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DAY_OF_MONTH, 1);
        return c.getTime();
    }

    private Date parseDate(WebElement timeElement, Date date) throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        SimpleDateFormat yyyyMMdd = new SimpleDateFormat("yyyy-MM-dd");
        WebElement departureSpan = timeElement.findElements(By.tagName("span")).get(0);
        return dateFormat.parse(yyyyMMdd.format(date) + " " + departureSpan.getText());
    }

    public List<PriceInfo> parsePriceInfos(WebElement modalContainer, Map<String, String> classes) {
        List<PriceInfo> priceInfos = new ArrayList<>();
        List<WebElement> priceElements = modalContainer.findElements(byExactClass(classes.get(PRICING_ITEM_CONTAINER)));
        for (WebElement priceElement : priceElements) {
            priceInfos.add(parsePriceInfo(priceElement, classes));
        }

        return priceInfos;
    }

    private PriceInfo parsePriceInfo(WebElement priceElement, Map<String, String> classes) {
        PriceInfo priceInfo = new PriceInfo();

        WebElement nameEl = priceElement.findElement(byExactClass(classes.get(PRICING_NAME_CONTAINER)));
        priceInfo.setProvider(nameEl.findElement(By.tagName("span")).getAttribute("innerText"));

        WebElement costEl = priceElement.findElement(byExactClass(classes.get(PRICING_COST_CONTAINER)));
        priceInfo.setPrice(parsePrice(costEl));

        return priceInfo;
    }

    private Price parsePrice(WebElement costEl) {
        String priceStr = costEl.findElement(By.tagName("div")).findElement(By.tagName("span")).getAttribute("innerText");
        String[] priceSplit = priceStr.split(" ");
        return new Price(new BigDecimal(priceSplit[0]), currencyCode);
    }

    private By byExactClass(String classStr) {
        return By.cssSelector("[class=\"" + classStr + "\"]");
    }
}
