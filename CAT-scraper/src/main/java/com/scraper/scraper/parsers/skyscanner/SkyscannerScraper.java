package com.scraper.scraper.parsers.skyscanner;


import com.catModel.*;
import com.scraper.general.Config;
import com.scraper.scraper.parsers.FlightScraper;
import com.scraper.scraper.parsers.ScraperUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class SkyscannerScraper extends FlightScraper<SkyscannerConfig> {

    private SkyscannerResponseParser parser;

    private SkyscannerRequestGenerator requestGenerator;

    public SkyscannerScraper(SkyscannerConfig skyscannerConfig, WebDriver driver) {
        super(skyscannerConfig, driver);
        this.parser = new SkyscannerResponseParser(skyscannerConfig);
        this.requestGenerator = new SkyscannerRequestGenerator(skyscannerConfig);
    }

    @Override
    public String getRequestUrl(FlightScrapeRequest request) throws Exception {
        return requestGenerator.generateRequest(request);
    }

    @Override
    protected void beforeParsing(WebDriver driver) throws InterruptedException {
        Thread.sleep(getConfig().getResultsWait() * 1000);
        while (driver.getCurrentUrl().contains(getConfig().getCaptchaMatcher())) {
            Thread.sleep(1000);
        }
        Thread.sleep(1000);
    }

    @Override
    protected ScrapeResult parse(WebDriver driver, FlightScrapeRequest request) throws Exception {
        FlightSearchResult result = new FlightSearchResult();
        Thread.sleep(30000);
        String networkData = ((JavascriptExecutor) driver).executeScript(getJSScript()).toString();
        result.setResultLoadingTime(parser.parseResultLoadingTime(networkData));

        WebElement rootElement = driver.findElement(By.id("app-root"));
        Map<String, String> resultPageClasses = parser.parseResultPageClasses(rootElement.getAttribute("innerHTML"));

        WebElement headerElement = driver.findElement(By.id("header-root"));
        parser.parseCurrencyCode(headerElement);

        By offerItemsSelector = By.className(resultPageClasses.get(SkyscannerResponseParser.OFFER_CONTAINER));
        List<WebElement> offerItems = rootElement.findElements(offerItemsSelector);
        WebElement modalElement = driver.findElement(By.id("modal-container"));

        for (WebElement offerItem : offerItems) {
            Trip trip = parseTrip(driver, modalElement, offerItem);
            if (trip != null) {
                result.addTrip(trip);
            }
        }

        return result;
    }

    private String getJSScript() throws IOException {
        String scriptPath = Config.getInstance().getResourcesPath() + "/scraper/skyscanner/networking.js";
        return ScraperUtils.getFileContent(scriptPath);
    }

    private Trip parseTrip(WebDriver driver, WebElement modalElement, WebElement offerItem) throws Exception {
        Map<String, String> offerDetailsClasses = null;
        openOfferInformation(driver, offerItem);
        try {
            offerDetailsClasses = parser.parseOfferDetailsClasses(modalElement.getAttribute("innerHTML"));
            expandItineraryInfo(driver, modalElement, offerDetailsClasses.get(SkyscannerResponseParser.ITINERARY_EXPAND_BTN));
            offerDetailsClasses = parser.parseOfferDetailsClasses(modalElement.getAttribute("innerHTML"));
            Trip trip = parser.parseTrip(modalElement, offerDetailsClasses);
            List<PriceInfo> priceInfos = parser.parsePriceInfos(modalElement, offerDetailsClasses);
            trip.setPriceInfos(priceInfos);
            Thread.sleep(500);
            closeOfferInformation(driver, modalElement, offerDetailsClasses.get(SkyscannerResponseParser.MODAL_BACK_BTN));
            return trip;
        } catch (Exception e) {
            System.out.println("Element parsing failed for Skypicker: " + e.getMessage());
            e.printStackTrace();
        }

        closeOfferInformation(driver, modalElement, offerDetailsClasses.get(SkyscannerResponseParser.MODAL_BACK_BTN));
        return null;
    }

    private void openOfferInformation(WebDriver driver, WebElement offerItem) throws InterruptedException {
        WebElement clickableElement = offerItem.findElement(By.cssSelector("[role=\"button\"]"));
        scrollToElement(driver, clickableElement);
        Thread.sleep(1000);
        triggerClick(driver, clickableElement);
    }

    private void closeOfferInformation(WebDriver driver, WebElement modalElement, String backBtnClass) throws InterruptedException {
        WebElement backBtn = modalElement.findElement(By.cssSelector("[class=\"" + backBtnClass + "\"]"));
        scrollToElement(driver, backBtn);
        Thread.sleep(1000);
        triggerClick(driver, backBtn);
    }

    private void expandItineraryInfo(WebDriver driver, WebElement modalElement, String expandBtnClass) throws InterruptedException {
        List<WebElement> elements = modalElement.findElements(By.cssSelector("[class=\"" + expandBtnClass + "\"]"));
        for (WebElement element : elements) {
            scrollToElement(driver, element);
            Thread.sleep(1000);
            triggerClick(driver, element);
        }
    }

    private void scrollToElement(WebDriver driver, WebElement element) {
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView({block: \"center\"});", element);
    }

    private void triggerClick(WebDriver driver, WebElement element) {
        Actions clickAction = new Actions(driver);
        clickAction.click(element).perform();
    }
}
