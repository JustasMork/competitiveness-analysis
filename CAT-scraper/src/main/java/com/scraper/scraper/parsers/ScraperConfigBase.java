package com.scraper.scraper.parsers;

import com.catModel.ScraperType;
import com.google.common.annotations.VisibleForTesting;

import com.scraper.general.Config;
import com.scraper.general.Env;


public abstract class ScraperConfigBase {

    protected String propPrefix;

    private Env env;

    ScraperType scraperType;

    public ScraperConfigBase(ScraperType scraperType) {
        this.env = Config.getInstance().getEnv();
        this.scraperType = scraperType;
        this.propPrefix = "scraper." + scraperType.getPropTag() + ".";
    }

    public ScraperType getScraperType() {
        return scraperType;
    }

    protected Env getEnv() {
        return env;
    }

    public String getRequestUrl() {
        return env.getProp(propPrefix + "requestUrl");
    }

    public String getRequestPath() {
        return env.getProp(propPrefix + "requestPath");
    }

    public String getRequestSuffix() {
        return "";
    }

    @VisibleForTesting
    public void setEnv(Env env) {
        this.env = env;
    }

}
