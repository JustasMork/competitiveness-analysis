package com.scraper.scraper.parsers.kiwi;

import com.catModel.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class KiwiResponseParser {

    public static String CURR_CODE = "Code-sc";
    public static String CURR_SIGN = "Sign-fan";

    public static String OFFER_CONTAINER = "ResultCardstyled__ResultCardWrapper";
    public static String OFFER_BUTTONS_ROW = "ResultCardBadgesstyled__ResultCardBadgesRow";
    public static String SHOW_DETAILS_BUTTON = "ResultCardcommonstyled__DetailsButton";

    public static String MODAL_CONTAINER = "ResultDetailModalstyled__";
    public static String EXPAND_SEGMENT = "TripSegmentstyled__ShowMore";
    public static String MODAL_CLOSE_BTN = "ModalCloseButton"; // data-test attribute
    public static String LEG_CONTAINER = "TripPopupWrapper"; // data-test attribute
    public static String SEGMENT_CONTAINER = "ResultDetailstyled__TripDetail";
    public static String SEGMENT_TIME_ELEMENT = "TripSegmentstyled__TripTimeText";
    public static String AIRPORT_ELEMENT = "TripSegmentstyled__TripPlace";
    public static String FLIGHT_NR_CONTAINER = "itineraryFlightNumber"; // data-test attribute
    public static String SEGMENT_DETAIL_VAL = "TripSegmentDetailItemValue";

    public static String PROVIDERS_LIST = "BookingOptionsstyled__ProvidersTable";
    public static String FOOTER_PROVIDER = "ResultDetailstyled__FooterProviderWrapper";
    public static String FOOTER_PRICE = "ResultDetailstyled__FooterPriceWrapper";
    public static String LIST_PROVIDER_NAME = "ProviderInfostyled__ProviderName";
    public static String LIST_PROVIDER_PRICE = "Button__StyledButtonContentChildren";

    private KiwiConfig config;
    private String currencyCode;
    private String currencySign;

    public KiwiResponseParser(KiwiConfig config) {
        this.config = config;
    }

    public void parseCurrencyParams(WebElement rootElement) {
        String rootHtml = getElementHtml(rootElement);
        String currCodeClass = getFirstMatchingClass(rootHtml, CURR_CODE);
        String currSignClass = getFirstMatchingClass(rootHtml, CURR_SIGN);

        currencyCode = rootElement.findElement(byExactClass(currCodeClass)).getAttribute("innerText");
        currencySign = rootElement.findElement(byExactClass(currSignClass)).getAttribute("innerText");
    }


    public String getShowDetailsBtnClass(WebElement rootElement) {
        String offerClasss = getFirstMatchingClass(getElementHtml(rootElement), OFFER_CONTAINER);

        WebElement offer = rootElement.findElement(byExactClass(offerClasss));
        String buttonsRowClass = getFirstMatchingClass(getElementHtml(offer), OFFER_BUTTONS_ROW);

        WebElement buttonsRow = offer.findElement(byExactClass(buttonsRowClass));
        return getFirstMatchingClass(getElementHtml(buttonsRow), SHOW_DETAILS_BUTTON);
    }

    public String getModalContainerClass(WebElement rootElement) {
        return getFirstMatchingClass(getElementHtml(rootElement), MODAL_CONTAINER);
    }

    public String getItineraryExpandClass(WebElement modalElement) {
        // TODO: fix this.
        return getFirstMatchingClass(getElementHtml(modalElement), EXPAND_SEGMENT);
    }

    public List<Leg> parseItineraryData(WebElement modalElement) throws Exception {
        List<Leg> legs = new ArrayList<>();

        By legElementSelector = By.cssSelector("[data-test=\"" + LEG_CONTAINER + "\"]");
        for (WebElement legElement : modalElement.findElements(legElementSelector)) {
            legs.add(parseLegData(legElement));
        }

        return legs;
    }

    private Leg parseLegData(WebElement legElement) throws Exception {
        Leg leg = new Leg();

        for (WebElement divElement : legElement.findElements(By.cssSelector("section > div"))) {
            String elementClass = divElement.getAttribute("class");
            if (elementClass.contains(SEGMENT_CONTAINER)) {
                leg.addSegment(parseSegment(divElement));
            }
        }
        return leg;
    }

    private Segment parseSegment(WebElement segmentElement) throws Exception {
        Segment segment = new Segment();

        String segmentHtml = getElementHtml(segmentElement);
        String segmentTimeClass = getFirstMatchingClass(segmentHtml, SEGMENT_TIME_ELEMENT);
        List<WebElement> timeElements = segmentElement.findElements(byExactClass(segmentTimeClass));
        if (timeElements.size() != 2) {
            throw new Exception("Bad times information. Expected size: 2, Actual: " + timeElements.size());
        }
        segment.setDeparture(parseDate(timeElements.get(0)));
        segment.setArrival(parseDate(timeElements.get(1)));

        String airportClass = getFirstMatchingClass(segmentHtml, AIRPORT_ELEMENT);
        int beginIndex = airportClass.indexOf(AIRPORT_ELEMENT);
        airportClass = airportClass.substring(beginIndex, airportClass.indexOf(" ", beginIndex));
        List<WebElement> airportElements = segmentElement.findElements(By.className(airportClass));
        if (airportElements.size() != 2) {
            throw new Exception("Bad airport information. Expected size: 2, Actual: " + airportElements.size());
        }
        segment.setOrigin(parseAirport(airportElements.get(0)));
        segment.setDestination(parseAirport(airportElements.get(1)));

        WebElement flightNrEl = segmentElement.findElement(By.cssSelector("[data-test=\"" + FLIGHT_NR_CONTAINER + "\"]"));
        segment.setFlightNr(parseFlightNr(flightNrEl));

        return segment;
    }

    private Date parseDate(WebElement timeElement) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSS");
        String dateTimeString = timeElement.findElement(By.tagName("time")).getAttribute("datetime");
        return format.parse(dateTimeString.substring(0, dateTimeString.indexOf("+")));
    }

    private Airport parseAirport(WebElement airportElement) {
        Airport airport = new Airport();
        airport.setIata(airportElement.findElement(By.tagName("p")).getAttribute("innerText"));
        return airport;
    }

    private String parseFlightNr(WebElement flightNrContainer) {
        String flightNrClass = getFirstMatchingClass(getElementHtml(flightNrContainer), SEGMENT_DETAIL_VAL);
        String flightNr = flightNrContainer.findElement(byExactClass(flightNrClass)).getAttribute("innerText");

        return flightNr.replace(" ", "");
    }

    public List<PriceInfo> parsePriceInfos(WebElement modalElement) throws Exception {
        String modalHtml = getElementHtml(modalElement);

        String providersListClass = getFirstMatchingClass(modalHtml, PROVIDERS_LIST);
        String footerProvider = getFirstMatchingClass(modalHtml, FOOTER_PROVIDER);
        String footerPrice = getFirstMatchingClass(modalHtml, FOOTER_PRICE);

        if (providersListClass != null) {
            return parseProvidersList(modalElement.findElement(byExactClass(providersListClass)));
        } else if (footerProvider == null && footerPrice != null) {
            return List.of(parseFooterPrice(modalElement.findElement(byExactClass(footerPrice))));
        }

        throw new Exception("Unable to find prices in details modal");
    }

    private List<PriceInfo> parseProvidersList(WebElement providersElement) {
        List<PriceInfo> priceInfos = new ArrayList<>();

        for (WebElement providerRow : providersElement.findElements(By.cssSelector("tbody tr"))) {
            String providerRowHtml = getElementHtml(providerRow);
            String providerNameClass = getFirstMatchingClass(providerRowHtml, LIST_PROVIDER_NAME);
            String providerPriceClass = getFirstMatchingClass(providerRowHtml, LIST_PROVIDER_PRICE);
            if (providerNameClass != null && providerPriceClass != null) {

                String providerName = providerRow.findElement(byExactClass(providerNameClass))
                        .getAttribute("innerText");

                String priceString = providerRow.findElement(byExactClass(providerPriceClass))
                        .findElement(By.tagName("span"))
                        .getAttribute("innerText");

                Price price = new Price();
                price.setCurrency(currencyCode);
                price.setAmount(new BigDecimal(cleanUpPrice(priceString)));

                PriceInfo priceInfo = new PriceInfo();
                priceInfo.setPrice(price);
                priceInfo.setProvider(providerName);
                priceInfos.add(priceInfo);

            } else {
                System.err.println("Provider price row parsing failed.");
            }
        }

        return priceInfos;
    }

    private String cleanUpPrice(String priceString) {
        return priceString.replace(currencySign, "").replace(",", "").trim();
    }


    private PriceInfo parseFooterPrice(WebElement footerPriceEL) {
        String priceString = footerPriceEL.findElement(By.tagName("span")).getAttribute("innerText");

        Price price = new Price();
        price.setAmount(new BigDecimal(cleanUpPrice(priceString)));
        price.setCurrency(currencyCode);

        PriceInfo priceInfo = new PriceInfo();
        priceInfo.setProvider("Kiwi.com");
        priceInfo.setPrice(price);

        return priceInfo;
    }


    private String getFirstMatchingClass(String htmlContent, String matcherStr) {
        String regex = "class=\"(.*?)\"";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(htmlContent);
        while (matcher.find()) {
            String className = matcher.group(1);
            if (className.contains(matcherStr)) {
                return className;
            }
        }

        return null;
    }

    private String getElementHtml(WebElement element) {
        return element.getAttribute("innerHTML");
    }

    public static By byExactClass(String classStr) {
        return By.cssSelector("[class=\"" + classStr + "\"]");
    }

    public Integer parseResultLoadingTime(String networkJsonString) {
        try {
            JSONArray networkLog = new JSONArray(networkJsonString);
            List<JSONObject> resultPollingEntries = getResultPollingEntries(networkLog);
            Integer loadingDuration = 0;

            for (JSONObject entry : resultPollingEntries) {
                loadingDuration += getApiCallDuration(entry);
            }

            return loadingDuration;
        } catch (Exception e) {
            System.out.println("Failed result loading time parsing for KIWI.");
            e.printStackTrace();
        }
        return null;
    }


    private List<JSONObject> getResultPollingEntries(JSONArray logArray) throws JSONException {
        List<JSONObject> filteredEntries = new LinkedList<>();

        for (int i = 0; i < logArray.length(); i++) {
            JSONObject entry = logArray.getJSONObject(i);
            if (isResultPolling(entry)) {
                filteredEntries.add(entry);
            }
        }

        return filteredEntries;
    }

    private boolean isResultPolling(JSONObject entry) {
        try {
            return entry.getString("initiatorType").equals("fetch")
                    && entry.getString("name").contains(config.getResultsPollingMatcher());
        } catch (Exception e) {
            return false;
        }
    }

    private Integer getApiCallDuration(JSONObject entry) throws JSONException {
        return (int) entry.getLong("duration");
    }


}
