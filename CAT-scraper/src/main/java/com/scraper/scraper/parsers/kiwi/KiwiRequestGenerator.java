package com.scraper.scraper.parsers.kiwi;

import com.catModel.Airport;
import com.catModel.FlightScrapeRequest;
import com.catModel.ODpair;
import com.catModel.PTC;
import org.apache.http.client.utils.URIBuilder;

import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

public class KiwiRequestGenerator {

    private KiwiConfig config;

    private SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

    public KiwiRequestGenerator(KiwiConfig config) {
        this.config = config;
    }

    public String generate(FlightScrapeRequest request) throws URISyntaxException {
        URIBuilder builder = new URIBuilder(config.getRequestUrl())
                .setPath(generatePath(request));

        return addQueryParams(builder, request).toString();

    }

    private String generatePath(FlightScrapeRequest request) {
        StringBuilder builder = new StringBuilder();
        builder.append("/")
                .append(request.getCountryCode().toLowerCase())
                .append(config.getRequestPath());

        if (request.isOneWay()) {
            buildOWItineraryPath(request, builder);
        } else {
            buildItineraryPath(request, builder);
        }

        return builder.toString();
    }

    private void buildItineraryPath(FlightScrapeRequest request, StringBuilder builder) {
        for (ODpair oDpair : request.getItinerary()) {
            builder.append("/")
                    .append(getAirportString(oDpair.getOrigin()));
        }

        for (ODpair oDpair : request.getItinerary()) {
            builder.append("/").append(getDateString(oDpair.getDepartureDate()));
        }
    }

    private void buildOWItineraryPath(FlightScrapeRequest request, StringBuilder builder) {
        builder.append("/")
                .append(getAirportString(request.getFirstOD().getOrigin()))
                .append("/")
                .append(getAirportString(request.getFirstOD().getDestination()))
                .append("/")
                .append(getDateString(request.getFirstOD().getDepartureDate()))
                .append("/no-return");
    }

    private String getAirportString(Airport airport) {
        String airportName = cleanupAirportName(airport).toLowerCase();
        String city = airport.getCity().replace(" ", "-").toLowerCase();
        String airportString = airportName;

        if (!city.equals(airportName)) {
            airportString += "-" + city;
        }

        airportString += "-" + airport.getCountry().replace(" ", "-").toLowerCase();
        return airportString;
    }

    private String cleanupAirportName(Airport airport) {
        return airport.getName()
                .replace("Airport", "").replace("airport", "")
                .replace("International", "").replace("international", "")
                .trim()
                .replace(" ", "-");
    }

    private String getDateString(Date date) {
        return format.format(date);
    }

    private URIBuilder addQueryParams(URIBuilder builder, FlightScrapeRequest request) {
        if (request.getTravellers().size() == 1) {
            return builder;
        }

        for (Map.Entry<PTC, Integer> entry : request.getTravellers().entrySet()) {
            builder.addParameter(KiwiConfig.PTC_TO_PARAM_NAME.get(entry.getKey()), entry.getValue().toString());
        }

        return builder;
    }
}
