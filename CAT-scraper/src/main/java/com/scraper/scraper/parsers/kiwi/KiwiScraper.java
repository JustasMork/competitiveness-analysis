package com.scraper.scraper.parsers.kiwi;

import com.catModel.FlightScrapeRequest;
import com.catModel.FlightSearchResult;
import com.catModel.ScrapeResult;
import com.catModel.Trip;
import com.scraper.general.Config;
import com.scraper.scraper.parsers.FlightScraper;
import com.scraper.scraper.parsers.ScraperUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.io.IOException;
import java.util.Objects;

import static com.scraper.scraper.parsers.kiwi.KiwiResponseParser.MODAL_CLOSE_BTN;
import static com.scraper.scraper.parsers.kiwi.KiwiResponseParser.byExactClass;

public class KiwiScraper extends FlightScraper<KiwiConfig> {

    private KiwiResponseParser parser;

    public KiwiScraper(KiwiConfig scraperConfigBase, WebDriver driver) {
        super(scraperConfigBase, driver);
        parser = new KiwiResponseParser(scraperConfigBase);
    }

    @Override
    protected String getRequestUrl(FlightScrapeRequest request) throws Exception {
        return (new KiwiRequestGenerator(getConfig())).generate(request);
    }

    @Override
    protected ScrapeResult parse(WebDriver driver, FlightScrapeRequest request) throws Exception {
        FlightSearchResult result = new FlightSearchResult();
        result.setScraperType(getConfig().getScraperType());

        Thread.sleep(30000);
        WebElement rootElement = driver.findElement(By.tagName("body"));

        parser.parseCurrencyParams(rootElement);
        String detailsBtnClass = parser.getShowDetailsBtnClass(rootElement);
        int counter = 0;
        for (WebElement detailsEl : rootElement.findElements(byExactClass(detailsBtnClass))) {
            if (counter > getConfig().resultPerSearchLimit()) {
                String networkData = ((JavascriptExecutor) driver).executeScript(getJSScript()).toString();
                result.setResultLoadingTime(parser.parseResultLoadingTime(networkData));
                return result;
            }
            WebElement detailsBtn = detailsEl.findElement(By.tagName("div"));
            scrollToElement(driver, detailsBtn);
            Thread.sleep(1000);
            triggerClick(driver, detailsBtn);
            Thread.sleep(1500);

            Trip trip = parseOfferDetails(driver, rootElement);
            if (trip != null) {
                result.addTrip(trip);
            }

            Thread.sleep(200);
            closeModal(driver, rootElement);
            Thread.sleep(1500);
            counter++;
        }

        String networkData = ((JavascriptExecutor) driver).executeScript(getJSScript()).toString();
        result.setResultLoadingTime(parser.parseResultLoadingTime(networkData));

        return result;
    }

    private Trip parseOfferDetails(WebDriver driver, WebElement rootElement) {
        try {
            Trip trip = new Trip();
            String modalClassName = parser.getModalContainerClass(rootElement);
            WebElement modalElement = rootElement.findElement(byExactClass(modalClassName));

            String expandClass = parser.getItineraryExpandClass(modalElement);
            modalElement.findElements(byExactClass(expandClass)).stream()
                    .filter(Objects::nonNull)
                    .forEach(item -> {
                        try {
                            scrollToElement(driver, item);
                            Thread.sleep(1000);
                            triggerClick(driver, item);
                            Thread.sleep(200);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    });

            trip.setLegs(parser.parseItineraryData(modalElement));
            trip.setPriceInfos(parser.parsePriceInfos(modalElement));

            return trip;
        } catch (Exception e) {
            System.err.println("Element parsing failed for: " + e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    private void closeModal(WebDriver driver, WebElement rootElement) throws InterruptedException {
        By modalCloseBtnSelector = By.cssSelector("[data-test=\"" + MODAL_CLOSE_BTN + "\"]");
        WebElement modalCloseBtn = rootElement.findElement(modalCloseBtnSelector);
        triggerClick(driver, modalCloseBtn);
    }

    private void scrollToElement(WebDriver driver, WebElement element) {
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView({block: \"center\"});", element);
    }

    private void triggerClick(WebDriver driver, WebElement element) throws InterruptedException {
        Actions clickAction = new Actions(driver);
        clickAction.click(element).perform();
        Thread.sleep(800);
    }

    private String getJSScript() throws IOException {
        String scriptPath = Config.getInstance().getResourcesPath() + "/scraper/kiwi/networking.js";
        return ScraperUtils.getFileContent(scriptPath);
    }
}
