package com.scraper.scraper.parsers.kiwi;

import com.catModel.PTC;
import com.catModel.ScraperType;
import com.google.common.collect.ImmutableMap;
import com.scraper.scraper.parsers.ScraperConfigBase;

public class KiwiConfig extends ScraperConfigBase {

    public static ImmutableMap<PTC, String> PTC_TO_PARAM_NAME = ImmutableMap.<PTC, String>builder()
            .put(PTC.ADT, "adults")
            .put(PTC.CHD, "children")
            .put(PTC.INF, "infants")
            .build();

    public KiwiConfig(ScraperType scraperType) {
        super(scraperType);
    }

    public String getResultsPollingMatcher() {
        return getEnv().getProp(propPrefix + "resultPollingMatcher");
    }

    public int resultPerSearchLimit() {
        return getEnv().getInt(propPrefix + "resultsLimit", 10);
    }

}
