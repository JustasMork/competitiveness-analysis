package com.scraper.scraper.parsers.skyscanner;


import com.catModel.FlightScrapeRequest;
import com.catModel.ODpair;
import com.catModel.PTC;
import org.apache.http.client.utils.URIBuilder;

import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Map;

public class SkyscannerRequestGenerator {

    private SkyscannerConfig config;

    public SkyscannerRequestGenerator(SkyscannerConfig config) {
        this.config = config;
    }

    public String generateRequest(FlightScrapeRequest request) throws URISyntaxException {
        URIBuilder builder = new URIBuilder(config.getRequestUrl());
        builder.setPath(generatePath(request))
                .setPath(generatePath(request));

        return addQueryParams(builder, request).toString();
    }

    private String generatePath(FlightScrapeRequest request) {
        SimpleDateFormat format = new SimpleDateFormat("YYMMdd");
        StringBuilder builder = new StringBuilder();
        builder.append(config.getRequestPath());

        if (request.isOneWay() || request.isRoundTrip()) {
            builder.append(request.getFirstOD().getOrigin().getIata().toLowerCase())
                    .append("/")
                    .append(request.getFirstOD().getDestination().getIata().toLowerCase())
                    .append("/");
        }

        for (ODpair odPair : request.getItinerary()) {
            builder.append(format.format(odPair.getDepartureDate()))
                .append("/");
        }

        return builder.toString();
    }

    private URIBuilder addQueryParams(URIBuilder builder, FlightScrapeRequest request) {

        addTravellersToQueryParams(builder, request, request.isOneWay() ? "v2" : "");
        if (request.isRoundTrip()) {
            addTravellersToQueryParams(builder, request, "v2");
        }

        builder.addParameter("cabinclass", SkyscannerConfig.CABIN_TO_PARAM_NAME.get(request.getCabinClass()));
        return builder;
    }

    private URIBuilder addTravellersToQueryParams(URIBuilder builder, FlightScrapeRequest request) {
        return this.addTravellersToQueryParams(builder, request, "");
    }

    private URIBuilder addTravellersToQueryParams(URIBuilder builder, FlightScrapeRequest request, String suffix) {
        for (Map.Entry<PTC, Integer> travelerType : request.getTravellers().entrySet()) {
            String paramName = SkyscannerConfig.PTC_TO_PARAM_NAME.get(travelerType.getKey()) + suffix;
            builder.addParameter(paramName, travelerType.getValue().toString());
        }

        return builder;
    }

}
