package com.scraper.scraper.parsers;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ScraperUtils {

    public static String getFileContent(String path) throws IOException {
        return Files.readString(Paths.get(path));
    }
}
