package com.scraper.scraper.parsers.skyscanner;

import com.catModel.CabinClass;
import com.catModel.PTC;
import com.catModel.ScraperType;
import com.google.common.collect.ImmutableMap;
import com.scraper.scraper.parsers.ScraperConfigBase;

import java.util.Map;


public class SkyscannerConfig extends ScraperConfigBase {

    public static final Map<PTC, String> PTC_TO_PARAM_NAME = ImmutableMap.<PTC, String>builder()
            .put(PTC.ADT, "adults")
            .put(PTC.CHD, "children")
            .put(PTC.INF, "infants")
            .build();

    public static final Map<CabinClass, String> CABIN_TO_PARAM_NAME = ImmutableMap.<CabinClass, String>builder()
            .put(CabinClass.Economy, "economy")
            .put(CabinClass.PremEconomy, "premiumeconomy")
            .put(CabinClass.Business, "business")
            .put(CabinClass.First, "first")
            .build();

    public SkyscannerConfig(ScraperType scraperType) {
        super(scraperType);
    }

    public Long getResultsWait() {
        return getEnv().getLong(propPrefix + "resultsWait", 30L);
    }

    public String getResultsPollingMatcher() {
        return getEnv().getProp(propPrefix + "resultPollingMatcher");
    }

    public String getCaptchaMatcher() {
        return getEnv().getProp(propPrefix + "captchaMatcher");
    }

    @Override
    public String getRequestSuffix() {
        return getEnv().getProp(propPrefix + "requestSuffix");
    }
}
