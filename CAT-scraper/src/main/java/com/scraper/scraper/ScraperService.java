package com.scraper.scraper;


import com.catModel.ScrapeRequests;
import com.catModel.ScrapeResults;

public interface ScraperService {

    void execute(ScrapeRequests scrapeRequest);

    ScrapeResults getResults();

}
