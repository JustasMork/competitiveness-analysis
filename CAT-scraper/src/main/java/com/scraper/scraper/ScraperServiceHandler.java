package com.scraper.scraper;

import com.catModel.ScrapeRequests;
import com.catModel.ScrapeResults;
import com.catModel.ScraperType;
import com.scraper.scraper.parsers.Scraper;
import com.scraper.scraper.parsers.ScraperConfigBase;
import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.opera.OperaOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.safari.SafariOptions;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

@Service
public class ScraperServiceHandler implements ScraperService {

    private final ScraperFactory factory;

    private ScrapeResults results;

    public ScraperServiceHandler(ScraperFactory factory) {
        this.factory = factory;
    }


    @Override
    public void execute(ScrapeRequests scrapeRequest) {
        List<Scraper<ScraperConfigBase>> scrapers = getScrapers(scrapeRequest);
        ScrapeResults results = null;
        try {
            results = parseResults(scrapers, scrapeRequest);
            results.setId(scrapeRequest.getId());
            this.results = results;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public ScrapeResults getResults() {
        return results;
    }

    private ScrapeResults parseResults(List<Scraper<ScraperConfigBase>> scrapers, ScrapeRequests scrapeRequest) throws InterruptedException {
        ScrapeResults results = new ScrapeResults();
        results.setId(scrapeRequest.getId());

        ExecutorService executorService = Executors.newFixedThreadPool(scrapers.size());

        for (Scraper<ScraperConfigBase> scraper : scrapers) {
            executorService.execute(() -> scrapeRequest.getRequests()
                    .forEach(request -> results.addResult(scraper.scrape(request))));
        }

        executorService.shutdown();
        executorService.awaitTermination(20, TimeUnit.MINUTES);
        quitAllDrivers(scrapers);

        return results;
    }

    private void quitAllDrivers(List<Scraper<ScraperConfigBase>> scrapers) {
        for (Scraper<ScraperConfigBase> scraper : scrapers) {
            scraper.quitDriver();
        }
    }

    private List<Scraper<ScraperConfigBase>> getScrapers(ScrapeRequests requests) {
        List<Scraper<ScraperConfigBase>> scraperList = new ArrayList<>();
        for (ScraperType scraperType : requests.getScraperTypes()) {
            WebDriver driver = getDriver(requests.getBrowser());
            Scraper<ScraperConfigBase> scraper = factory.createScraper(scraperType.getPropTag(), driver);
            scraperList.add(scraper);
        }

        return scraperList;
    }

    private WebDriver getDriver(String browser) {
        MutableCapabilities capabilities = getDriverCapabilities(browser);
        switch (browser) {
            case "firefox":
                return new FirefoxDriver((FirefoxOptions) capabilities);
            case "chrome":
                return new ChromeDriver((ChromeOptions) capabilities);
            case "IE":
                return new InternetExplorerDriver((InternetExplorerOptions) capabilities);
            case "opera":
                return new OperaDriver((OperaOptions) capabilities);
            case "safari":
                return new SafariDriver((SafariOptions) capabilities);
        }
        return new FirefoxDriver((FirefoxOptions) capabilities);
    }

    private MutableCapabilities getDriverCapabilities(String browser) {
        LoggingPreferences logPrefs = new LoggingPreferences();
        logPrefs.enable(LogType.PERFORMANCE, Level.ALL);

        MutableCapabilities capabilities = getBrowserCapabilities(browser);
        capabilities.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
        return capabilities;
    }

    private MutableCapabilities getBrowserCapabilities(String browser) {
        switch (browser) {
            case "firefox":
                return new FirefoxOptions();
            case "chrome":
                return new ChromeOptions();
            case "IE":
                return new InternetExplorerOptions();
            case "opera":
                return new OperaOptions();
            case "safari":
                return new SafariOptions();
        }

        return new FirefoxOptions();
    }
}
