package com.scraper.scraper;


import com.catModel.ScraperType;
import com.scraper.general.Config;
import com.scraper.scraper.parsers.Scraper;
import com.scraper.scraper.parsers.ScraperConfigBase;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.lang.reflect.Constructor;

@Service
public class ScraperFactoryManager implements ScraperFactory {

    private final static Logger log = LoggerFactory.getLogger(ScraperFactoryManager.class);

    private final static String PROP_PREFIX = "scraper.factory.";

    @Override
    public Scraper<ScraperConfigBase> createScraper(String propTag, WebDriver driver) {
        String classPath = getScraperClassPath(propTag);
        if (classPath != null) {
            try {
                Constructor<Scraper<ScraperConfigBase>> constructor = getConstructor(classPath);
                ScraperConfigBase config = initializeConfig(constructor, propTag);

                return constructor.newInstance(config, driver);
            } catch (Exception e) {
                handleUnexpected(e.getMessage());
            }

        } else {
            String msg = "Scraper classpath not found :" + propTag;
            handleUnexpected(msg);
        }
        return null;
    }

    private String getScraperClassPath(String propTag) {
        return Config.getInstance().getEnv().getProp(PROP_PREFIX + propTag);
    }

    private Constructor<Scraper<ScraperConfigBase>> getConstructor(String classpath) throws ClassNotFoundException {
        Class clazz = Class.forName(classpath);
        Constructor[] constructors = clazz.getConstructors();
        if (constructors.length != 1) {
            String msg = "All scrapers must have only one constructor: " + clazz.getSimpleName();
            handleUnexpected(msg);
        }

        return (Constructor<Scraper<ScraperConfigBase>>) constructors[0];
    }

    private ScraperConfigBase initializeConfig(Constructor<Scraper<ScraperConfigBase>> constructor, String propTag) throws Exception {
        Class<?>[] parameters = constructor.getParameterTypes();

        if (parameters.length != 2) {
            String msg = "Unknown amount of constructor parameters when initializing scraper config.";
            handleUnexpected(msg);

            return null;
        }

        return (ScraperConfigBase) parameters[0].getDeclaredConstructor(ScraperType.class).newInstance(ScraperType.fromPropTag(propTag));
    }

    private void handleUnexpected(String msg) {
        log.error(msg);
    }

}
