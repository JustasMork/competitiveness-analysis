package com.scraper.scraper;

import com.scraper.scraper.parsers.Scraper;
import com.scraper.scraper.parsers.ScraperConfigBase;
import org.openqa.selenium.WebDriver;

public interface ScraperFactory {

    Scraper<ScraperConfigBase> createScraper(String propTag, WebDriver driver);

}
