package com.scraper;

import com.scraper.controller.ScraperController;
import com.scraper.scraper.ScraperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Scanner;

@SpringBootApplication
public class ScraperApplication implements CommandLineRunner {

	@Autowired
	ScraperService scraper;

	public static void main(String[] args) {
		setSystemProperties();
		SpringApplication.run(ScraperApplication.class, args);
	}

	@Override
	public void run(String... args) throws IOException {
		System.out.println("Scraper application started...");
		Scanner sc = new Scanner(System.in);
		sc.nextLine();
		try {
			(new ScraperController(scraper)).run();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void setSystemProperties() {
		String appPath = System.getProperty("user.dir");
		String execPath = appPath + "\\CAT-scraper\\exec\\";
		System.setProperty("webdriver.gecko.driver", execPath + "geckodriver.exe");
		System.setProperty("webdriver.chrome.driver", execPath + "chromedriver.exe");
	}
}
