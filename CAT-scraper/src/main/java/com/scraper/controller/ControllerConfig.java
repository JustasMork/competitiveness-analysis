package com.scraper.controller;

import com.scraper.general.Config;
import com.scraper.general.Env;

public class ControllerConfig {

    private Env env;

    private static String prefix = "controller.";

    public ControllerConfig() {
        env = Config.getInstance().getEnv();
    }

    public String getScrapingTasksUrl() {
        return env.getProp(prefix + "scrapingTasks.requestUrl");
    }

    public String getScrapingResultsUrl() {
        return env.getProp(prefix + "scrapingResults.requestUrl");
    }

}
