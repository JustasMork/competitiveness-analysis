package com.scraper.controller;

import com.catModel.ScrapeRequests;
import com.catModel.ScrapeResults;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.scraper.scraper.ScraperService;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.io.IOException;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class ScraperController {

    private ScraperService scraper;

    private ControllerConfig config;

    public ScraperController(ScraperService service) {
        this.scraper = service;
        this.config = new ControllerConfig();
    }

    public void run() throws InterruptedException, IOException, URISyntaxException {
        ScrapeRequests scrapingTask = getScrapingTask();
        if (scrapingTask != null) {
            startScraping(scrapingTask);
            ScrapeResults results = scraper.getResults();
            System.out.println("Received results.");
            postResults(results);
        }
    }

    private ScrapeRequests getScrapingTask() throws URISyntaxException, IOException, InterruptedException {
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder(new URI(config.getScrapingTasksUrl())).build();
        System.out.println("Retrieving scraping tasks...");
        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
        if (response.statusCode() == 200) {
            System.out.println("Successfully retrieved scraping tasks.");
            return parseJson(response.body());
        }
        System.out.println("Failed to retrieve scraping tasks.");
        return null;
    }

    private ScrapeRequests parseJson(String json) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return mapper.readValue(json, ScrapeRequests.class);
    }

    private void startScraping(ScrapeRequests requests) {
        System.out.println("Scraping started.");
        scraper.execute(requests);
        System.out.println("Scraping completed.");
    }

    private void postResults(ScrapeResults results) throws IOException {
        if (results == null) {
            System.out.println("Null scraping result received.");
            return;
        }

        if (results.getScrapeResults() == null || results.getScrapeResults().size() == 0) {
            System.out.println("Result does not contain any elements.");
        }

        String requestBody = getResultsJson(results);

        System.out.println("Posting scraping results.");
        sendRequest(requestBody);
    }

    private String getResultsJson(ScrapeResults results) {
        StringWriter writer = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        try {
            mapper.writeValue(writer, results);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return writer.toString();
    }

    private void sendRequest(String json) throws IOException {
        HttpPost post = new HttpPost(config.getScrapingResultsUrl());
        post.setEntity(new StringEntity(json, ContentType.APPLICATION_JSON));
        CloseableHttpClient client = HttpClients.createDefault();
        org.apache.http.HttpResponse response = client.execute(post);

        if (response.getStatusLine().getStatusCode() == 200) {
            System.out.println("Scraping results successfully sent.");
        } else {
            System.out.println("Failed to send scraping results. " + response.getStatusLine().getReasonPhrase());
        }
    }
}
