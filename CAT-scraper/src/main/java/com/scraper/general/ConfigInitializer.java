package com.scraper.general;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
class ConfigInitializer {

    private final Env env;

    public ConfigInitializer(Env env) {
        this.env = env;
    }

    @PostConstruct
    public void initStatiConfig() {
        Config.getInstance().setEnv(env);
    }
}
