package com.scraper.general;

import java.nio.file.Paths;

public class Config {

    private Env env;

    private static Config instance;

    public static Config getInstance() {
        if (instance == null) {
            instance = new Config();
        }
        return instance;
    }

    public Env getEnv() {
        return env;
    }

    void setEnv(Env env) {
        this.env = env;
    }

    public String getResourcesPath() {
        return Paths.get("CAT-scraper","src", "main", "resources").toFile().getAbsolutePath();
    }
}
