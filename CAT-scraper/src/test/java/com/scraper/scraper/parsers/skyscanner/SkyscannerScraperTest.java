package com.scraper.scraper.parsers.skyscanner;


import com.catModel.FlightScrapeRequest;
import com.catModel.ScrapeResult;
import com.catModel.ScraperType;
import com.scraper.TestBase;
import com.scraper.TestUtils;
import com.scraper.scraper.ScraperFactory;
import com.scraper.scraper.parsers.Scraper;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.ParseException;
import java.util.logging.Level;

@Disabled("Should be executed manually")
class SkyscannerScraperTest extends TestBase {

    @Autowired
    ScraperFactory factory;

    @BeforeAll
    static void setUp() {
        String appPath = System.getProperty("user.dir");
        String execPath = appPath + "\\exec\\";
        System.setProperty("webdriver.gecko.driver", execPath + "geckodriver.exe");
        System.setProperty("webdriver.chrome.driver", execPath + "chromedriver.exe");
    }

    @Test
    void testSkycannerScraper() throws ParseException {
        LoggingPreferences logPrefs = new LoggingPreferences();
        logPrefs.enable(LogType.PERFORMANCE, Level.ALL);
        MutableCapabilities capabilities = new FirefoxOptions();
        capabilities.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
        FirefoxDriver chromeDriver = new FirefoxDriver((FirefoxOptions) capabilities);

        Scraper scraper = factory.createScraper(ScraperType.SKYSCANNER.getPropTag(), chromeDriver);
        FlightScrapeRequest request = TestUtils.generateOWRequest();
        ScrapeResult result = scraper.scrape(request);
    }
}