package com.scraper.scraper.parsers.skyscanner;


import com.catModel.FlightScrapeRequest;
import com.catModel.ScraperType;
import com.scraper.TestBase;
import com.scraper.TestUtils;
import com.scraper.general.Env;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.net.URISyntaxException;
import java.text.ParseException;

import static org.junit.jupiter.api.Assertions.assertTrue;

class SkyscannerRequestGeneratorTest extends TestBase {

    @Autowired
    private Env env;

    @Test
    void testGenerateOWRequest() throws ParseException, URISyntaxException {
        SkyscannerConfig config = new SkyscannerConfig(ScraperType.SKYSCANNER);
        config.setEnv(env);

        SkyscannerRequestGenerator generator = new SkyscannerRequestGenerator(config);

        FlightScrapeRequest request = TestUtils.generateOWRequest();
        String requestUrl = generator.generateRequest(request);

        assertTrue(requestUrl.contains("https://www.skyscanner.net/transport/flights"));
        assertTrue(requestUrl.contains("VNO/SEN"));
        assertTrue(requestUrl.contains("201229"));
        assertTrue(requestUrl.contains("adults=2"));
        assertTrue(requestUrl.contains("children=1"));
        assertTrue(requestUrl.contains("cabinclass=economy"));
    }


}