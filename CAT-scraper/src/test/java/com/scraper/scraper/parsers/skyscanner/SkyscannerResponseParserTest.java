package com.scraper.scraper.parsers.skyscanner;

import com.scraper.TestBase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class SkyscannerResponseParserTest extends TestBase {

    private String resourcesPath;

    private SkyscannerConfig config;

    @BeforeEach
    void setUp() {
        resourcesPath = getResourcePath() + "/scraper/skyscanner/";
        config = mock(SkyscannerConfig.class);
    }

    @Test
    void testResultLoadingTimeParsing() throws IOException {
        when(config.getResultsPollingMatcher()).thenReturn("conductor/v1/fps3/search");

        String networkLog = getFileContent(resourcesPath + "NetworkLog.json");
        SkyscannerResponseParser parser = new SkyscannerResponseParser(config);

        Integer responseLoadingTime = parser.parseResultLoadingTime(networkLog);
        assertNotNull(responseLoadingTime);
        assertEquals(324, responseLoadingTime);
    }

    @Test
    void testResultPageClassesParsing() throws IOException {
        String rootHtmlContent = getFileContent(resourcesPath + "RootElementContent.html");
        SkyscannerResponseParser parser = new SkyscannerResponseParser(config);

        Map<String, String> classesMap = parser.parseResultPageClasses(rootHtmlContent);

        assertEquals(2, classesMap.size());
        assertNotNull(classesMap.get(SkyscannerResponseParser.OFFER_CONTAINER));
        assertEquals("FlightsTicket_container__3yvwg", classesMap.get(SkyscannerResponseParser.OFFER_CONTAINER));
        assertNotNull(classesMap.get(SkyscannerResponseParser.MAIN_PRICE_CONTAINER));
        assertEquals("Price_mainPriceContainer__1dqsw", classesMap.get(SkyscannerResponseParser.MAIN_PRICE_CONTAINER));
    }

    @Test
    void testOfferDetailsClassesParsing() throws IOException {
        String offerHtmlContent = getFileContent(resourcesPath + "OfferModalContent.html");
        SkyscannerResponseParser parser = new SkyscannerResponseParser(config);

        Map<String, String> classesMap = parser.parseOfferDetailsClasses(offerHtmlContent);

        assertEquals(12, classesMap.size());
        assertEquals(classesMap.get(SkyscannerResponseParser.MODAL_BACK_BTN), "BpkLink_bpk-link__2Jqrw BpkNavigationBarButtonLink_bpk-navigation-bar-button-link__14cZB BpkNavigationBar_bpk-navigation-bar__leading-item__2WJLe");
        assertEquals(classesMap.get(SkyscannerResponseParser.ITINERARY_EXPAND_BTN), "LegSummary_container__25maG LegSummary_opened__3vzUe");
        assertEquals(classesMap.get(SkyscannerResponseParser.LEG_INFO_CONTAINER), "Itinerary_leg__r5ifn");
        assertEquals(classesMap.get(SkyscannerResponseParser.LEG_DATE_CONTAINER), "BpkText_bpk-text__2NHsO BpkText_bpk-text--base__2vfTl LegHeader_legDate__wjV4s");
        assertEquals(classesMap.get(SkyscannerResponseParser.LEG_SEGMENT_CONTAINER), "LegSegmentDetails_container__6E_85");
        assertEquals(classesMap.get(SkyscannerResponseParser.FLIGHT_NR_CONTAINER), "BpkText_bpk-text__2NHsO BpkText_bpk-text--sm__345aT OperatedBy_operatedBy__Q3u8t");
        assertEquals(classesMap.get(SkyscannerResponseParser.SEGMENT_TIMES_CONTAINER), "Times_segmentTimes__2eToH");
        assertEquals(classesMap.get(SkyscannerResponseParser.SEGMENT_ROUTES_CONTAINER), "Routes_routes__3lp8G");
        assertEquals(classesMap.get(SkyscannerResponseParser.PRICING_ITEM_CONTAINER), "PricingItem_agentRow__3GMsS");
        assertEquals(classesMap.get(SkyscannerResponseParser.PRICING_NAME_CONTAINER), "AgentDetails_agentContainer__kp8JA");
        assertEquals(classesMap.get(SkyscannerResponseParser.PRICING_COST_CONTAINER), "PricingItem_price__3bRSw");
        assertEquals(classesMap.get(SkyscannerResponseParser.TIME_OFFSET_TOOLTIP), "BpkText_bpk-text__2NHsO BpkText_bpk-text--sm__345aT TimeWithOffsetTooltip_offsetTooltip__24Ffv");
    }

    @Test
    void testDateParsing() throws ParseException {
        SimpleDateFormat parseDateFormat = new SimpleDateFormat("E, d MMM yyyy");
        String dateElement = "Tue, 29 Dec 2020";

        Date date = parseDateFormat.parse(dateElement);
        assertNotNull(date);
    }
}