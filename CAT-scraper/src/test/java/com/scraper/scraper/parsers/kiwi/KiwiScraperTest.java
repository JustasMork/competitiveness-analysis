package com.scraper.scraper.parsers.kiwi;

import com.catModel.*;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.scraper.TestBase;
import com.scraper.TestUtils;
import com.scraper.scraper.ScraperFactory;
import com.scraper.scraper.parsers.Scraper;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.io.StringWriter;
import java.text.ParseException;
import java.util.List;
import java.util.logging.Level;

@Disabled("Should be executed manually")
class KiwiScraperTest extends TestBase {

    @Autowired
    ScraperFactory factory;

    @BeforeAll
    static void setUp() {
        String appPath = System.getProperty("user.dir");
        String execPath = appPath + "\\exec\\";
        System.setProperty("webdriver.gecko.driver", execPath + "geckodriver.exe");
        System.setProperty("webdriver.chrome.driver", execPath + "chromedriver.exe");
    }

    @Test
    void testKiwiScraper() throws ParseException {
        LoggingPreferences logPrefs = new LoggingPreferences();
        logPrefs.enable(LogType.PERFORMANCE, Level.ALL);
        MutableCapabilities capabilities = new ChromeOptions();
        capabilities.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
        ChromeDriver chromeDriver = new ChromeDriver((ChromeOptions) capabilities);

        Scraper scraper = factory.createScraper(ScraperType.KIWI.getPropTag(), chromeDriver);
        FlightScrapeRequest request = TestUtils.generateParisBerlin();
        ScrapeResult result = scraper.scrape(request);

    }

    @Test
    void generateTest() throws ParseException {
        ScrapeRequests requests = new ScrapeRequests();
        requests.setBrowser("chrome");
        requests.setScraperTypes(List.of(ScraperType.SKYSCANNER, ScraperType.KIWI));
        requests.addRequest(TestUtils.generateParisBerlin());

        String labas = getResultsJson(requests);
        String assada = "asd";

    }

    private String getResultsJson(ScrapeRequests results) {
        StringWriter writer = new StringWriter();
        ObjectMapper mapper = new ObjectMapper();
        try {
            mapper.writeValue(writer, results);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return writer.toString();
    }
}