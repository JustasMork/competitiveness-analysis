package com.scraper.scraper.parsers.kiwi;

import com.catModel.FlightScrapeRequest;
import com.catModel.ScraperType;
import com.scraper.TestBase;
import com.scraper.TestUtils;
import com.scraper.general.Env;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.net.URISyntaxException;
import java.text.ParseException;

import static org.junit.jupiter.api.Assertions.assertTrue;

class KiwiRequestGeneratorTest extends TestBase {

    @Autowired
    private Env env;

    @Test
    void testGenerateOwRequest() throws ParseException, URISyntaxException {
        KiwiConfig config = new KiwiConfig(ScraperType.KIWI);
        config.setEnv(env);

        KiwiRequestGenerator generator = new KiwiRequestGenerator(config);

        FlightScrapeRequest request = TestUtils.generateOWRequest();
        String requestUrl = generator.generate(request);

        assertTrue(requestUrl.contains("https://www.kiwi.com/"));
        assertTrue(requestUrl.contains("/us/search/results"));
        assertTrue(requestUrl.contains("/vilnius-lithuania"));
        assertTrue(requestUrl.contains("/southend-united-kingdom"));
        assertTrue(requestUrl.contains("/2020-08-15"));
        assertTrue(requestUrl.contains("/no-return"));
        assertTrue(requestUrl.contains("adults=2"));
        assertTrue(requestUrl.contains("children=1"));
    }

    @Test
    void testGenerateOwaOrlyTegelRequest() throws ParseException, URISyntaxException {
        KiwiConfig config = new KiwiConfig(ScraperType.KIWI);
        config.setEnv(env);

        KiwiRequestGenerator generator = new KiwiRequestGenerator(config);

        FlightScrapeRequest request = TestUtils.generateParisBerlin();
        String requestUrl = generator.generate(request);

        assertTrue(requestUrl.contains("https://www.kiwi.com/"));
        assertTrue(requestUrl.contains("/us/search/results"));
        assertTrue(requestUrl.contains("/paris-orly-paris-france"));
        assertTrue(requestUrl.contains("/berlin-tegel-berlin-germany"));
        assertTrue(requestUrl.contains("/2021-02-20"));
        assertTrue(requestUrl.contains("/no-return"));
        assertTrue(requestUrl.contains("adults=2"));
        assertTrue(requestUrl.contains("children=1"));
    }
}