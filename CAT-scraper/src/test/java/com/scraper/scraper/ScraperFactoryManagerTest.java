package com.scraper.scraper;

import com.catModel.ScraperType;
import com.scraper.TestBase;
import com.scraper.scraper.parsers.Scraper;
import com.scraper.scraper.parsers.kiwi.KiwiScraper;
import com.scraper.scraper.parsers.skyscanner.SkyscannerScraper;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;

class ScraperFactoryManagerTest extends TestBase {

    @Autowired
    ScraperFactory factory;

    @Test
    void testSkyscannerScraperInitialization() {
        WebDriver webDriverMock = mock(WebDriver.class);
        Scraper scraper = factory.createScraper(ScraperType.SKYSCANNER.getPropTag(), webDriverMock);
        assertEquals(SkyscannerScraper.class, scraper.getClass());
    }

    @Test
    void testKiwiScraperInitialization() {
        WebDriver webDriverMock = mock(WebDriver.class);
        Scraper scraper = factory.createScraper(ScraperType.KIWI.getPropTag(), webDriverMock);
        assertEquals(KiwiScraper.class, scraper.getClass());
    }

}