package com.scraper;


import com.catModel.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestUtils {

    public static final SimpleDateFormat dtFormat = new SimpleDateFormat("yyyy-MM-dd");

    public static FlightScrapeRequest generateOWRequest() throws ParseException {
        Airport origin = new Airport();
        origin.setIata("VNO");
        origin.setName("Vilnius International Airport");
        origin.setCity("Vilnius");
        origin.setCountry("Lithuania");
        origin.setCountryCode("lt");

        Airport destination = new Airport();
        destination.setIata("SEN");
        destination.setName("Southend Airport");
        destination.setCity("Southend");
        destination.setCountry("United Kingdom");
        destination.setCountryCode("gb");

        ODpair odPair = new ODpair();
        odPair.setOrigin(origin);
        odPair.setDestination(destination);
        odPair.setDeparture(dtFormat.parse("2020-08-15"));

        Map<PTC, Integer> travellers = new HashMap<>();
        travellers.put(PTC.ADT, 2);
        travellers.put(PTC.CHD, 1);

        FlightScrapeRequest request = new FlightScrapeRequest();
        request.setCountryCode("us");
        request.setItinerary(List.of(odPair));
        request.setCabinClass(CabinClass.Economy);
        request.setTravellers(travellers);

        return request;
    }

    public static FlightScrapeRequest generateParisBerlin() throws ParseException {
        Airport origin = new Airport();
        origin.setIata("ORY");
        origin.setName("Paris-Orly Airport");
        origin.setCity("Paris");
        origin.setCountry("France");
        origin.setCountryCode("fr");

        Airport destination = new Airport();
        destination.setIata("TXL");
        destination.setName("Berlin-Tegel Airport");
        destination.setCity("Berlin");
        destination.setCountry("Germany");
        destination.setCountryCode("de");

        ODpair odPair = new ODpair();
        odPair.setOrigin(origin);
        odPair.setDestination(destination);
        odPair.setDeparture(dtFormat.parse("2021-02-20"));

        Map<PTC, Integer> travellers = new HashMap<>();
        travellers.put(PTC.ADT, 1);
    //    travellers.put(PTC.CHD, 1);

        FlightScrapeRequest request = new FlightScrapeRequest();
        request.setCountryCode("us");
        request.setItinerary(List.of(odPair));
        request.setCabinClass(CabinClass.Economy);
        request.setTravellers(travellers);

        return request;
    }

}
